//
//  GMMapTeamFansCellTableViewCell.h
//  GridironMoe
//
//  Created by Adya on 8/17/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^SaveButtonCallback)();

@interface GMMapTeamFansCell : UITableViewCell

-(void) setFansCount:(NSInteger)fansCount forTeam:teamId andSaveBlock:(SaveButtonCallback) callback;

@property BOOL canSave;
@property BOOL savedTeam;

+(CGFloat) height;
@end
