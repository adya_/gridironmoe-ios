//
//  TSBlockView.h
//  GridironMoe
//
//  Created by Adya on 10/3/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef BOOL(^BlockCompletion)(CGPoint point);
typedef void(^BlockAction)(CGPoint point);

@interface TSBlockView : UIView

-(void) setBlockerCondition:(BlockCompletion)condition;
-(void) setCustomTouchAction:(BlockAction)action;

@end
