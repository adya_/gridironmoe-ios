//
//  GMQuizAnswerView.m
//  GridironMoe
//
//  Created by Adya on 8/13/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMQuizAnswerView.h"

@interface GMQuizAnswerView ()
@property (weak, nonatomic) IBOutlet UIImageView *ivBackground;
@property (weak, nonatomic) IBOutlet UILabel *lTitle;
@property (weak, nonatomic) IBOutlet UILabel *lDetails;
@property (weak, nonatomic) IBOutlet UILabel *lLetter;
@property (weak, nonatomic) IBOutlet UILabel *lResult;
@property (weak, nonatomic) IBOutlet UIButton *bAccept;
@end

@implementation GMQuizAnswerView{
     ButtonCallback completionBlock;
    ButtonCallback redirectionBlock;
    UIImage* wrongBG;
    UIImage* rightBG;
}

-(void) awakeFromNib{
    wrongBG = [UIImage imageNamed:@"popup_wrong_question"];
    rightBG = [UIImage imageNamed:@"popup_next_question"];
}

-(void) prepareForInterfaceBuilder{
    [self setAnswer:YES withLetter:@"B" andPoints:10 orRedirectBlock:nil withCompletion:nil];
}

- (IBAction)resultClick:(id)sender {
    if(redirectionBlock)
        redirectionBlock();
    else NSLog(@"Redirection block is nil");
}

- (IBAction)acceptClick:(id)sender {
    if (completionBlock)
        completionBlock();
    else NSLog(@"Completion block is nil");
}

-(void) setAnswer:(BOOL)isCorrect withLetter:(NSString *)letter andPoints:(int)points orRedirectBlock:(ButtonCallback)redirect withCompletion:(ButtonCallback)completion{
    completionBlock = completion;
    redirectionBlock = redirect;
    self.ivBackground.image = (isCorrect ? rightBG : wrongBG);
    self.lTitle.text = (isCorrect ? @"You're Correct" : @"Sorry! Wrong Answer");
    self.lLetter.text = letter;
    NSString* result = (isCorrect ? [NSString stringWithFormat:@"You've earned %d points", points] : @"");
    self.lResult.text = result;
    [self.bAccept setTitle:@"NEXT QUESTION" forState:UIControlStateNormal];
    [self setAlertMode:NO];
}

-(void) setAlertWithTitle:(NSString *)title andMessage:(NSString *)message withCompletion:(ButtonCallback)completion{
    completionBlock = completion;
    self.lTitle.text = title;
    self.ivBackground.image = wrongBG;
    self.lDetails.text = message;
    [self.bAccept setTitle:@"OK" forState:UIControlStateNormal];
    [self setAlertMode:YES];
    
}

-(void) setAlertMode:(BOOL) isAlertMode{
    self.lTitle.hidden = NO;
    self.lDetails.hidden = NO;
    self.bAccept.hidden = NO;
    self.lLetter.hidden = isAlertMode;
    self.lResult.hidden = isAlertMode;
}

@end
