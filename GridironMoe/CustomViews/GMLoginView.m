//
//  GMQuizAnswerView.m
//  GridironMoe
//
//  Created by Adya on 8/13/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMLoginView.h"

@interface GMLoginView ()

@property (weak, nonatomic) IBOutlet UITextField *tfUsername;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword;

@end

@implementation GMLoginView{
    LoginPopupCallback callback;
}

-(void) setDefaultUserame:(NSString*) username{
    self.tfUsername.text = username;
}

-(void) setLoginCallback:(LoginPopupCallback)_callback{
    callback = _callback;
}

- (IBAction)signIn:(id)sender {
    if (callback)
        callback(self.tfUsername.text, self.tfPassword.text);
}

@end
