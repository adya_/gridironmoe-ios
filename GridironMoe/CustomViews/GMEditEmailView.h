//
//  GMQuizAnswerView.h
//  GridironMoe
//
//  Created by Adya on 8/13/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^EditEmailPopupCallback)(NSString* email);

@interface GMEditEmailView : UIView

-(void) setEditedCallback:(EditEmailPopupCallback) callback;

-(void) setEmail:(NSString*)email;

-(void) startEdit;
@end
