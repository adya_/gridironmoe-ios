//
//  GMQuizAnswerView.h
//  GridironMoe
//
//  Created by Adya on 8/13/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ForgotPasswordPopupCallback)(NSString* username, NSString* email);

@interface GMForgotPasswordView : UIView

-(void) setEditedCallback:(ForgotPasswordPopupCallback) callback;

-(void) setUsername:(NSString*)username andEmail:(NSString*)email;

-(void) startEdit;
@end
