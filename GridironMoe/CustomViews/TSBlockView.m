//
//  TSBlockView.m
//  GridironMoe
//
//  Created by Adya on 10/3/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "TSBlockView.h"

@implementation TSBlockView{
    BlockCompletion condition;
    BlockAction action;
    BOOL blocked;
}

-(void) setBlockerCondition:(BlockCompletion)_condition{
    condition = _condition;
}
-(void) setCustomTouchAction:(BlockAction)_action{
    action = _action;
}

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch* t = [touches anyObject];
    //[self logTouch:t];
    if (blocked && action)
        action([t locationInView:self]);
}

-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    //UITouch* t = [touches anyObject];
    //[self logTouch:t];
    blocked = NO;
}

-(void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    //UITouch* t = [touches anyObject];
    //[self logTouch:t];
    blocked = NO;
}

-(void) logTouch:(UITouch*)t{
    switch (t.phase) {
        case UITouchPhaseBegan: {
            NSLog(@"TOUCH BEGAN");
            break;
        }
        case UITouchPhaseMoved: {
            NSLog(@"TOUCH MOVED");
            break;
        }
        case UITouchPhaseStationary: {
            NSLog(@"TOUCH STATIONARY");
            break;
        }
        case UITouchPhaseEnded: {
            NSLog(@"TOUCH ENDED");
            break;
        }
        case UITouchPhaseCancelled: {
            NSLog(@"TOUCH CANCELLED");
            break;
        }
        default: {
            NSLog(@"TOUCH DEFAULT");
            break;
        }
    }
}

-(BOOL) pointInside:(CGPoint)point withEvent:(UIEvent *)event{
    //NSLog(@"POINT: %@", NSStringFromCGPoint(point));
    point = [self convertPoint:point toView:self.superview];
    //NSLog(@"CONV POINT: %@", NSStringFromCGPoint(point));
    blocked = condition(point);
    //NSLog(@"TOUCH %@ : %@",self, (blocked ? @"BLOCK" : @"PASS"));
    return blocked;
}


@end
