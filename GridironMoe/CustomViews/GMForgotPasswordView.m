//
//  GMQuizAnswerView.m
//  GridironMoe
//
//  Created by Adya on 8/13/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMForgotPasswordView.h"
#import "TSUtils.h"

@interface GMForgotPasswordView ()

@property (weak, nonatomic) IBOutlet UITextField *tfUsername;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;

@end

@implementation GMForgotPasswordView{
    ForgotPasswordPopupCallback callback;
}

-(void) setEditedCallback:(ForgotPasswordPopupCallback)_callback{
    callback = _callback;
}

-(void) setUsername:(NSString *)username andEmail:(NSString *)email{
    self.tfUsername.text = username;
    self.tfEmail.text = email;
}

- (IBAction)changeClick:(id)sender {
    if (callback){
        NSString* username = trim(self.tfUsername.text);
        NSString* email = trim(self.tfEmail.text);
        callback(username.length == 0 ? nil : username, email.length == 0 ? nil : email);
    }
}

- (IBAction)backClick:(id)sender {
    if (callback)
        callback(nil, nil);
}

-(void) startEdit{
    [self.tfUsername becomeFirstResponder];
}

@end
