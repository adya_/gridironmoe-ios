//
//  GMQuizAnswerView.h
//  GridironMoe
//
//  Created by Adya on 8/13/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^EditUsernamePopupCallback)(NSString* username);

@interface GMEditUsernameView : UIView

-(void) setEditedCallback:(EditUsernamePopupCallback) callback;

-(void) setUsername:(NSString*)username;
@end
