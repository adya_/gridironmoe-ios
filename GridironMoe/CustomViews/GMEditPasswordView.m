//
//  GMQuizAnswerView.m
//  GridironMoe
//
//  Created by Adya on 8/13/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMEditPasswordView.h"
#import "TSUtils.h"

@interface GMEditPasswordView ()
@property (weak, nonatomic) IBOutlet UITextField *tfOld;
@property (weak, nonatomic) IBOutlet UITextField *tfNew;
@property (weak, nonatomic) IBOutlet UITextField *tfConfirm;

@end

@implementation GMEditPasswordView{
    EditPasswordPopupCallback callback;
}

-(void) setEditedCallback:(EditPasswordPopupCallback)_callback{
    callback = _callback;
}

- (IBAction)changeClick:(id)sender {
    if (callback){
        NSString* old = trim(self.tfOld.text);
        NSString* new = trim(self.tfNew.text);
        NSString* confirm = trim(self.tfConfirm.text);
        old = (old.length == 0 ? nil : old);
        new = (new.length == 0 ? nil : new);
        confirm = (confirm.length == 0 ? nil : confirm);
        callback(old, new, confirm);
    }
}

- (IBAction)backClick:(id)sender {
    if (callback)
        callback(nil, nil, nil);
}

-(void) clear{
    self.tfOld.text = self.tfNew.text = self.tfConfirm.text = @"";
}

-(void) startEdit{
    [self.tfOld becomeFirstResponder];
}

@end
