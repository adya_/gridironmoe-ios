//
//  GMQuizAnswerView.h
//  GridironMoe
//
//  Created by Adya on 8/13/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^EditPasswordPopupCallback)(NSString* oldPassword, NSString* newPassword, NSString* confirmPassword);

@interface GMEditPasswordView : UIView

-(void) setEditedCallback:(EditPasswordPopupCallback) callback;
-(void) clear;
-(void) startEdit;
@end
