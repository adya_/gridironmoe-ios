//
//  GMQuizAnswerView.m
//  GridironMoe
//
//  Created by Adya on 8/13/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMEditAvatarView.h"

@interface GMEditAvatarView ()

@property (weak, nonatomic) IBOutlet UITextField *tfUsername;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword;

@end

@implementation GMEditAvatarView{
    LoginPopupCallback callback;
}

-(void) setLoginCallback:(LoginPopupCallback)_callback{
    callback = _callback;
}

- (IBAction)signIn:(id)sender {
    if (callback)
        callback(self.tfUsername.text, self.tfPassword.text);
}

@end
