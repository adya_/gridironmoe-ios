//
//  GMQuizAnswerView.h
//  GridironMoe
//
//  Created by Adya on 8/13/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GMMoeFace;

typedef void(^SelectMoePopupCallback)(GMMoeFace* moeName);

@interface GMSelectMoeView : UIView

-(void) setSelectMoeCallback:(SelectMoePopupCallback) callback;
@end
