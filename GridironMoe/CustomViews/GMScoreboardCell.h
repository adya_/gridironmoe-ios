//
//  GMScoreboardCell.h
//  GridironMoe
//
//  Created by Adya on 8/17/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GMScoreboardCell : UITableViewCell

-(void) setUser:(NSString*) username withAvatar:(NSString*)avatar andPoints:(int) points toPosition:(NSInteger) position;

-(void) setUser:(NSString*) username withAvatarURL:(NSString*)avatar andPoints:(int) points toPosition:(NSInteger) position;

-(void) setUser:(NSString*) username withAvatar:(NSString*)avatar andPoints:(int) points toPosition:(NSInteger) position datePlayed:(NSString*)date;

-(void) setUser:(NSString*) username withAvatarURL:(NSString*)avatar andPoints:(int) points toPosition:(NSInteger) position datePlayed:(NSString*)date;

+(CGFloat) height;
@end
