//
//  GMQuizAnswerView.m
//  GridironMoe
//
//  Created by Adya on 8/13/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMEditUsernameView.h"
#import "TSUtils.h"

@interface GMEditUsernameView ()

@property (weak, nonatomic) IBOutlet UITextField *tfUsername;

@end

@implementation GMEditUsernameView{
    EditUsernamePopupCallback callback;
}

-(void) setEditedCallback:(EditUsernamePopupCallback)_callback{
    callback = _callback;
}

-(void) setUsername:(NSString *)username{
    self.tfUsername.text = username;
}

- (IBAction)changeClick:(id)sender {
    if (callback){
        NSString* username = trim(self.tfUsername.text);
        callback(username.length == 0 ? nil : username);
    }
}

- (IBAction)backClick:(id)sender {
    if (callback)
        callback(nil);
}

@end
