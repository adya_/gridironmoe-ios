//
//  GMMapTeamFansCellTableViewCell.m
//  GridironMoe
//
//  Created by Adya on 8/17/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMMapTeamFansCell.h"
#import "TSUtils.h"


@interface GMMapTeamFansCell ()
@property (weak, nonatomic) IBOutlet UILabel *lTeam;
@property (weak, nonatomic) IBOutlet UILabel *lCount;
@property (weak, nonatomic) IBOutlet UIButton *bSave;

@end

@implementation GMMapTeamFansCell{
    SaveButtonCallback call;
    BOOL saved;
}

@synthesize canSave;

-(void) setSavedTeam:(BOOL)savedTeam{
    saved = savedTeam;
    self.contentView.backgroundColor = (savedTeam ? colorHexString(@"#1b1c1f") : colorHexString(@"#26282f"));
}

-(BOOL) savedTeam{
    return saved;
}

- (void)awakeFromNib {
    // Initialization code
}

-(void) setFansCount:(NSInteger)fansCount forTeam:teamId andSaveBlock:(SaveButtonCallback) callback {
    self.lTeam.text = teamId;
    self.lCount.text = [NSString stringWithFormat:@"%ld", (long)fansCount];
    call = callback;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected){
        UIView* view = [UIView new];
        view.backgroundColor = colorHexString(@"#50C800");
        [self setSelectedBackgroundView:view];
    }
    self.bSave.hidden = !canSave || !selected;
    
}

- (IBAction)saveClick:(id)sender {
    if (call) call();
}

+(CGFloat) height{
    return 30;
}

@end
