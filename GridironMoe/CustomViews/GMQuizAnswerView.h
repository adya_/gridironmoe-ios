//
//  GMQuizAnswerView.h
//  GridironMoe
//
//  Created by Adya on 8/13/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ButtonCallback)();

@interface GMQuizAnswerView : UIView

-(void) setAnswer:(BOOL) isCorrect withLetter:(NSString*) letter andPoints:(int) points orRedirectBlock:(ButtonCallback) redirect withCompletion:(ButtonCallback) completion;

-(void) setAlertWithTitle:(NSString*)title andMessage:(NSString*) message withCompletion:(ButtonCallback)completion;
@end
