//
//  GMQuizAnswerView.m
//  GridironMoe
//
//  Created by Adya on 8/13/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMSelectMoeView.h"
#import "GMUserManager.h"
#import "GMMoeFace.h"
#import "CPPickerView.h"

@interface GMSelectMoeView () <CPPickerViewDelegate, CPPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UIImageView *ivPreview;
@property (weak, nonatomic) IBOutlet UIView *vOptions;
@property (weak, nonatomic) IBOutlet UILabel *lMoeName;
@property (weak, nonatomic) IBOutlet CPPickerView*pvPreview;
@end

@implementation GMSelectMoeView{
    SelectMoePopupCallback callback;
    GMMoeFace* selectedMoe;
    NSInteger selectedMoeIndex;
}

-(id) init{
    self = [super init];
    [self adjustMoeButtons];
    [self loadCurrentMoe];
    [self.pvPreview reloadData];
    [self initSelection];
    return self;
}

-(void) awakeFromNib{
    [self adjustMoeButtons];
    [self loadCurrentMoe];
    [self.pvPreview reloadData];
    [self initSelection];
}

-(void) initSelection{
    selectedMoe = [GMUserManager sharedManager].moeFace;
    selectedMoeIndex = [GMMoeFace indexOfMoeFace:[GMUserManager sharedManager].moeFace];
    [self.pvPreview setSelectedItem:selectedMoeIndex];
}

-(NSInteger) numberOfItemsInPickerView:(CPPickerView *)pickerView{
    return [GMMoeFace availableMoeFaces];
}

-(UIView*) pickerView:(CPPickerView *)pickerView viewForItem:(NSInteger)item reusingView:(UIView *)reusableView{
    UIImageView* img = (UIImageView*)reusableView;
    
    if (!img){
        img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
        img.contentMode = UIViewContentModeScaleAspectFit;
    }
    img.image = [UIImage imageNamed:[GMMoeFace moeFaceAtIndex:item].avatar];
    return img;
}

-(void) pickerView:(CPPickerView *)pickerView didSelectItem:(NSInteger)item{
    [self selectMoeAtIndex:item];
}

-(void) createPicker{
}

-(void) loadCurrentMoe{
    selectedMoe = [GMUserManager sharedManager].moeFace;
    selectedMoeIndex = [GMMoeFace indexOfMoeFace:selectedMoe];
    [self selectMoeAtIndex:selectedMoeIndex];
}

-(void) adjustMoeButtons{
    for (UIButton* btn in self.vOptions.subviews) {
        btn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    
}

-(void) setSelectMoeCallback:(SelectMoePopupCallback)_callback{
    callback = _callback;
}

- (IBAction)save:(id)sender {
    if (callback)
        callback(selectedMoe);
}

- (IBAction)right:(id)sender {
    NSUInteger total = [GMMoeFace availableMoeFaces];
    selectedMoeIndex = (selectedMoeIndex + 1) % total;
    [self.pvPreview setSelectedItem:selectedMoeIndex animated:YES];
    //[self selectMoeAtIndex:selectedMoeIndex];
}

- (IBAction)left:(id)sender {
    NSUInteger total = [GMMoeFace availableMoeFaces];
    selectedMoeIndex = (selectedMoeIndex == 0 ? total - 1 : selectedMoeIndex - 1);
    [self.pvPreview setSelectedItem:selectedMoeIndex animated:YES];
    //[self selectMoeAtIndex:selectedMoeIndex];
}
- (IBAction)moeClick:(id)sender {
    selectedMoeIndex = [(UIView*)sender tag] - 1;
    [self.pvPreview setSelectedItem:selectedMoeIndex animated:YES];
    //[self selectMoeAtIndex:selectedMoeIndex];
}

-(void) selectMoeAtIndex:(NSInteger)index{
    selectedMoe = [GMMoeFace moeFaceAtIndex:index];
    self.lMoeName.text = selectedMoe.name;
}

@end
