//
//  GMQuizAnswerView.h
//  GridironMoe
//
//  Created by Adya on 8/13/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^LoginPopupCallback)(NSString* username, NSString* password);

@interface GMLoginView : UIView

-(void) setLoginCallback:(LoginPopupCallback) callback;
-(void) setDefaultUserame:(NSString*) username;
@end
