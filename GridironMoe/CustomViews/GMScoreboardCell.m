//
//  GMScoreboardCell.m
//  GridironMoe
//
//  Created by Adya on 8/17/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMScoreboardCell.h"
#import "UIImageView+AFNetworking.h"


@interface GMScoreboardCell ()
@property (weak, nonatomic) IBOutlet UIImageView *ivAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lPosition;
@property (weak, nonatomic) IBOutlet UILabel *lDate;
@property (weak, nonatomic) IBOutlet UILabel *lPoints;
@property (weak, nonatomic) IBOutlet UILabel *lName;
@end

@implementation GMScoreboardCell

-(void) setUser:(NSString *)username withAvatar:(NSString*)avatar andPoints:(int)points toPosition:(NSInteger)position{
    [self setUser:username withAvatar:avatar andPoints:points toPosition:position datePlayed:nil];
}

-(void) setUser:(NSString *)username withAvatarURL:(NSString*)avatar andPoints:(int)points toPosition:(NSInteger)position{
    [self setUser:username withAvatarURL:avatar andPoints:points toPosition:position datePlayed:nil];
}

-(void) setUser:(NSString *)username withAvatar:(NSString*)avatar andPoints:(int)points toPosition:(NSInteger)position datePlayed:(NSString *)date{
    self.lName.text = username;
    self.lPoints.text = [NSString stringWithFormat:@"%d", points];
    self.lPosition.text = [NSString stringWithFormat:@"%ld", (long)position];
    self.lDate.text = date;
    if (avatar)
        self.ivAvatar.image = [UIImage imageNamed:avatar];
    self.ivAvatar.layer.cornerRadius = self.ivAvatar.frame.size.width/2;
    self.ivAvatar.layer.masksToBounds = YES;
    self.ivAvatar.contentMode = UIViewContentModeScaleAspectFit;
}

-(void) setUser:(NSString *)username withAvatarURL:(NSString*)avatar andPoints:(int)points toPosition:(NSInteger)position datePlayed:(NSString *)date{
    self.lName.text = username;
    self.lPoints.text = [NSString stringWithFormat:@"%d", points];
    self.lPosition.text = [NSString stringWithFormat:@"%ld", (long)position];
    self.lDate.text = date;
    if (avatar)
        [self.ivAvatar setImageWithURL:[NSURL URLWithString:avatar]];
    self.ivAvatar.layer.cornerRadius = self.ivAvatar.frame.size.width/2;
    self.ivAvatar.layer.masksToBounds = YES;
    self.ivAvatar.contentMode = UIViewContentModeScaleAspectFit;
}

+(CGFloat) height{
    return 62.0;
}
@end
