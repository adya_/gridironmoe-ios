//
//  GMQuizAnswerView.m
//  GridironMoe
//
//  Created by Adya on 8/13/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMEditEmailView.h"
#import "TSUtils.h"

@interface GMEditEmailView ()

@property (weak, nonatomic) IBOutlet UITextField *tfEmail;

@end

@implementation GMEditEmailView{
    EditEmailPopupCallback callback;
}

-(void) setEditedCallback:(EditEmailPopupCallback)_callback{
    callback = _callback;
}

-(void) setEmail:(NSString *)email{
    self.tfEmail.text = email;
}

- (IBAction)changeClick:(id)sender {
    if (callback){
        NSString* email = trim(self.tfEmail.text);
        callback(email.length == 0 ? nil : email);
    }
}

- (IBAction)backClick:(id)sender {
    if (callback)
        callback(nil);
}

-(void) startEdit{
    [self.tfEmail becomeFirstResponder];
}

@end
