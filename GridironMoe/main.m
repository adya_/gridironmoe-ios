//
//  main.m
//  GridironMoe
//
//  Created by Adya on 8/10/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GMAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([GMAppDelegate class]));
    }
}
