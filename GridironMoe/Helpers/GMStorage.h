//
//  GMStorage.h
//  GridironMoe
//
//  Created by Adya on 8/19/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "TSStorage.h"

#define STORAGE_DICTIONARY_TEAMS @"teamsDictionary"

#define STORAGE_REF_LOGIN_VIEW_CONTROLLER @"gmLoginViewController"

#define STORAGE_REF_QUIZ_READY_VIEW_CONTROLLER @"gmGetReadyViewController"

#define STORAGE_VALUE_QUIZ_RESULT @"quizResult"

#define STORAGE_VALUE_VOTE_RESULT @"voteResult"
#define STORAGE_VALUE_VOTE_GAME @"voteGame"

#define STORAGE_VALUE_VOTE_SELECTED_GAME @"voteSelectedGame"

#define STORAGE_ARRAY_OFFENSE_STRATEGIES @"offenseStrategies"

#define STORAGE_ARRAY_DEFENSE_STRATEGIES @"defenseStrategies"

#define STORAGE_ARRAY_AVAILABLE_MOE @"availableMoes"

#define STORAGE_ARRAY_MOE_MESSAGES @"moeMessages"


@class TSError;

@interface GMStorage : TSStorage

@property (readonly) BOOL isLoaded;

-(void) preloadStorageWithCompletion:(void (^)(BOOL success, TSError* error))completion;

@end
