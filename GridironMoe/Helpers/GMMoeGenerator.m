//
//  GMMoeGenerator.m
//  GridironMoe
//
//  Created by Adya on 10/2/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMMoeGenerator.h"
#import "GMMoeFace.h"
#import "TSUtils.h"
#import "TSNotifier.h"
#import "TSBlockView.h"
#import "MBProgressHUD.h"
#import "GMStorage.h"

#define LOGGING NO

#define POPUP_TIME 2.3f

// time in seconds between spawns
#define MIN_SPAWN_INTERVAL 6
#define MAX_SPAWN_INTERVAL 12

// floating time in seconds
#define MIN_FLOAT_INTERVAL 4
#define MAX_FLOAT_INTERVAL 8

// rotation time in seconds
#define MIN_ROTATION_INTERVAL 1
#define MAX_ROTATION_INTERVAL 4

// default floating view size
#define MOE_SIZE 60

// dispersion of the floating view size
#define MOE_SIZE_DISP 0

#define KEY_ENABLED @"moeGeneratorEnabled"

typedef NS_ENUM(NSInteger, GMPosition){
    GM_POS_TOP = 0,
    GM_POS_RIGHT,
    GM_POS_DOWN,
    GM_POS_LEFT,
    GM_POS_COUNT
};

@interface GMMoeGenerator () <UIGestureRecognizerDelegate>

@end

@implementation GMMoeGenerator{
    UIView* target;
    TSBlockView* blockView;
    NSMutableArray* floatingViews;
    //UIImageView* floatingView;
    NSTimer* spawnTimer;
    
    BOOL isEnabled;
    int maxMoes;
    int minSpawnInterval;
    int maxSpawnInterval;
    
    
    // random params
    GMMoeFace* moeFace;
    CGSize moeSize;
    GMPosition startPos;
    CGPoint startPoint;
    GMPosition endPos;
    CGPoint endPoint;
    BOOL shouldRotate;
    NSTimeInterval rotatingTime; // rotation speed
    NSTimeInterval floatingTime; // floating speed
    NSTimeInterval nextSpawnTime;// spawn frequency
}


+(GMMoeGenerator*) sharedGenerator{
    static GMMoeGenerator* generator = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{generator = [[self alloc] init];});
    return generator;
}

-(id) init{
    self = [super init];
    maxMoes = 1;
    floatingViews = [NSMutableArray new];
    [self setSpawnIntervalFrom:MIN_SPAWN_INTERVAL to:MAX_SPAWN_INTERVAL];
    return self;
}

+(void) setEnabled:(BOOL)_isEnabled{
    [[GMMoeGenerator sharedGenerator] setEnabled:_isEnabled];
}

+(BOOL) isEnabled{
    return [[GMMoeGenerator sharedGenerator] isEnabled];
}

+ (void) setMaxMoesAmount:(int)maxMoesAmount{
    [[GMMoeGenerator sharedGenerator] setMaxMoesAmount:maxMoesAmount];
}

+(void) setSpawnIntervalFrom:(int)from to:(int)to{
    [[GMMoeGenerator sharedGenerator] setSpawnIntervalFrom:from to:to];
}

+(void) startSpawnOnView:(UIView*) targetView{
    [[self sharedGenerator] startSpawnOnView:targetView];
}

+(void) startSpawn{
    [[self sharedGenerator] startSpawn];
}
+(void) pauseSpawn{
    [[self sharedGenerator] pauseSpawn];
}
+(void) stopSpawn{
    [[self sharedGenerator] stopSpawn];
}

-(void) startSpawnOnView:(UIView*)targetView{
    target = targetView;
    [self loadSettings];
    [self startTimer];
}

-(void) startSpawn{
    if (!target){
        if (LOGGING) NSLog(@"Can't spawn moes: Target View was not set.");
        return;
    }
    [self startTimer];
}

-(void) pauseSpawn{
    [spawnTimer invalidate];
    startPos = endPos = GM_POS_COUNT;
    while (floatingViews.count > 0) {
        [self removeView:floatingViews[0]];
    }
}

-(void) stopSpawn{
    [self pauseSpawn];
    [blockView removeFromSuperview];
    blockView = nil;
    target = nil;
}

-(void) setMaxMoesAmount:(int) maxMoesAmount{
    maxMoes = maxMoesAmount;
}

-(void) setSpawnIntervalFrom:(int)from to:(int)to{
    minSpawnInterval = from;
    maxSpawnInterval = to;
}

-(void) saveSettings{
    NSUserDefaults* prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:@(isEnabled) forKey:KEY_ENABLED];
    [prefs synchronize];
}

-(void) loadSettings{
    NSUserDefaults* prefs = [NSUserDefaults standardUserDefaults];
    id obj = [prefs objectForKey:KEY_ENABLED];
    isEnabled = (obj ? [obj boolValue] : YES);
}

-(void) setEnabled:(BOOL) _isEnabled{
    isEnabled = _isEnabled;
    [self saveSettings];
    if (!isEnabled)
        [self stopSpawn];
}
-(BOOL) isEnabled{
    return isEnabled;
}

-(void) startTimer{
    if (!isEnabled){
        if (LOGGING) NSLog(@"Moe Generator disabled.");
        return;
    }
        
    if (!target){
        if (LOGGING) NSLog(@"Can't spawn moes: Target View was not set.");
        return;
    }
    nextSpawnTime = [self randomSpawnInterval];
    if (LOGGING) NSLog(@"Next Moe Face in %.1f sec", nextSpawnTime);
    spawnTimer = [NSTimer scheduledTimerWithTimeInterval:nextSpawnTime target:self selector:@selector(timerTick:) userInfo:nil repeats:NO];
}

-(void) timerTick:(id)sender{
    if (floatingViews.count < maxMoes){
        [self initFloatingParams];
        UIImageView* view = [self createFloatingView];
        [self createBlocker];
        [self createFloatingAnimationForView:view];
        if (shouldRotate)
            [self createRotationAnimationForView:view];
    }
    [self startTimer];
}

-(void) removeView:(UIView*)view{
    [floatingViews removeObject:view];
    [view removeFromSuperview];
    if (LOGGING) NSLog(@"Floating Moes: (%d/%d)", floatingViews.count, maxMoes);
}

// determines moe view size and floating path
-(void) initFloatingParams{
    moeSize = CGSizeMake(MOE_SIZE, MOE_SIZE);//[self randomMoeSize];// size must be calculated first (will be used in position generation)
    startPos = [self randomPositionExcluding:GM_POS_COUNT];
    endPos = [self randomPositionExcluding:startPos];
    startPoint = [self randomPointForPosition:startPos];
    endPoint = [self randomPointForPosition:endPos];
    shouldRotate = [self randomRotationFlag];
    rotatingTime = [self randomRotationInterval];
    floatingTime = [self randomFloatingInterval];
    if (LOGGING) NSLog(@"\n\tFloating: %.1f sec\n\tRotation: %.1f sec\n\tSize: %@\n\tFrom: %@\n\tTo: %@\n", floatingTime, (shouldRotate ? rotatingTime : 0), NSStringFromCGSize(moeSize), NSStringFromCGPoint(startPoint), NSStringFromCGPoint(endPoint));
}

-(UIImageView*) createFloatingView{
    if (!target){
        if (LOGGING) NSLog(@"Can't create views: Target View was not set.");
        return nil;
    }
    UIImageView* floatingView = [self generateRandomFloatingMoe];
    floatingView.frame = CGRectMake(startPoint.x, startPoint.y, moeSize.width, moeSize.height);
    [target addSubview:floatingView];
    [floatingViews addObject:floatingView];
    if (LOGGING) NSLog(@"Floating Moes: (%d/%d)", floatingViews.count, maxMoes);
    return floatingView;
}

-(void) createBlocker{
    if (!blockView){
        blockView = [[TSBlockView alloc] initWithFrame:target.frame];
    }
    __weak NSMutableArray* weakFloatingViews = floatingViews;
    __weak UIView* weakTarget = target;
    __weak id weakSelf = self;
    __block UIView* hitView = nil;
    [blockView setBlockerCondition:^BOOL(CGPoint point) {
        for (UIView* floatingView in weakFloatingViews) {
            if ([floatingView.layer.presentationLayer hitTest:point] != nil){
                hitView = floatingView;
                return YES;
            }
        }
        return NO;
    }];
    
    [blockView setCustomTouchAction:^(CGPoint point) {
        [TSNotifier notify:[weakSelf randomMessage]
                    withAppearance:@{kTSNotificationAppearanceNotificationColor : colorHexString(@"#C800B832"),
                                     kTSNotificationAppearanceTextColor : [UIColor whiteColor],
                                     kTSNotificationAppearanceTextSize : @(20.0f)}
                    timeInterval:POPUP_TIME
                    onView:weakTarget];
        if (hitView)
            [weakSelf removeView:hitView];
       // [weakSelf pauseSpawn];
       // [weakSelf startTimer];
    }];
    [target addSubview:blockView];
}

-(void) createFloatingAnimationForView:(UIView*)floatingView{
    if (!target || !floatingView){
        if (LOGGING) NSLog(@"Can't animate floating: either Target View or Floating View was not set.");
        return;
    }
    [UIView animateWithDuration:floatingTime delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        floatingView.frame = CGRectMake(endPoint.x, endPoint.y, moeSize.width, moeSize.height);
    } completion:^(BOOL finished) {
        [floatingView.layer removeAllAnimations];
        if (finished){
            if (maxMoes == 1){
                [self startTimer];
            }
            else{
                [self removeView:floatingView];
            }
        }
    }];
    
}

-(void) createRotationAnimationForView:(UIView*)floatingView{
    [UIView animateWithDuration:rotatingTime/4.0f delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        [floatingView setTransform:CGAffineTransformRotate(floatingView.transform, M_PI_2)];
    } completion:^(BOOL finished) {
        if (finished){
            [self createRotationAnimationForView:floatingView];
        }
    }];
}

-(NSString*) randomMessage{
    NSArray* msgs = [[GMStorage sharedStorage] getValueForKey:STORAGE_ARRAY_MOE_MESSAGES];
    return [msgs objectAtIndex:randomInRange(0, msgs.count)];
}
-(BOOL) randomRotationFlag{
    return random(100) % 2 == 0;
}
-(CGSize) randomMoeSize{
    NSInteger size = randomInRange(MOE_SIZE - MOE_SIZE_DISP, MOE_SIZE + MOE_SIZE_DISP);
    return CGSizeMake(size, size);
}
-(NSTimeInterval) randomSpawnInterval{
    return randomInRange(minSpawnInterval, maxSpawnInterval);
}
-(NSTimeInterval) randomRotationInterval{
    return randomInRange(MIN_ROTATION_INTERVAL, MAX_ROTATION_INTERVAL);
}
-(NSTimeInterval) randomFloatingInterval{
    return randomInRange(MIN_FLOAT_INTERVAL, MAX_FLOAT_INTERVAL);
}
-(GMPosition) randomPositionExcluding:(GMPosition) excluded{
    GMPosition pos = excluded;
    while (pos == excluded)
        pos = random(GM_POS_COUNT);
    return pos;
}
-(CGPoint) randomPointForPosition:(GMPosition) pos{
    switch (pos) {
        case GM_POS_TOP:
            return CGPointMake(randomInRange(moeSize.width, target.frame.size.width - moeSize.width), -moeSize.height);
        case GM_POS_RIGHT:
            return CGPointMake(target.frame.size.width, randomInRange(moeSize.height, target.frame.size.height - moeSize.height));
        case GM_POS_DOWN:
            return CGPointMake(randomInRange(moeSize.width, target.frame.size.width - moeSize.width), target.frame.size.height);
        default:
        case GM_POS_LEFT:
            return CGPointMake(-moeSize.width, randomInRange(moeSize.height, target.frame.size.height - moeSize.height));
    }
}


-(UIImageView*) generateRandomFloatingMoe{
    NSInteger rnd = arc4random() % [GMMoeFace availableMoeFaces];
    return [self generateFloatingMoeAtIndex:rnd];
}

-(UIImageView*) generateFloatingMoeAtIndex:(NSInteger)index{
    UIImage* img = [UIImage imageNamed:[GMMoeFace moeFaceAtIndex:index].avatar];
    UIImageView* imageView = [[UIImageView alloc] initWithImage:img];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
//    UIButton* button = [UIButton new];
//    [button setImage:img forState:UIControlStateNormal];
//    button.imageView.contentMode = UIViewContentModeScaleAspectFit;
//    [button addTarget:self action:@selector(moePressed:) forControlEvents:UIControlEventTouchDown];
    return imageView;
}



@end
