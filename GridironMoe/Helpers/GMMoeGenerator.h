//
//  GMMoeGenerator.h
//  GridironMoe
//
//  Created by Adya on 10/2/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

// Helps to generate floating moes

@interface GMMoeGenerator : NSObject


+(void) setEnabled:(BOOL)isEnabled;
+(BOOL) isEnabled;
+(void) startSpawnOnView:(UIView*) targetView;
+(void) setMaxMoesAmount:(int)maxMoes;
+(void) setSpawnIntervalFrom:(int)from to:(int)to;
+(void) startSpawn;
+(void) pauseSpawn;
+(void) stopSpawn;
@end
