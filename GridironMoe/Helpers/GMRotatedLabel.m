//
//  GMRotatedLabel.m
//  GridironMoe
//
//  Created by Adya on 8/28/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMRotatedLabel.h"

@implementation GMRotatedLabel

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    CGContextRotateCTM(context, -(M_PI_2));
    
    //UIFont* systemFont17 = [UIFont systemFontOfSize:17.0];
    
    NSDictionary* attrs= @{NSFontAttributeName : self.font, NSForegroundColorAttributeName:self.textColor};
    
    CGSize textSize = [self.text sizeWithAttributes:attrs];
    CGFloat middleY = (self.bounds.size.width - textSize.height) / 2;
    CGFloat middleX = (-self.bounds.size.height - textSize.width) / 2;
    [self.text drawAtPoint:CGPointMake(middleX, middleY) withAttributes:attrs];
    
    CGContextRestoreGState(context);
}

@end
