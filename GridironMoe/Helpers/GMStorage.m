//
//  GMStorage.m
//  GridironMoe
//
//  Created by Adya on 8/19/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMStorage.h"
#import "GMRequestManager.h"
#import "GMStrategy.h"
#import "GMMoeFace.h"

#define PLIST_TYPE @"plist"

#define PLIST_STRATEGIES @"strategies"
#define PLIST_MOES @"moes"
#define PLIST_MOE_MESSAGES @"moe_messages"

#define KEY_STRATEGIES_OFFENSE @"offense"
#define KEY_STRATEGIES_DEFESNSE @"defense"

#define KEY_STRATEGY_NAME @"Name"
#define KEY_STRATEGY_DETAIL @"Detail"

#define KEY_MOES_NAMES @"names"
#define KEY_MOES_AVATARS @"avatars"
#define KEY_MOES_TOP_BARS @"top_bars"

@implementation GMStorage

@synthesize isLoaded = _isLoaded;

-(void) preloadStorageWithCompletion:(void (^)(BOOL, TSError *))completion{
    [self loadStrategies];
    [self loadAvailableMoes];
    [self loadMoeMessages];
    [[GMRequestManager sharedManager] requestGetTeamMapWithResponseCallback:^(BOOL success, NSObject *object, TSError *error) {
        if (success){
            _isLoaded = YES;
            [self putValue:object forKey:STORAGE_DICTIONARY_TEAMS];
        }
        if (completion)
            completion(success, error);
    }];
}

-(void) loadStrategies{
    NSDictionary* strategies  = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:PLIST_STRATEGIES ofType:PLIST_TYPE]];
    NSArray* offenseDic = [strategies objectForKey:KEY_STRATEGIES_OFFENSE];
    NSArray* defenseDic = [strategies objectForKey:KEY_STRATEGIES_DEFESNSE];
    
    NSMutableArray* offense = [NSMutableArray new];
    for (NSDictionary* dic in offenseDic) {
        GMStrategy* strategy = [GMStrategy new];
        strategy.name = [dic objectForKey:KEY_STRATEGY_NAME];
        strategy.details = [dic objectForKey:KEY_STRATEGY_DETAIL];
        [offense addObject:strategy];
    }
    
    NSMutableArray* defense = [NSMutableArray new];
    for (NSDictionary* dic in defenseDic) {
        GMStrategy* strategy = [GMStrategy new];
        strategy.name = [dic objectForKey:KEY_STRATEGY_NAME];
        strategy.details = [dic objectForKey:KEY_STRATEGY_DETAIL];
        [defense addObject:strategy];
    }
    
    [self putValue:offense forKey:STORAGE_ARRAY_OFFENSE_STRATEGIES];
    [self putValue:defense forKey:STORAGE_ARRAY_DEFENSE_STRATEGIES];
}

-(void) loadAvailableMoes{
    NSDictionary* moes = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:PLIST_MOES ofType:PLIST_TYPE]];
    
    NSArray* moeNames = [moes objectForKey:KEY_MOES_NAMES];
    NSArray* moeAvatars = [moes objectForKey:KEY_MOES_AVATARS];
    NSArray* moeTopBars = [moes objectForKey:KEY_MOES_TOP_BARS];

    NSMutableArray* moeFaces = [NSMutableArray new];
    for (NSInteger i = 0; i < moeNames.count; ++i) {
        GMMoeFace* face = [GMMoeFace new];
        face.avatar = [moeAvatars objectAtIndex:i];
        face.topBar = [moeTopBars objectAtIndex:i];
        face.name = [moeNames objectAtIndex:i];
        face.description = face.name;
        [moeFaces addObject:face];
    }
    [self putValue:moeFaces forKey:STORAGE_ARRAY_AVAILABLE_MOE];
}

-(void) loadMoeMessages{
    NSArray* messages = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:PLIST_MOE_MESSAGES ofType:PLIST_TYPE]];
    [self putValue:messages forKey:STORAGE_ARRAY_MOE_MESSAGES];
}

@end
