//
//  GMBaseViewController.h
//  GridironMoe
//
//  Created by Adya on 8/10/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "TSViewController.h"
#import "SWRevealViewController.h"
#import "TSShareManager.h"

#ifndef _VIEW_CONTROLLER_IDS
#define _VIEW_CONTROLLER_IDS
    #define GM_QUIZ_VIEW_CONTROLLER_ID @"twoMinsDrillsNavController"
    #define GM_GAME_VIEW_CONTROLLER_ID @"gameNavController"

#endif



@interface GMBaseViewController : TSViewController <SWRevealViewControllerDelegate, TSShareDelegate>
-(void) updateMoeFace;
-(void) showSideMenu;
-(void) hideSideMenu;
-(void) showViewControllerWithIdentifier:(NSString*) identifier;

@property BOOL enableFloatingMoes;

@end
