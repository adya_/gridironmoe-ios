//
//  GMBaseViewController.m
//  GridironMoe
//
//  Created by Adya on 8/10/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMBaseViewController.h"
#import "TSNotifier.h"
#import "GMGameViewController.h"
#import "GMUserManager.h"
#import "GMMoeGenerator.h"
#import "MBProgressHUD.h"
#import "TSUtils.h"


@interface GMBaseViewController () 
    @property IBOutlet UIButton* moeButton;

@end

@implementation GMBaseViewController{
    BOOL _enableFloatingMoes;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.enableFloatingMoes = YES;
    [TSShareManager setShareSocialDelegate:self];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self attachRevealViewController];
    [self updateMoeFace];
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [TSNotifier hideProgressOnView:self.view];
    self.enableFloatingMoes = NO;
}

-(void) attachRevealViewController{
    if (self.revealViewController){
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.delegate = self;
        self.revealViewController.rearViewRevealWidth = 240;
    }
}

-(BOOL) enableFloatingMoes{
    return _enableFloatingMoes;
}

-(void) setEnableFloatingMoes:(BOOL)enableFloatingMoes{
    _enableFloatingMoes = enableFloatingMoes;
    if (enableFloatingMoes){
        [GMMoeGenerator setMaxMoesAmount:1];
        [GMMoeGenerator startSpawnOnView:self.view];
    }
    else
        [GMMoeGenerator stopSpawn];
}

-(void) showSideMenu{
    SWRevealViewController *controller = self.revealViewController;
    if (controller){
        [controller setFrontViewPosition:FrontViewPositionRight animated:YES];
    }
}

-(void) updateMoeFace{
    if (self.moeButton)
        [self.moeButton setImage:[GMUserManager sharedManager].moeFaceImage forState:UIControlStateNormal];
}

- (IBAction) sideMenuClick:(id) sender{
    [self showSideMenu];
}

- (IBAction) playGameClick:(id) sender{
    [self showViewControllerWithIdentifier:GM_GAME_VIEW_CONTROLLER_ID];
}

-(void) showViewControllerWithIdentifier:(NSString*) identifier{
    SWRevealViewController *controller = self.revealViewController;
    if (controller){
        UIViewController* dest = [controller.storyboard instantiateViewControllerWithIdentifier:identifier];
        if (dest)
            [controller setFrontViewController:dest animated:YES];
    }
}

-(void) hideSideMenu{
    SWRevealViewController *controller = self.revealViewController;
    if (controller){
       [controller setFrontViewPosition:FrontViewPositionLeft animated:YES];
    }
}

-(void) revealControllerPanGestureBegan:(SWRevealViewController *)revealController{
    if (_enableFloatingMoes) [GMMoeGenerator stopSpawn];
}

-(void) revealControllerPanGestureEnded:(SWRevealViewController *)revealController{
    if (_enableFloatingMoes) [GMMoeGenerator startSpawnOnView:self.view];
}

-(void) revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position{
    if (position == FrontViewPositionLeft)
        [self updateMoeFace];
}

-(void) revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position{

    if (position == FrontViewPositionLeft){
        [self setEnabled:YES inHierarchy:revealController.frontViewController.view.subviews];
        [self setEnableFloatingMoes:YES];
    }
    else{
        [self setEnabled:NO inHierarchy:revealController.frontViewController.view.subviews];
        [self setEnableFloatingMoes:NO];
    }
}

-(void) setEnabled:(BOOL)enabled inHierarchy:(NSArray*) views{
    [views enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UIControl class]])
            [(UIControl*)obj setEnabled:enabled];
        else if ([obj isKindOfClass:[UIPickerView class]])
            [(UIPickerView*)obj setUserInteractionEnabled:enabled];
        else if ([obj isKindOfClass:[UIScrollView class]])
            [(UIScrollView*)obj setUserInteractionEnabled:enabled];
        else if ([obj isKindOfClass:[UIView class]])
            [self setEnabled:enabled inHierarchy:[(UIView*)obj subviews]];
    }];
}

-(NSString*) shareSocialMessageForService:(NSString *)service{
    return NSLocalizedString(@"GM_SHARE_DEFAULT_TEXT", nil);
}

- (IBAction)shareTwitter:(id)sender {
    [TSShareManager shareWithService:TSSocialShareTwitter onViewController:self];
}

- (IBAction)shareFacebook:(id)sender {
    [TSShareManager shareWithService:TSSocialShareFacebook onViewController:self];
}

-(void) onShareViewDidDisappearWithService:(NSString *)service shared:(BOOL)shared{
    NSString* msg;
    if (shared){
        if ([TSSocialShareTwitter isEqualToString:service])
            msg = @"Your tweet was successfuly posted!";
        else if ([TSSocialShareFacebook isEqualToString:service])
            msg = @"Your message was successfuly posted on Facebook!";
        
        if (msg){
            [TSNotifier notify:msg];
        }
    }
}



@end
