//
//  GMGame.m
//  GridironMoe
//
//  Created by Adya on 8/19/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMGame.h"
#import "GMTeam.h"
#import "TSUtils.h"

static NSString* const kGameLive = @"inprogress";
static NSString* const kGameCreated = @"created";
static NSString* const kGameScheduled = @"scheduled";
static NSString* const kGameCompleted = @"complete";
static NSString* const kGameClosed = @"closed";

@implementation GMGame

@synthesize gameId, homeTeam, awayTeam, status;

-(id) initWithJSON:(NSDictionary *)jsonObject{
    self = [self init];
    gameId = nonNull([jsonObject objectForKey:@"id"]);
    homeTeam = [GMTeam teamWithTeamId:nonNull([jsonObject objectForKey:@"home_team"])];
    awayTeam = [GMTeam teamWithTeamId:nonNull([jsonObject objectForKey:@"away_team"])];
    status = GM_GAME_UNKNOWN;
    return self;
}

+(GMGameStatus) statusFromString:(NSString*)statusString{
    if ([statusString isEqualToString:kGameLive])
        return GM_GAME_LIVE;
    else if ([statusString isEqualToString:kGameCreated] || [statusString isEqualToString:kGameScheduled])
        return GM_GAME_SCHEDULED;
    else if ([statusString isEqualToString:kGameCompleted] || [statusString isEqualToString:kGameClosed])
        return GM_GAME_CLOSED;
    else return GM_GAME_UNKNOWN;
}

-(BOOL) isEqual:(id)object{
    return object && [object isKindOfClass:[GMGame class]] && [self.gameId isEqualToString:[object gameId]];
}

@end
