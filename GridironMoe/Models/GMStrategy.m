//
//  GMStrategy.m
//  GridironMoe
//
//  Created by Adya on 8/26/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMStrategy.h"
#import "GMStorage.h"

@implementation GMStrategy

@synthesize name, details;

+(GMStrategy*) strategyNamed:(NSString *)name{
    GMStrategy* res = [self strategyNamed:name fromArray:[[GMStorage sharedStorage] getValueForKey:STORAGE_ARRAY_DEFENSE_STRATEGIES]];
    if (!res)
        res = [self strategyNamed:name fromArray:[[GMStorage sharedStorage] getValueForKey:STORAGE_ARRAY_OFFENSE_STRATEGIES]];
    return res;
}

+(GMStrategy*) strategyNamed:(NSString *)name fromArray:(NSArray*) strategies{
    __block GMStrategy* res = nil;
    [strategies enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        GMStrategy* str = (GMStrategy*)obj;
        if ([str.name isEqualToString:name]){
            res = str;
            *stop = YES;
        }
    }];
    return res;
}

@end
