//
//  GMQuestion.m
//  GridironMoe
//
//  Created by Adya on 8/20/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMQuestion.h"
#import "TSUtils.h"
#import "GMAppDelegate.h"

#define APP_DELEGATE ((GMAppDelegate*)[UIApplication sharedApplication].delegate)

#define QUESTION_ENTITY_NAME @"Question"

#define MIN_ID @"minId"
#define MAX_ID @"maxId"

@implementation GMQuestion

@dynamic question;
@dynamic wrongAnswer1;
@dynamic wrongAnswer2;
@dynamic answer;
@dynamic responseToWrongAnswer1;
@dynamic responseToWrongAnswer2;
@dynamic questionId;
@dynamic superbowl;

//  Creates a new Question.
+ (GMQuestion*) newQuestion {
    GMQuestion *question = [NSEntityDescription insertNewObjectForEntityForName:QUESTION_ENTITY_NAME inManagedObjectContext:APP_DELEGATE.managedObjectContext];
    return question;
}

//  Returns Question with the given Question id.
+ (GMQuestion*) questionWithQuestionId:(NSNumber *) questionId {
    NSFetchRequest *fetchRequest = [NSFetchRequest new];
    [fetchRequest setEntity:[NSEntityDescription entityForName:QUESTION_ENTITY_NAME inManagedObjectContext:APP_DELEGATE.managedObjectContext]];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"questionId == %@", questionId]];
    NSError *error = nil;
    NSUInteger count = [APP_DELEGATE.managedObjectContext countForFetchRequest:fetchRequest error:&error];
    if (!error && count > 0){
        NSArray *result = [APP_DELEGATE.managedObjectContext executeFetchRequest:fetchRequest error:nil];
        GMQuestion *question = (GMQuestion*)[result objectAtIndex:0];
        return question;
    } else {
        return nil;
    }
}

//  creates Question if not exists and then returns.
+ (GMQuestion*) createQuestionIfNotExists:(NSNumber *) questionId {
    GMQuestion *question = [GMQuestion questionWithQuestionId:questionId];
    if(!question) {
        question = [GMQuestion newQuestion];
        question.questionId = questionId;
    }
    return question;
}

+ (GMQuestion*) createQuestionFromDictionary:(NSDictionary *) jsonObject {
    if (jsonObject && [jsonObject objectForKey:@"id"]) {
        GMQuestion *question = [GMQuestion createQuestionIfNotExists:[jsonObject objectForKey:@"id"]];
        question.questionId = [jsonObject objectForKey:@"id"];
        question.superbowl = nonNull([jsonObject objectForKey:@"superbowl"]);
        question.question = nonNull([jsonObject objectForKey:@"questions"]);
        question.answer = nonNull([jsonObject objectForKey:@"answers"]);
        question.wrongAnswer1 = nonNull([jsonObject objectForKey:@"wrongA1"]);
        question.wrongAnswer2 = nonNull([jsonObject objectForKey:@"wrongA2"]);
        question.responseToWrongAnswer1 = nonNull([jsonObject objectForKey:@"responsetoWrongAnswer1"]);
        question.responseToWrongAnswer2 = nonNull([jsonObject objectForKey:@"responsetoWrongAnswer2"]);
        [APP_DELEGATE.managedObjectContext save:nil];
        return question;
    } else {
        return nil;
    }
}

+(void) setIdRange:(NSNumber*)min max:(NSNumber*)max{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:min forKey:MIN_ID];
    [defaults setObject:max forKey:MAX_ID];
}

+(NSNumber*) minId{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:MIN_ID];
}

+(NSNumber*) maxId{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:MAX_ID];
}

+(int) count{
    int min = [[GMQuestion minId] intValue];
    int max = [[GMQuestion maxId] intValue];
    return max - min;
}

+(void) clearAllQuestions{
    NSManagedObjectContext * context = APP_DELEGATE.managedObjectContext;
    NSFetchRequest * fetch = [NSFetchRequest new];
    [fetch setEntity:[NSEntityDescription entityForName:QUESTION_ENTITY_NAME inManagedObjectContext:context]];
    NSArray * result = [context executeFetchRequest:fetch error:nil];
    for (id question in result)
        [context deleteObject:question];
    [APP_DELEGATE.managedObjectContext save:nil];
}


@end
