//
//  GMGameSchedule.h
//  GridironMoe
//
//  Created by Adya on 8/27/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TSJSONParselable.h"
#import "GMGame.h"

@interface GMGameSchedule : GMGame
@property NSInteger scheduleId;


@property NSDate* date;
@property (readonly) NSString* dateString;
@property (readonly) NSString* timeString;
@end
