//
//  GMGameLive.h
//  GridironMoe
//
//  Created by Adya on 8/31/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMGame.h"

/*
 "id": "26c58df7-fcb9-4bc9-9981-5b793cdad53a",
 "home_team": {
 "id": "NO",
 "name": "Saints",
 "points": 3
 },
 "away_team": {
 "id": "HOU",
 "name": "Texans",
 "points": 10
 },
 "quarter_number": 2,
 "clock": "11:21",
 "down": 2,
 "yfd": 3,
 "offense": "NO",
 "updated": "2015-08-30T21:01:00+00:00",
 "seasonYear": 2015,
 "downId": "cb774d4f-de22-4adc-a072-75e79e132227",
 "summary": null,
 "event": "play"
 */
/*
 Pausing:
 teamtimeout
 injurytimeout
 tvtimeout
 quarterend
 twominuteswarning
 gameover
 
 Scoring:
 touchdown
 extrapoint
 conversion
 fieldgoal
 safety

 "Touchdown!"
 "Extra Point!"
 "Conversion!"
 "Field Goal!"
 "Safety!"
 
 */



typedef NS_ENUM(NSUInteger, GMGameEvent) {
    GM_GAME_EVENT_NONE,
    
    GM_GAME_EVENT_TIMEOUT,
    GM_GAME_EVENT_QUARTER_END,
    GM_GAME_EVENT_GAME_OVER,
    
    GM_GAME_EVENT_TOUCHDOWN,
    GM_GAME_EVENT_EXTRA_POINT,
    GM_GAME_EVENT_CONVERSION,
    GM_GAME_EVENT_FIELD_GOAL,
    GM_GAME_EVENT_SAFETY
    
};

@interface GMGameLive : GMGame

@property int homeTeamPoints;
@property int awayTeamPoints;
@property NSString* clock;
@property int down;
@property GMTeam* offenseTeam;
@property GMTeam* defenseTeam;
@property int quarter;
@property int yards;
@property GMTeam* ballSideTeam;
@property int ballPosition;

@property NSString* downId;
@property GMGameEvent event;
@property GMGameEvent scoreEvent;
@property NSString* summary;
@property NSString* scoreSummary;

+(BOOL) isTimeoutEvent:(GMGameEvent) event;
+(BOOL) isScoringEvent:(GMGameEvent) event;

@end

