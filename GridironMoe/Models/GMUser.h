//
//  GMUser.h
//  GridironMoe
//
//  Created by Adya on 8/18/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "TSJSONParselable.h"

@class GMScore;
@class GMTeam;

@interface GMUser : NSObject

@property (readonly) NSString* username;
@property (readonly) NSString* password;

@property NSString* avatarURL;
@property NSString* email;

@property (readonly) NSString* rankSeasonTitle;
@property (readonly) NSString* rankGameTitle;

@property GMScore* total;
@property GMScore* defence;
@property GMScore* offence;

@property GMTeam* team;

-(id) initWithUsername:(NSString*)username andPassword:(NSString*) password;

-(id) initWithUsername:(NSString*)username andPassword:(NSString*) password email:(NSString*) userEmail andAvatarURL:(NSString*) url;
@end
