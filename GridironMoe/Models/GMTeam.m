//
//  GMTeam.m
//  GridironMoe
//
//  Created by Adya on 8/19/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMTeam.h"
#import "GMStorage.h"
#import "TSUtils.h"

//static int const GM_TEAM_UNKNOWN = -1;

@implementation GMTeam

@synthesize name = _name, teamId = _teamId, locationId = _locationId, locationTitle = _locationTitle;

-(id) initWithJSON:(NSDictionary *)jsonObject{
    NSString* name = nonNull([jsonObject objectForKey:@"teamName"]);
    NSString* teamId = nonNull([jsonObject objectForKey:@"teamId"]);
    int locationId = [nonNull([jsonObject objectForKey:@"location"]) intValue];
    NSString* location = nonNull([jsonObject objectForKey:@"locationName"]);
    return [self initWithName:name teamId:teamId locationId:locationId andLocationName:location];
}

-(id) initWithName:(NSString*)name teamId:(NSString*) teamId locationId:(int)locationId andLocationName:(NSString*) location{
    self = [self init];
    _name = name;
    _teamId = teamId;
    _locationId = locationId;
    _locationTitle = location;
    //[[[GMStorage sharedStorage] getValueForKey:STORAGE_ARRAY_TEAMS_LOCATIONS_TITLES] objectAtIndex:(_locationId-1)];
    return self;
}

+(GMTeam*) teamWithLocationId:(int)locationId{
    NSDictionary* teams = [[GMStorage sharedStorage] getValueForKey:STORAGE_DICTIONARY_TEAMS];
    __block GMTeam* tm;
    [teams enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if (((GMTeam*)obj).locationId == locationId){
            tm = obj;
            *stop = YES;
        }
    }];
    return tm;
}

+(GMTeam*) teamWithTeamId:(NSString *)teamId{
    NSDictionary* teams = [[GMStorage sharedStorage] getValueForKey:STORAGE_DICTIONARY_TEAMS];
    GMTeam* tm = [teams objectForKey:teamId];
    return tm;
}

-(NSString*) description{
    return [NSString stringWithFormat:@"%@ : %@ (%@)", _teamId, _name, _locationTitle];
}

-(BOOL) isEqual:(id)object{
    GMTeam* other = (GMTeam*)object;
    if (!other) return NO;
    if (self == other) return YES;
    return [self.teamId isEqualToString:other.teamId];
}

@end
