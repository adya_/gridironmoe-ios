//
//  GMGameSchedule.m
//  GridironMoe
//
//  Created by Adya on 8/27/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMGameSchedule.h"
#import "TSUtils.h"
#import "GMTeam.h"

@implementation GMGameSchedule

@synthesize date, scheduleId;


-(id) initWithJSON:(NSDictionary *)jsonObject{
    self = [self init];
    NSNumber* timestamp = nonNull([jsonObject objectForKey:@"scheduled"]);
    self.date = [TSUtils dateFromUnixTimestamp:([timestamp  doubleValue]/1000)];
    self.scheduleId = [nonNull([jsonObject objectForKey:@"id"]) integerValue];
    self.gameId = nonNull([jsonObject objectForKey:@"game_id"]);
    self.homeTeam = [GMTeam teamWithTeamId:nonNull([jsonObject objectForKey:@"home"])];
    self.awayTeam = [GMTeam teamWithTeamId:nonNull([jsonObject objectForKey:@"away"])];
    self.status = [GMGame statusFromString:nonNull([jsonObject objectForKey:@"status"])];
    return self;
}

-(NSString*) dateString{
    return [TSUtils convertDate:date toStringWithFormat:@"MM.dd.yy" withTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"EST"]];
}

-(NSString*) timeString{
    return [TSUtils convertDate:date toStringWithFormat:@"hh:mm a 'EST'" withTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"EST"]];
}

@end
