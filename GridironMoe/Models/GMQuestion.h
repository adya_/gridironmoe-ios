//
//  GMQuestion.h
//  GridironMoe
//
//  Created by Adya on 8/20/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface GMQuestion : NSManagedObject

//"id": 5976,
//"superbowl": "SB I (1), Jan. 14, 1967",
//"questions": "Who Played?",
//"answers": "Green Bay-Kansas City",
//"wrongA1": "Green Bay - Oakland",
//"wrongA2": "New York - Baltimore",
//"responsetoWrongAnswer1": "This was the 1968 game",
//"responsetoWrongAnswer2": "This was the 1969 game"

@property (nonatomic, retain) NSString * question;
@property (nonatomic, retain) NSString * wrongAnswer1;
@property (nonatomic, retain) NSString * wrongAnswer2;
@property (nonatomic, retain) NSString * answer;
@property (nonatomic, retain) NSString * responseToWrongAnswer1;
@property (nonatomic, retain) NSString * responseToWrongAnswer2;
@property (nonatomic, retain) NSNumber * questionId;
@property (nonatomic, retain) NSString * superbowl;


+(NSNumber*) minId;
+(NSNumber*) maxId;
+(int) count;

+ (GMQuestion *)newQuestion;
+ (GMQuestion *)questionWithQuestionId:(NSNumber *)questionId;
+ (GMQuestion *)createQuestionIfNotExists:(NSNumber *)questionId;
+ (GMQuestion *)createQuestionFromDictionary:(NSDictionary *)jsonObject;

+ (void) setIdRange:(NSNumber*)min max:(NSNumber*)max;
+ (void)clearAllQuestions;
@end
