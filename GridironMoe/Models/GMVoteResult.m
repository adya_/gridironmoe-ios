//
//  GMVoteResult.m
//  GridironMoe
//
//  Created by Adya on 9/3/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

/*
 "data": {
 "offensePlayPoints": 25,
 "defensePlayPoints": 30,
 "offensePlayerPct": 43,
 "defensePlayerPct": 50,
 "topDefensePlayPct": [
 {
 "pct": 100,
 "name": "Blitz"
 }
 ],
 "topOffensePlayPct": [
 {
 "pct": 100,
 "name": "Bootleg"
 }
 ]
 }
 */

#import "GMVoteResult.h"
#import "TSUtils.h"
#import "GMStrategy.h"

@implementation GMVoteResult

@synthesize offenseVote, offenseAgreed, offensePoints, offenseTopVote, offenseTopVoteRate;
@synthesize defenseVote, defenseAgreed, defensePoints, defenseTopVote, defenseTopVoteRate;
@synthesize totalPoints;

-(id) initWithJSON:(NSDictionary *)jsonObject{
    self = [self init];
    offensePoints = [nonNull([jsonObject objectForKey:@"offensePlayPoints"]) intValue];
    defensePoints = [nonNull([jsonObject objectForKey:@"defensePlayPoints"]) intValue];
    totalPoints = offensePoints + defensePoints;
    
    offenseAgreed = [nonNull([jsonObject objectForKey:@"offensePlayerPct"]) intValue];
    defenseAgreed = [nonNull([jsonObject objectForKey:@"defensePlayerPct"]) intValue];
    
    NSDictionary* topOff = nonNull([jsonObject objectForKey:@"topOffensePlayPct"]);
    NSDictionary* topDef = nonNull([jsonObject objectForKey:@"topDefensePlayPct"]);
    if (topOff && topOff.count > 0){
        offenseTopVote = [GMStrategy strategyNamed:nonNull([topOff objectForKey:@"name"])];
        offenseTopVoteRate = [nonNull([topOff objectForKey:@"pct"]) intValue];
    }
    if (topDef && topDef.count > 0){
        defenseTopVote = [GMStrategy strategyNamed:nonNull([topDef objectForKey:@"name"])];
        defenseTopVoteRate = [nonNull([topDef objectForKey:@"pct"]) intValue];
    }
    return self;
}

@end
