//
//  GMVoteResult.h
//  GridironMoe
//
//  Created by Adya on 9/3/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TSJSONParselable.h"

@class GMStrategy;

@interface GMVoteResult : NSObject <TSJSONParselable>

@property GMStrategy* offenseVote;
@property int offensePoints;
@property int offenseAgreed;
@property GMStrategy* offenseTopVote;
@property int offenseTopVoteRate;


@property GMStrategy* defenseVote;
@property int defensePoints;
@property int defenseAgreed;
@property GMStrategy* defenseTopVote;
@property int defenseTopVoteRate;

@property int totalPoints;

@end
