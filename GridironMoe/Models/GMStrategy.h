//
//  GMStrategy.h
//  GridironMoe
//
//  Created by Adya on 8/26/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GMStrategy : NSObject

@property NSString* name;
@property NSString* details;

+(GMStrategy*) strategyNamed:(NSString*)name;


@end
