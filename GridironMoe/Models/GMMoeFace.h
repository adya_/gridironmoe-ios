//
//  GMMoeFace.h
//  GridironMoe
//
//  Created by Adya on 10/2/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GMMoeFace : NSObject

@property NSString* avatar;
@property NSString* topBar;
@property NSString* name;
@property NSString* description;

+(NSUInteger) availableMoeFaces;
+(GMMoeFace*) moeFaceNamed:(NSString*)name;
+(GMMoeFace*) moeFaceAtIndex:(NSInteger)index;
+(NSInteger) indexOfMoeFace:(GMMoeFace*)moeFace;
+(NSInteger) indexOfMoeFaceNamed:(NSString*)name;
+(BOOL) isValidMoeName:(NSString*)name;
@end
