//
//  GMScore.m
//  GridironMoe
//
//  Created by Adya on 8/19/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMScore.h"
#import "TSUtils.h"

int const GM_SCORE_UNRANKED = -1;

@implementation GMScore

@synthesize score = _score, rank = _rank;


-(id) initWithScore:(int)score andRank :(int)rank{
    self = [self init];
    _score = score;
    _rank = rank;
    return self;
}
+(GMScore*) scoreFromString:(NSString *)string{
    GMScore* sc = [GMScore new];
    string = trim(string);
    NSArray* comp = [string componentsSeparatedByString:@"|"];
    sc.score = [[comp objectAtIndex:0] intValue];
    sc.rank = [[comp objectAtIndex:1] intValue];
    return sc;
}
@end
