//
//  GMMoeFace.m
//  GridironMoe
//
//  Created by Adya on 10/2/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMMoeFace.h"
#import "GMStorage.h"

@implementation GMMoeFace

@synthesize avatar, topBar, name, description;

+(NSUInteger) availableMoeFaces{
    return ((NSArray*)[[GMStorage sharedStorage] getValueForKey:STORAGE_ARRAY_AVAILABLE_MOE]).count;
}

+(GMMoeFace*) moeFaceNamed:(NSString *)name{
    NSArray* moes = [[GMStorage sharedStorage] getValueForKey:STORAGE_ARRAY_AVAILABLE_MOE];
    __block GMMoeFace* face = nil;
    [moes enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([((GMMoeFace*)obj).name isEqualToString:name]){
            face = obj;
            *stop = YES;
        }
    }];
    return face;
}

+(GMMoeFace*) moeFaceAtIndex:(NSInteger)index{
    NSArray* moes = [[GMStorage sharedStorage] getValueForKey:STORAGE_ARRAY_AVAILABLE_MOE];
    if (index < 0 || index >= moes.count)
        return nil;
    return [moes objectAtIndex:index];
}

+(NSInteger) indexOfMoeFace:(GMMoeFace *)moeFace{
    NSArray* moes = [[GMStorage sharedStorage] getValueForKey:STORAGE_ARRAY_AVAILABLE_MOE];
    return [moes indexOfObject:moeFace];
}

+(NSInteger) indexOfMoeFaceNamed:(NSString *)name{
    return [self indexOfMoeFace:[self moeFaceNamed:name]];
}


-(BOOL) isEqual:(id)object{
    GMMoeFace* other = (GMMoeFace*)object;
    
    if (!other) return NO;
    if (self == other) return YES;
    
    return [self.name isEqualToString:other.name];
}

+(BOOL) isValidMoeName:(NSString *)name{
    return [self moeFaceNamed:name] != nil;
}

@end
