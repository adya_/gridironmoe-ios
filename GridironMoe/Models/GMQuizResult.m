//
//  GMQuiz.m
//  GridironMoe
//
//  Created by Adya on 8/20/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMQuizResult.h"
#import "TSUtils.h"

@implementation GMQuizResult

@synthesize right, total, score, playedDate, username, userAvatar;

-(id) initWithJSON:(NSDictionary *)jsonObject{
    self = [self init];
    right = [nonNull([jsonObject objectForKey:@"right"]) intValue];
    total = [nonNull([jsonObject objectForKey:@"attempted"]) intValue];
    score = [nonNull([jsonObject objectForKey:@"score"]) intValue];
    
    playedDate = nonNull([jsonObject objectForKey:@"playedDate"]);
    playedDate = [TSUtils formatDateString:playedDate withFormat:@"yyyy/MM/dd'T'HH:mm:ss" andTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"] toFormat:@"MM/dd/yyyy HH:mm" andTimeZone:[NSTimeZone defaultTimeZone]];
    
    username = nonNull([jsonObject objectForKey:@"username"]);
    return self;
}
@end
