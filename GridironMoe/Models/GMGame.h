//
//  GMGame.h
//  GridironMoe
//
//  Created by Adya on 8/19/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TSJSONParselable.h"

typedef  NS_ENUM(NSInteger, GMGameStatus) {
    GM_GAME_UNKNOWN = -1,
    GM_GAME_SCHEDULED,
    GM_GAME_LIVE,
    GM_GAME_CLOSED
};

@class GMTeam;

@interface GMGame : NSObject <TSJSONParselable>
@property GMTeam* homeTeam;
@property GMTeam* awayTeam;
@property NSString* gameId;
@property GMGameStatus status;

+(GMGameStatus) statusFromString:(NSString*)statusString;

@end
