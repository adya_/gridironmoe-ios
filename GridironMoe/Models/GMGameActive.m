//
//  GMGameActive.m
//  GridironMoe
//
//  Created by Adya on 8/30/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMGameActive.h"
#import "GMTeam.h"
#import "TSUtils.h"

@implementation GMGameActive

-(id) initWithJSON:(NSDictionary *)jsonObject{
    self = [self init];
    NSNumber* timestamp = nonNull([jsonObject objectForKey:@"time"]);
    self.date = [TSUtils dateFromUnixTimestamp:[timestamp  doubleValue]/1000];
    self.gameId = nonNull([jsonObject objectForKey:@"id"]);
    self.homeTeam = [GMTeam teamWithTeamId:nonNull([jsonObject objectForKey:@"home_team"])];
    self.awayTeam = [GMTeam teamWithTeamId:nonNull([jsonObject objectForKey:@"away_team"])];
    self.status = GM_GAME_LIVE;
    return self;
}

@end
