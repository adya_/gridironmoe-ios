//
//  GMScore.h
//  GridironMoe
//
//  Created by Adya on 8/19/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

extern int const GM_SCORE_UNRANKED;

@interface GMScore : NSObject

@property int score;
@property int rank;

-(id) initWithScore:(int)score andRank:(int) rank;

+(GMScore*) scoreFromString:(NSString*)string;


@end
