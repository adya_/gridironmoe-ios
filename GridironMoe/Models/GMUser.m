//
//  GMUser.m
//  GridironMoe
//
//  Created by Adya on 8/18/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMUser.h"
#import "GMScore.h"

#define GAME_RANK_1 600
#define GAME_RANK_2 1200
#define GAME_RANK_3 1800
#define GAME_RANK_4 2400
#define GAME_RANK_5 3000
#define GAME_RANK_6 3600
#define GAME_RANK_7 4200
#define GAME_RANK_8 4800

#define SEASON_RANK_1 10000
#define SEASON_RANK_2 120000
#define SEASON_RANK_3 240000
#define SEASON_RANK_4 420000
#define SEASON_RANK_5 480000


@implementation GMUser

@synthesize username, password, total, defence, offence, team, avatarURL, email;

-(id) initWithUsername:(NSString*)_username andPassword:(NSString*) _password{
    return [self initWithUsername:_username andPassword:_password email:nil andAvatarURL:nil];
}

-(id) initWithUsername:(NSString *)_username andPassword:(NSString *)_password email:(NSString *)_email andAvatarURL:(NSString *)url{
    self = [self init];
    username = _username;
    password = _password;
    email = _email;
    avatarURL = url;
    return self;
}
/*
 POINTS PER GAME
 Table: Points Total & Reward
 
 4800 or more I am Superbowl Bound!
 4200-4800 Finally! Conference Champion Level!
 3600-4200 Making it! Division Champion Level!
 3000-3600 OK, progress! I am Assistant Grade!
 2400-3000 Ha!! I am a Rising Star!
 1800-2400 It is what it is… I am Mid Pack
 1200-1800 On my way! I am Career Builder Level!
 600-1200 Starting out, not too bad! I am Learning Level!
 0-600 I should not quit my day job… Will keep playing!
 
 POINTS PER SEASON
 Table: Season Points Total & Reward
 480,000 or more I am Superbowl Bound!
 420,000-480,000 Finally! Conference Champion Level!
 240,000-420,000 Making it! Division Champion Level!
 120,000-240,000 I got the Wild Card!
 10,000-120,000 Yikes, Not Playoff Bound.
 0-10,000 OK,OK, Likely to be fired
 
 */

-(NSString*) rankGameTitle{
    int score = total.score;
    if (score <= GAME_RANK_1)
        return NSLocalizedString(@"GM_TITLE_GAME_RANK_1", @"600");
    else if(score <= GAME_RANK_2)
        return NSLocalizedString(@"GM_TITLE_GAME_RANK_2", @"1200");
    else if(score <= GAME_RANK_3)
        return NSLocalizedString(@"GM_TITLE_GAME_RANK_3", @"1800");
    else if(score <= GAME_RANK_4)
        return NSLocalizedString(@"GM_TITLE_GAME_RANK_4", @"2400");
    else if(score <= GAME_RANK_5)
        return NSLocalizedString(@"GM_TITLE_GAME_RANK_5", @"3000");
    else if(score <= GAME_RANK_6)
        return NSLocalizedString(@"GM_TITLE_GAME_RANK_6", @"3600");
    else if(score <= GAME_RANK_7)
        return NSLocalizedString(@"GM_TITLE_GAME_RANK_7", @"4200");
    else if(score <= GAME_RANK_8)
        return NSLocalizedString(@"GM_TITLE_GAME_RANK_8", @"4800");
    else
        return NSLocalizedString(@"GM_TITLE_GAME_RANK_MAX", @"4800+");
}


-(NSString*) rankSeasonTitle{
    int score = total.score;
    if (score <= SEASON_RANK_1)
        return NSLocalizedString(@"GM_TITLE_SEASON_RANK_1", @"10000");
    else if(score <= SEASON_RANK_2)
        return NSLocalizedString(@"GM_TITLE_SEASON_RANK_2", @"120000");
    else if(score <= SEASON_RANK_3)
        return NSLocalizedString(@"GM_TITLE_SEASON_RANK_3", @"240000");
    else if(score <= SEASON_RANK_4)
        return NSLocalizedString(@"GM_TITLE_SEASON_RANK_4", @"420000");
    else if(score <= SEASON_RANK_5)
        return NSLocalizedString(@"GM_TITLE_SEASON_RANK_5", @"480000");
    else
        return NSLocalizedString(@"GM_TITLE_SEASON_RANK_MAX", @"480000+");
}

@end
