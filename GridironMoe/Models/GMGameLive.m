//
//  GMGameLive.m
//  GridironMoe
//
//  Created by Adya on 8/31/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMGameLive.h"
#import "TSUtils.h"
#import "GMTeam.h"


static NSString* const kEventTeamTimeout = @"teamtimeout";
static NSString* const kEventInjuryTimeout = @"injurytimeout";
static NSString* const kEventTVTimeout = @"tvtimeout";
static NSString* const kEventTwoMinsWarning = @"twominuteswarning";
static NSString* const kEventQuarterEnd = @"quarterend";
static NSString* const kEventGameOver = @"gameover";

static NSString* const kEventTouchdown = @"touchdown";
static NSString* const kEventExtraPoint = @"extrapoint";
static NSString* const kEventConversion = @"conversion";
static NSString* const kEventFieldGoal = @"fieldgoal";
static NSString* const kEventSafety = @"safety";


@implementation GMGameLive

@synthesize homeTeamPoints, awayTeamPoints, clock, down, offenseTeam, defenseTeam, quarter, yards, ballSideTeam, ballPosition, downId, event, summary, scoreEvent, scoreSummary;

-(id) initWithJSON:(NSDictionary *)jsonObject{
    self = [self init];
    NSDictionary* live = nonNull([jsonObject objectForKey:@"live"]);
    self.gameId = nonNull([live objectForKey:@"id"]);
    NSDictionary* hTeamDic = nonNull([live objectForKey:@"home_team"]);
    NSDictionary* aTeamDic = nonNull([live objectForKey:@"away_team"]);
    self.homeTeam = [GMTeam teamWithTeamId:nonNull([hTeamDic objectForKey:@"id"])];
    self.awayTeam = [GMTeam teamWithTeamId:nonNull([aTeamDic objectForKey:@"id"])];
    self.homeTeamPoints = [nonNull([hTeamDic objectForKey:@"points"]) intValue];
    self.awayTeamPoints = [nonNull([aTeamDic objectForKey:@"points"]) intValue];
    self.offenseTeam = [GMTeam teamWithTeamId:nonNull([live objectForKey:@"team"])];
    self.defenseTeam = (self.offenseTeam == self.homeTeam ? self.awayTeam : self.homeTeam);
    self.clock = nonNull([live objectForKey:@"clock"]);
    self.clock = [self fixClock:self.clock];
    
    self.down = [nonNull([live objectForKey:@"down"]) intValue];
    self.yards = [nonNull([live objectForKey:@"yfd"]) intValue];
    self.ballPosition = [nonNull([live objectForKey:@"yard_line"]) intValue];
    self.ballSideTeam = [GMTeam teamWithTeamId:nonNull([live objectForKey:@"side"])];
    self.quarter = [nonNull([live objectForKey:@"quarter_number"]) intValue];
    self.downId = nonNull([live objectForKey:@"downId"]);
    self.summary = nonEmpty([live objectForKey:@"summary"]);
    self.event = [self parseEvent:nonEmpty([live objectForKey:@"event"])];
    self.status = [GMGame statusFromString:nonNull([jsonObject objectForKey:@"status"])];
    self.scoreEvent = [self parseEvent:nonEmpty([live objectForKey:@"score_type"])];
    self.scoreSummary = nonEmpty([live objectForKey:@"score_summary"]);
    return self;
}

-(GMGameEvent) parseEvent:(NSString*) eventString{
    if ([eventString isEqualToString:kEventTeamTimeout] ||
        [eventString isEqualToString:kEventTVTimeout] ||
        [eventString isEqualToString:kEventInjuryTimeout] ||
        [eventString isEqualToString:kEventTwoMinsWarning]) {
        return GM_GAME_EVENT_TIMEOUT;
    }
    else if ([eventString isEqualToString:kEventGameOver])
        return GM_GAME_EVENT_GAME_OVER;
    else if ([eventString isEqualToString:kEventQuarterEnd])
        return GM_GAME_EVENT_QUARTER_END;
    else if ([eventString isEqualToString:kEventTouchdown])
        return GM_GAME_EVENT_TOUCHDOWN;
    else if ([eventString isEqualToString:kEventExtraPoint])
        return GM_GAME_EVENT_EXTRA_POINT;
    else if ([eventString isEqualToString:kEventConversion])
        return GM_GAME_EVENT_CONVERSION;
    else if ([eventString isEqualToString:kEventFieldGoal])
        return GM_GAME_EVENT_FIELD_GOAL;
    else if ([eventString isEqualToString:kEventSafety])
        return GM_GAME_EVENT_SAFETY;
    else
        return GM_GAME_EVENT_NONE;
}

+(BOOL) isTimeoutEvent:(GMGameEvent)event{
    switch (event) {
        case GM_GAME_EVENT_TIMEOUT:
        case GM_GAME_EVENT_QUARTER_END:
        case GM_GAME_EVENT_GAME_OVER:
            return YES;
        default:
            return NO;
    }
}

+(BOOL) isScoringEvent:(GMGameEvent)event{
    switch (event) {
        case GM_GAME_EVENT_TOUCHDOWN:
        case GM_GAME_EVENT_EXTRA_POINT:
        case GM_GAME_EVENT_CONVERSION:
        case GM_GAME_EVENT_FIELD_GOAL:
        case GM_GAME_EVENT_SAFETY:
            return YES;
        default:
            return NO;
    }
}

-(NSString*) fixClock:(NSString*) clockString{
    NSArray* comp = [clockString componentsSeparatedByString:@":"];
    if (comp.count == 1)
        return [NSString stringWithFormat:@"00:%.2d", [comp[0] intValue]];
    else if (comp.count == 2)
        return [NSString stringWithFormat:@"%.2d:%.2d", [comp[0] intValue], [comp[1] intValue]];
    else return nil;
}
-(NSString*) fixSummaryClock:(NSString*) summaryString {
    NSArray* comp = [summaryString componentsSeparatedByString:@":"];
    if (comp.count == 0)
        return summaryString;
    
    if (comp.count == 2){
        NSArray* leftPart = [self processSummaryLeftPart:comp[0]];
        NSArray* rightPart = [self processSummaryRightPart:comp[1]];
        NSString* clockStr = [NSString stringWithFormat:@"%@:%@", leftPart[1], rightPart[1]];
        NSString* left = leftPart[0];
        left = (nonEmpty(left) ? [left stringByAppendingString:@" "] : left);
        NSString* right = rightPart[0];
        right = (nonEmpty(right) ? [@" " stringByAppendingString:right] : right);
        return [NSString stringWithFormat:@"%@%@%@", left, [self fixClock:clockStr], right];
    }
    else
        return nil;
}

-(NSArray*) processSummaryLeftPart:(NSString*)part{
    NSArray* comp = [part componentsSeparatedByString:@" "];
    NSString* text = [[comp subarrayWithRange:NSMakeRange(0, comp.count-1)] componentsJoinedByString:@" "];
    NSString* clockStr = [comp lastObject];
    return @[text, clockStr];
}
-(NSArray*) processSummaryRightPart:(NSString*)part{
    NSArray* comp = [part componentsSeparatedByString:@" "];
    NSString* text = [[comp subarrayWithRange:NSMakeRange(1, comp.count-1)] componentsJoinedByString:@" "];
    NSString* clockStr = [comp firstObject];
    return @[text, clockStr];
}

@end
