//
//  GMTeam.h
//  GridironMoe
//
//  Created by Adya on 8/19/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TSJSONParselable.h"

extern int GM_TEAM_UNKNOWNN;


@interface GMTeam : NSObject <TSJSONParselable>

@property (readonly) NSString* name;
@property (readonly) NSString* teamId;
@property (readonly) int locationId;
@property (readonly) NSString* locationTitle;

-(id) initWithName:(NSString*)name teamId:(NSString*) teamId locationId:(int)locationId andLocationName:(NSString*) location;

+(GMTeam*) teamWithLocationId:(int) locationId;
+(GMTeam*) teamWithTeamId:(NSString*) teamId;

@end
