//
//  GMQuiz.h
//  GridironMoe
//
//  Created by Adya on 8/20/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TSJSONParselable.h"

@interface GMQuizResult : NSObject <TSJSONParselable>

@property int right;
@property int total;
@property int score;
@property NSString* playedDate;
@property NSString* username;
@property NSString* userAvatar;

@end
