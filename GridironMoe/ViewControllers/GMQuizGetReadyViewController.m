//
//  GMQuizGetReadyViewController.m
//  GridironMoe
//
//  Created by Adya on 8/17/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMQuizGetReadyViewController.h"
#import "GMQuizManager.h"
#import "TSNotifier.h"
#import "GMStorage.h"

@interface GMQuizGetReadyViewController () <GMQuizDelegate>

@end

@implementation GMQuizGetReadyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.enableFloatingMoes = NO;
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [GMQuizManager sharedManager].updateDelegate = self;
    [TSNotifier showProgressWithMessage:@"Get ready.." onView:self.view];
    [[GMQuizManager sharedManager] performUpdateQuestions];
    [[GMStorage sharedStorage] putValue:self forKey:STORAGE_REF_QUIZ_READY_VIEW_CONTROLLER];
}

-(NSString*) shareSocialMessageForService:(NSString *)service{
    return NSLocalizedString(@"GM_SHARE_2_MIN_DRILL", nil);
}

-(void) onQuestionsUpdatedResult:(BOOL)success orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    if (success || !error)
        [self performSegueWithIdentifier:@"segStartQuiz" sender:self];
}

@end
