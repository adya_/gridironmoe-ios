//
//  GMStartQuizViewController.m
//  GridironMoe
//
//  Created by Adya on 8/11/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMQuizStartViewController.h"

@interface GMQuizStartViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lDescr;
@property (weak, nonatomic) IBOutlet UITextView *tvDescr;

@end

@implementation GMQuizStartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSTextAttachment *attachment = [NSTextAttachment new];
    attachment.image = [UIImage imageNamed:@"play_button_empty"];
    attachment.bounds = CGRectMake(0, -5, 20, 20);
    
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    
    NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:@"Tap on "];
    [myString appendAttributedString:attachmentString];

    [myString appendAttributedString:[[NSAttributedString alloc] initWithString:@" to start the quiz\n\nTap the answer you think is the right one and tap "]];
    [myString appendAttributedString:attachmentString];
    [myString appendAttributedString:[[NSAttributedString alloc] initWithString:@" to submit your answer.\n\nYou have 2:00 minutes to answer as many SB questions correctly as you can!\n\nYou'll get 10 points for every right answer"]];
    NSRange range = NSMakeRange(0, myString.length);
    [myString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:20] range:range];
    [myString addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:range];
    self.tvDescr.attributedText = myString;
}

-(NSString*) shareSocialMessageForService:(NSString *)service{
    return NSLocalizedString(@"GM_SHARE_2_MIN_DRILL", nil);
}

@end
