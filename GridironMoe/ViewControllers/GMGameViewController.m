//
//  GMGameViewController.m
//  GridironMoe
//
//  Created by Adya on 8/27/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "GMGameViewController.h"
#import "TSUtils.h"
#import "GMGameSchedule.h"
#import "GMTeam.h"
#import "GMGameManager.h"
#import "TSNotifier.h"
#import "GMStorage.h"
#import "GMStrategy.h"
#import "GMGameLive.h"
#import "TSError.h"
#import "GMVoteResult.h"
#import "GMUserManager.h"
#import "GMGameActive.h"
#import "MBProgressHUD.h"
#import "GMMoeGenerator.h"
#import "TSNotificationManager.h"

#define GAME_VIEW_SELECTED_COLOR colorHexString(@"#00BA42")
#define GAME_VIEW_DESELECTED_COLOR [UIColor clearColor]

#define GAME_LABEL_SELECTED_COLOR colorHexString(@"#00BA42")
#define GAME_LABEL_DESELECTED_COLOR [UIColor whiteColor]

#define FIELD_WIDTH 100
#define PICKER_VIEW_ROW_HEIGHT (IS_IPAD ? 44 : 36)
#define PICKER_VIEW_Y_OFFSET (IS_IPAD || IS_IPHONE_6 || IS_IPHONE_6P ? 0 : -10)
#define PICKER_VIEW_X_OFFEST (IS_IPAD || IS_IPHONE_6 || IS_IPHONE_6P ? 0 : -10)

#define TAG_SCHEDULE_TEAM_LABEL 1
#define TAG_SCHEDULE_DATE_LABEL 2
#define TAG_SCHEDULE_TIME_LABEL 3

typedef NS_ENUM(NSInteger, MOE_DIR){
    MOE_LEFT, MOE_RIGHT, MOE_STOP
};



@interface GMGameViewController () <GMGameDelegate> {
    NSArray* games;
    NSArray* offenseStrategies;
    NSArray* defenseStrategies;
    
    GMStrategy* selectedOffense;
    GMStrategy* selectedDefense;
    
    CGFloat pickersScaleFactor;
    
    GMGameLive* displayedGame;
    GMGame* selectedGame;

    BOOL selectGameInBackground;
    
    BOOL isLive;
    BOOL firstUpdateGames;
    BOOL voted;
}

@property (weak, nonatomic) IBOutlet UILabel *lTimer;

@property (weak, nonatomic) IBOutlet UILabel *lDowns;

@property (weak, nonatomic) IBOutlet UILabel *lPosition;
@property (weak, nonatomic) IBOutlet UILabel *lScore;
@property (weak, nonatomic) IBOutlet UIView *vTouchdown;

@property (weak, nonatomic) IBOutlet UIView *vMovingMoe;
@property (weak, nonatomic) IBOutlet UIImageView *ivLeftMoeMove;
@property (weak, nonatomic) IBOutlet UIImageView *ivRightMoeMove;
@property (weak, nonatomic) IBOutlet UIView *vPlayground;
@property (weak, nonatomic) IBOutlet UIImageView *ivMoe;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcMovingMoe;

@property (weak, nonatomic) IBOutlet UICollectionView *cvScheduledGames;
@property (weak, nonatomic) IBOutlet UILabel *lNoSchedule;

// this outlets needed for creating UIPickerViews on them.
@property (weak, nonatomic) IBOutlet UIImageView *ivOffenseBG;
@property (weak, nonatomic) IBOutlet UIImageView *ivDefenseBG;
@property (weak, nonatomic) IBOutlet UILabel *lOffense;
@property (weak, nonatomic) IBOutlet UIView *vOffense;
@property (weak, nonatomic) IBOutlet UILabel *lDefense;
@property (weak, nonatomic) IBOutlet UIView *vDefense;
@property UIPickerView *pvOffense;
@property UIPickerView *pvDefense;

@property (weak, nonatomic) IBOutlet UILabel *lHome;
@property (weak, nonatomic) IBOutlet UILabel *lAway;
@property (weak, nonatomic) IBOutlet UIButton *bCallThePlay;

@property (weak, nonatomic) IBOutlet UIView *vInfoBar;
@property (weak, nonatomic) IBOutlet UIView *vGameBar;
@property (weak, nonatomic) IBOutlet UIView *vVoteBar;
@property (weak, nonatomic) IBOutlet UIView *vMoePlayground;

@end

@interface GMGameViewController (Voting) < UIPickerViewDelegate, UIPickerViewDataSource>

-(void) loadStrategies;
-(void) createPickers;
-(void) fixPickers;
-(void) createPickerView:(NSString*)pickerViewName onBackgroundImageView:(UIImageView*) imageView addToView:(UIView*) parent;
-(void) initPickerView:(UIPickerView*)pickerView onBackgroundImageView:(UIImageView*) imageView addToView:(UIView*) parent;
-(void) clearVote;
- (IBAction)callThePlay:(id)sender;
-(void) onVoteSent:(BOOL)success withResults:(GMVoteResult *)result orError:(TSError *)error;
-(void) blinkButtons;
- (void)blink:(BOOL)enable view:(UIView*) view;
-(void) selectStrategy:(NSArray*) strategies atIndex:(NSInteger)index;

@end

@interface GMGameViewController (Schedule) <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
-(void) reloadSchedule;
-(void) selectGame:(GMGame*)game inBackground:(BOOL)inBackground;
@end

@interface GMGameViewController (LiveGame)

-(void) setGameViewVisible:(BOOL) isVisible;
-(void) moveMoeFor:(int)yards inDirection:(MOE_DIR) dir onSide:(MOE_DIR) side;
-(void) showLiveGame:(GMGameLive*) game;

@end

@implementation GMGameViewController

@synthesize pvDefense = _pvDefense;
@synthesize pvOffense = _pvOffense;

- (void)viewDidLoad {
    [super viewDidLoad];
    firstUpdateGames = YES;
    [self moveMoeFor:FIELD_WIDTH/2 inDirection:MOE_STOP onSide:MOE_STOP];
    //selectedGameIndex = -1;
    selectedGame = nil;
    [self createPickers];
    [self fixPickers];
    [self loadStrategies];
    [self reloadSchedule];
 
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(testGameClick)];
    tap.numberOfTapsRequired = 2;
    [self.vMovingMoe addGestureRecognizer:tap];
    
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [GMGameManager sharedManager].delegate = self;
    [GMGameManager sharedManager].autoUpdateGames = YES;
    if (self.pvDefense && !selectedDefense)
        [self.pvDefense selectRow:0 inComponent:0 animated:NO];
    if (self.pvOffense && !selectedOffense)
        [self.pvOffense selectRow:0 inComponent:0 animated:NO];
    self.ivMoe.image = [GMUserManager sharedManager].moeFaceImage;
    [self setGameViewVisible:[GMGameManager sharedManager].activeGames.count != 0];
    
    GMGame* game = [[GMStorage sharedStorage] popValueForKey:STORAGE_VALUE_VOTE_GAME];
    if (game){
        [[GMGameManager sharedManager] setGame:game live:YES];
        voted = YES;
    }
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    GMGame* voteSelectedGame = [[GMStorage sharedStorage] popValueForKey:STORAGE_VALUE_VOTE_SELECTED_GAME];
    if (voteSelectedGame){
        [self selectGame:voteSelectedGame inBackground:NO];
    }
}

-(void) viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if (voted){
        voted = NO;
        [self showLiveGame:[GMGameManager sharedManager].liveGame];
    }
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[GMGameManager sharedManager] setGame:nil live:NO];
   // [GMGameManager sharedManager].autoUpdateGames = NO;
}

// Override this to generate moes on another view
-(void) setEnableFloatingMoes:(BOOL)enableFloatingMoes{
    [super setEnableFloatingMoes:enableFloatingMoes];
    if (IS_IPAD){
        if (enableFloatingMoes){
            [GMMoeGenerator stopSpawn];
            [GMMoeGenerator setMaxMoesAmount:3];
            [GMMoeGenerator setSpawnIntervalFrom:4 to:4];
            [GMMoeGenerator startSpawnOnView:self.vMoePlayground];
        }
        else
            [GMMoeGenerator stopSpawn];
    }
}

-(void) testGameClick{
    [GMGameManager sharedManager].testing = ![GMGameManager sharedManager].testing;
    NSString* text = ([GMGameManager sharedManager].testing ? @"Emulation ENABLED" : @"Emulation DISABLED");
    [TSNotifier notify:text timeInterval:TS_NOTIFICATION_TIME_SWIFT onView:self.view];
}

-(void) revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position{
    [super revealController:revealController didMoveToPosition:position];
    if (position == FrontViewPositionLeft){
        [self setPickersEnabled:YES];
    }
    else{
        [self setPickersEnabled:NO];
    }
}

-(void) setPickersEnabled:(BOOL)enabled{
    [self.pvDefense setUserInteractionEnabled:enabled];
    [self.pvOffense setUserInteractionEnabled:enabled];
}

@end


@implementation GMGameViewController (Schedule)

-(void) reloadSchedule{
    NSArray* scheduledGames = [GMGameManager sharedManager].scheduledGames;
    NSArray* activeGames = [GMGameManager sharedManager].activeGames;
    if (activeGames.count > 0){
        games = [activeGames arrayByAddingObjectsFromArray:scheduledGames];
    }
    else{
        games = scheduledGames;
    }
    self.lNoSchedule.hidden = (games.count > 0);
    [self.cvScheduledGames reloadData];
}

-(void) onGamesUpdated:(BOOL)success withScheduledGames:(NSArray *)scheduled andActiveGames:(NSArray *)active orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    if (success){
        GMGameManager* manager = [GMGameManager sharedManager];
        [self reloadSchedule];
        if (active.count == 0){
            [self setGameViewVisible:NO];
            [self selectGame:nil inBackground:YES];
            [manager setGame:nil live:NO];
            if (firstUpdateGames){
                [TSNotifier alert:NSLocalizedString(@"GM_ALERT_MSG_NO_LIVE_GAMES", nil)
                    acceptButton:NSLocalizedString(@"GM_ALERT_BUTTON_SURE", nil)
                    acceptBlock:^{
                        [self showViewControllerWithIdentifier:GM_QUIZ_VIEW_CONTROLLER_ID];
                    }
                    cancelButton:NSLocalizedString(@"GM_ALERT_BUTTON_NO_THANKS", nil)
                    cancelBlock:nil];
            }
        }
        else{
            [self setGameViewVisible:YES];
            if (!manager.liveGame || manager.liveGame.status != GM_GAME_LIVE){
                GMGame* game = [[GMStorage sharedStorage] getValueForKey:STORAGE_VALUE_VOTE_GAME];
                if (game)
                    [self selectGame:game inBackground:YES];
                else
                    [self selectGame:[manager.activeGames objectAtIndex:0] inBackground:NO];
            }
            else
                [self selectGame:manager.liveGame inBackground:YES];
        }
        firstUpdateGames = NO;
    }
    else
        [TSNotifier alertError:error acceptButton:NSLocalizedString(@"GM_BUTTON_TITLE_RETRY", nil) acceptBlock:^{
            [GMGameManager sharedManager].autoUpdateGames = YES;
        }];
}


-(CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger index = indexPath.row;
    if (index % 2 == 0){
        return CGSizeMake(self.cvScheduledGames.frame.size.height*1.47f, self.cvScheduledGames.frame.size.height);
    }
    else
        return CGSizeMake(1, self.cvScheduledGames.frame.size.height);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return ((games.count == 0) ? 0 : games.count * 2); // counts also dividerCells
}

-(UICollectionViewCell*) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger index = indexPath.row;
    NSInteger gameIndex = index / 2;
    UICollectionViewCell* cell;
    GMGame* game = [games objectAtIndex:gameIndex];
    if (index % 2 == 0){ // game cell
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"gameCell" forIndexPath:indexPath];
        //if (gameIndex == selectedGameIndex)
        if ([game isEqual:selectedGame])
            cell.backgroundColor = GAME_VIEW_SELECTED_COLOR;
        else
            cell.backgroundColor = GAME_VIEW_DESELECTED_COLOR;
        GMGameSchedule* game = [games objectAtIndex:gameIndex];
        UILabel* lTeams = (UILabel*)[cell viewWithTag:TAG_SCHEDULE_TEAM_LABEL];
        UILabel* lDate = (UILabel*)[cell viewWithTag:TAG_SCHEDULE_DATE_LABEL];
        UILabel* lTime = (UILabel*)[cell viewWithTag:TAG_SCHEDULE_TIME_LABEL];
        lTime.text = game.timeString;
        lDate.text = game.dateString;
        lTeams.text = [NSString stringWithFormat:@"%@ @ %@", game.awayTeam.teamId, game.homeTeam.teamId];
        
    } // divider cell
    else{
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"dividerCell" forIndexPath:indexPath];
    }
    return cell;
}

-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger index = (indexPath.row % 2 == 0 ? indexPath.row : indexPath.row - 1);
    NSInteger gameIndex = index / 2;
    GMGame* game = [games objectAtIndex:gameIndex];
    if ([[GMGameManager sharedManager].activeGames indexOfObject:game] != NSNotFound && game.status == GM_GAME_LIVE){
        if (![[GMGameManager sharedManager].liveGame isEqual:game]){
            UICollectionViewCell* cell;
            //if (selectedGameIndex >= 0){
            if (selectedGame){
                NSInteger index = [games indexOfObject:selectedGame];
                cell = [collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:index*2 inSection:0]];
                cell.backgroundColor = GAME_VIEW_DESELECTED_COLOR;
                displayedGame = nil;
            }
            if (!selectGameInBackground || firstUpdateGames){
                self.enableFloatingMoes = NO; // prevent stuck progress
                [TSNotifier showProgressWithMessage:@"Loading game.." onView:self.view];
                [self clearVote];
            }
            [[GMGameManager sharedManager] setGame:game live:YES];
            
            //selectedGameIndex = gameIndex;
            selectedGame = game;
            cell = [collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
            cell.backgroundColor = (game.status == GM_GAME_LIVE ? GAME_VIEW_SELECTED_COLOR : GAME_VIEW_DESELECTED_COLOR);
        }
    }
    else
        [TSNotifier notify:NSLocalizedString(@"GM_SELECTED_GAME_IS_NOT_LIVE", nil) timeInterval:TS_NOTIFICATION_TIME_SWIFT onView:self.view];
        
}

-(void) collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(void) selectGame:(GMGame*)game inBackground:(BOOL) inBack{
    NSUInteger index = [games indexOfObject:game];
    if (index != NSNotFound)
    {
        selectGameInBackground = inBack;
        [self collectionView:self.cvScheduledGames didSelectItemAtIndexPath:[NSIndexPath indexPathForRow:index*2 inSection:0]];
        selectGameInBackground = NO;
    }
    else{
        NSLog(@"No such game (%@) in schedule", game.gameId);
        for (int i=0; i < games.count*2; i+=2) {
            UICollectionViewCell* cell = [self.cvScheduledGames cellForItemAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0]];
            cell.backgroundColor = GAME_VIEW_DESELECTED_COLOR;
        }
    }
}


@end

@implementation GMGameViewController (LiveGame)

-(void) setGameViewVisible:(BOOL) isVisible{
    isLive = isVisible;
    if (!isLive)
        [self showLiveGame:nil];
    //self.vInfoBar.hidden = !isVisible;
    //self.vGameBar.hidden = !isVisible;
    //self.vVoteBar.hidden = !isVisible;
}

-(void) moveMoeFor:(int)yards inDirection:(MOE_DIR) dir onSide:(MOE_DIR) side{
    int yd = (side == MOE_RIGHT ? FIELD_WIDTH - yards : yards);
    
    switch (dir) {
        case MOE_LEFT:
            self.ivLeftMoeMove.hidden = NO;
            self.ivRightMoeMove.hidden = YES;
            break;
        case MOE_RIGHT:
            self.ivLeftMoeMove.hidden = YES;
            self.ivRightMoeMove.hidden = NO;
            break;
        default:
        case MOE_STOP:
            yd = FIELD_WIDTH / 2;
            self.ivLeftMoeMove.hidden = YES;
            self.ivRightMoeMove.hidden = YES;
            break;
    };
    
    CGRect ground = self.vPlayground.frame;
    NSLog(@"YARDS: %d", yd);
    int zero = -self.vMovingMoe.frame.size.width/2;
    [self.vGameBar layoutIfNeeded];
    [UIView animateWithDuration:0.5 animations:^{
        self.lcMovingMoe.constant = zero + ground.size.width * yd / FIELD_WIDTH;
        [self.vGameBar layoutIfNeeded];
    }];
    

    [self.view layoutIfNeeded];
}

-(void) showLiveGame:(GMGameLive*) game{
    if (isLive && game){
            if (game.status == GM_GAME_LIVE){
                if ([GMGameLive isScoringEvent:game.scoreEvent] && (displayedGame && game.scoreEvent != displayedGame.scoreEvent)){
                    NSString* message;
                    switch (game.scoreEvent) {
                        case GM_GAME_EVENT_TOUCHDOWN: {
                            message = @"Touchdown!";
                            break;
                        }
                        case GM_GAME_EVENT_EXTRA_POINT: {
                            message = @"Extra Point!";
                            break;
                        }
                        case GM_GAME_EVENT_CONVERSION: {
                            message = @"Conversion!";
                            break;
                        }
                        case GM_GAME_EVENT_FIELD_GOAL: {
                            message = @"Field Goal!";
                            break;
                        }
                        case GM_GAME_EVENT_SAFETY: {
                            message = @"Safety!";
                            break;
                        }
                        default: {
                            message = nil;
                            break;
                        }
                    }
                    if (message){ // should never be the case..
                        [TSNotifier notify:message withAppearance:@{kTSNotificationAppearanceTextColor : [UIColor greenColor], kTSNotificationAppearancePositionYOffset : @(5)} onView:self.vTouchdown];
                    }
                }
                if ([GMGameLive isTimeoutEvent:game.event] && (game.event != displayedGame.event)){
                    [TSNotifier notify:game.summary withAppearance:@{kTSNotificationAppearanceTextColor : [UIColor redColor], kTSNotificationAppearanceTextSize : @(20.0f)} onView:self.view];
                }
                
                self.lTimer.text = ([GMGameLive isTimeoutEvent:game.event] ? game.summary : game.clock);
                self.lTimer.textColor = ([GMGameLive isTimeoutEvent:game.event] ? [UIColor redColor] : [UIColor whiteColor]);
                self.lPosition.hidden = YES;//(game.summary != nil);
               // self.lPosition.text = [NSString stringWithFormat:@"%@ ON %@ %d YD", game.offenseTeam.teamId, ((game.offenseTeam == game.ballSideTeam) ? @"OWN" : game.ballSideTeam.teamId), game.ballPosition];
                
                self.lDowns.text = ((game.down == 0) ? @"" : [NSString stringWithFormat:@"%@ & %d", [TSUtils ordinalNumber:game.down withFormat:nil], game.yards]);
                
                if (displayedGame && game.down != 0 && (game.down != displayedGame.down || game.yards != displayedGame.yards)){
                   // UIFont* tmpFont = self.lDowns.font;
                   // self.lDowns.font = [tmpFont fontWithSize:36];
                  //  [self.lDowns sizeToFit];
                    self.lDowns.transform = CGAffineTransformScale(self.lDowns.transform, 1.0, 1.0);
                    [UIView animateWithDuration:0.3  delay:0 options:UIViewAnimationOptionAutoreverse animations:^{
                        self.lDowns.transform = CGAffineTransformScale(self.lDowns.transform, 1.25, 1.25);
                    } completion:^(BOOL finished) {
                     //   self.lDowns.font = tmpFont;
                     //   [self.lDowns sizeToFit];
                        self.lDowns.transform = CGAffineTransformScale(self.lDowns.transform, 0.8, 0.8);
                    }];
                }
                
                if (game.down != 0){
                    if (game.event == GM_GAME_EVENT_QUARTER_END || game.event == GM_GAME_EVENT_GAME_OVER){
                        [self moveMoeFor:FIELD_WIDTH/2 inDirection:MOE_STOP onSide:MOE_STOP];
                    }
                    else{
                        MOE_DIR side = (game.ballSideTeam == game.homeTeam ? MOE_LEFT : MOE_RIGHT);
                        MOE_DIR dir = (game.offenseTeam == game.homeTeam ? MOE_RIGHT : MOE_LEFT);
                        [self moveMoeFor:game.ballPosition inDirection:dir onSide:side];
                    }
                }
                
            }
            else {
                self.lTimer.text = @"OVER";
                self.lTimer.textColor = [UIColor whiteColor];
                [self moveMoeFor:FIELD_WIDTH/2 inDirection:MOE_STOP onSide:MOE_STOP];
              //  self.lPosition.text = @"";
                self.lDowns.text = @"";
            }
            
            self.lScore.text = [NSString stringWithFormat:@"Q%d %d-%d", game.quarter, game.awayTeamPoints, game.homeTeamPoints];
            self.lHome.text = game.homeTeam.teamId;
            self.lAway.text = game.awayTeam.teamId;
            displayedGame = game;
    }
    else{
        self.lTimer.text = @"00:00";
        self.lTimer.textColor = [UIColor whiteColor];
      //  self.lPosition.text = @"Quarter";
        self.lHome.text = @"HOME";
        self.lAway.text = @"AWAY";
        self.lDowns.text = @"Down";
        self.lDowns.hidden = NO;
        self.lScore.text = @"Score";
        [self moveMoeFor:FIELD_WIDTH/2 inDirection:MOE_STOP onSide:MOE_STOP];
    }
}

-(void) onLiveFeedUpdated:(BOOL)success withLiveGame:(GMGameLive *)game orError:(TSError *)error{
        if (!success){
        [[GMGameManager sharedManager] setGame:nil live:NO];
    }
    else
        [self showLiveGame:game];
    [TSNotifier hideProgressOnView:self.view];
    self.enableFloatingMoes = YES; // prevent stuck progress
//    [self performSelector:@selector(hideProgress) withObject:nil afterDelay:1];

}

-(void) hideProgress{
    [TSNotifier hideProgressOnView:self.view];
}

@end

@implementation GMGameViewController (Voting)

-(void) loadStrategies{
    offenseStrategies = [[GMStorage sharedStorage] getValueForKey:STORAGE_ARRAY_OFFENSE_STRATEGIES];
    defenseStrategies = [[GMStorage sharedStorage] getValueForKey:STORAGE_ARRAY_DEFENSE_STRATEGIES];
}

-(void) createPickers{
    [self createPickerView:@"pvOffense" onBackgroundImageView:self.ivOffenseBG addToView:self.vOffense];
    [self createPickerView:@"pvDefense" onBackgroundImageView:self.ivDefenseBG addToView:self.vDefense];
}

-(void) fixPickers{
    //NSLog(@"Offense:\n\n");
    [self initPickerView:self.pvOffense onBackgroundImageView:self.ivOffenseBG addToView:self.vOffense];
    //NSLog(@"Defense:\n\n");
    [self initPickerView:self.pvDefense onBackgroundImageView:self.ivDefenseBG addToView:self.vDefense];
}

-(void) createPickerView:(NSString*)pickerViewName onBackgroundImageView:(UIImageView*) imageView addToView:(UIView*) parent{
    UIPickerView* tmpPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, imageView.frame.size.width, imageView.frame.size.height)];
    [parent addSubview:tmpPickerView];
    tmpPickerView.delegate = self;
    tmpPickerView.dataSource = self;
    tmpPickerView.showsSelectionIndicator = NO;
    [self setValue:tmpPickerView forKey:pickerViewName];
}

-(void) initPickerView:(UIPickerView*)pickerView onBackgroundImageView:(UIImageView*) imageView addToView:(UIView*) parent{
    //NSLog(@"View Frame: %@\n", NSStringFromCGRect(parent.frame));
    //NSLog(@"Image Frame: %@\n", NSStringFromCGRect(imageView.frame));
    //NSLog(@"Original Frame: %@\n", NSStringFromCGRect(pickerView.frame));
    
    // scale
    pickersScaleFactor = (imageView.frame.size.height)/pickerView.frame.size.height;
    //NSLog(@"Factor: %.2f", pickersScaleFactor);
    CGAffineTransform scale = CGAffineTransformMakeScale(pickersScaleFactor, pickersScaleFactor);
    pickerView.frame = CGRectMake(pickerView.frame.origin.x, pickerView.frame.origin.y, imageView.frame.size.width / pickersScaleFactor, pickerView.frame.size.height);
    [pickerView setTransform:scale]; // apply immediately to know scaled frame
    
    //NSLog(@"Scaled Frame: %@\n", NSStringFromCGRect(pickerView.frame));
    
    // position
    CGFloat zeroX = -pickerView.frame.origin.x;
    CGFloat zeroY = -pickerView.frame.origin.y;
    CGFloat centeredX = imageView.frame.origin.x + PICKER_VIEW_X_OFFEST;
    CGFloat centeredY = imageView.frame.origin.y + PICKER_VIEW_Y_OFFSET;
    CGAffineTransform translate = CGAffineTransformMakeTranslation(zeroX + centeredX, zeroY + centeredY);
    //NSLog(@"Zero: (%.2f; %.2f)", zeroX, zeroY);
    //NSLog(@"Center: (%.2f; %.2f)", centeredX, centeredY);
    // applying transformation
    [pickerView setTransform:CGAffineTransformConcat(scale, translate)];
    //NSLog(@"New Frame: %@\n", NSStringFromCGRect(pickerView.frame));
   // pickerView.backgroundColor = colorARGB(40, 255, 0, 0);
}

-(void) clearVote{
    [self selectStrategy:offenseStrategies atIndex:-1];
    [self selectStrategy:defenseStrategies atIndex:-1];
    [self blinkButtons];
}

- (IBAction)callThePlay:(id)sender {
        if (!selectedOffense && !selectedDefense){
            [TSNotifier notify:@"Select offense and/or defense" onView:self.view];
            return;
        }
    if (isLive){
        GMGameEvent event = [GMGameManager sharedManager].liveGame.event;
        switch (event) { // check ability to vote
            case GM_GAME_EVENT_TIMEOUT:
            case GM_GAME_EVENT_QUARTER_END: {
                [TSNotifier notify:@"Sorry, you can't vote during timeout" onView:self.view];
                [self clearVote];
                return;
            }
            case GM_GAME_EVENT_GAME_OVER: {
                [TSNotifier notify:@"Sorry, this game is over" onView:self.view];
                [self clearVote];
                return;
            }
            default: break;
        }
        self.enableFloatingMoes = NO; // prevent stuck progress
        [TSNotifier showProgressWithMessage:@"Saving vote.." onView:self.view];
        [[GMGameManager sharedManager] performSendVoteWithOffence:selectedOffense andDefense:selectedDefense forTeam:nil];
    }
    else{
        GMVoteResult* res = [GMVoteResult new];
        res.offensePoints = 0;
        res.defensePoints = 0;
        res.totalPoints = 0;
        res.offenseAgreed = 0;
        res.defenseAgreed = 0;
        
        if (selectedOffense){
            res.offenseVote = selectedOffense;
            res.offenseTopVote = selectedOffense;
            res.offenseTopVoteRate = 100;
        }
        if (selectedDefense){
            res.defenseVote = selectedDefense;
            res.defenseTopVote = selectedDefense;
            res.defenseTopVoteRate = 100;
        }
        [[GMStorage sharedStorage] putValue:res forKey:STORAGE_VALUE_VOTE_RESULT];
        [self clearVote];
        [self performSegueWithIdentifier:@"segVote" sender:self];
    }
}

-(void) onVoteSent:(BOOL)success withResults:(GMVoteResult *)result orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    self.enableFloatingMoes = YES; // prevent stuck progress
    if (success){
        result.offenseVote = selectedOffense;
        result.defenseVote = selectedDefense;
        [[GMStorage sharedStorage] putValue:result forKey:STORAGE_VALUE_VOTE_RESULT];
        [[GMStorage sharedStorage] putValue:[GMGameManager sharedManager].liveGame forKey:STORAGE_VALUE_VOTE_GAME];
        [self clearVote];
        [self performSegueWithIdentifier:@"segVote" sender:self];
    }
    else
        [TSNotifier notifyError:error onView:self.view];
    
}

-(void) blinkButtons{
    BOOL readyToVote = (selectedDefense || selectedOffense);
    [self blink:readyToVote view:self.bCallThePlay];
}

- (void)blink:(BOOL)enable view:(UIView*) view {
    
    if (enable){
        CGFloat oldAlpha = view.alpha;
        CGFloat minAlpha = 0.55;
        enum UIViewAnimationOptions options = UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionAutoreverse | UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionRepeat;
        [UIView animateWithDuration:.5 delay:0.3 options:options animations:^{
            view.alpha = minAlpha;
        }
        completion:^(BOOL finished) {
            view.alpha = oldAlpha;
        }];
    }
    else{
        [view.layer removeAllAnimations];
        view.alpha = 1;
    }
}

-(void) selectStrategy:(NSArray*) strategies atIndex:(NSInteger)index{
    if (strategies == offenseStrategies){
        if (index < 0 || index >= strategies.count){
            selectedOffense = nil;
            self.lOffense.textColor = [UIColor whiteColor];
            [self.pvOffense selectRow:0 inComponent:0 animated:NO];
        }
        else{
            selectedOffense = [strategies objectAtIndex:index];
            self.lOffense.textColor = colorHexString(@"#00BA42");
        }
    }
    else{
        if (index < 0 || index >= strategies.count){
            selectedDefense = nil;
            self.lDefense.textColor = [UIColor whiteColor];
            [self.pvDefense selectRow:0 inComponent:0 animated:NO];
        }
        else{
            selectedDefense = [strategies objectAtIndex:index];
            self.lDefense.textColor = colorHexString(@"#00BA42");
        }
    }
}

-(CGFloat) pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return PICKER_VIEW_ROW_HEIGHT * pickersScaleFactor;
}

-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView == self.pvOffense)
        return offenseStrategies.count;
    else
        return defenseStrategies.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (pickerView == self.pvOffense){
        return [(GMStrategy*)[offenseStrategies objectAtIndex:row] name];
    }
    else{
        return [(GMStrategy*)[defenseStrategies objectAtIndex:row] name];
    }
    
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row
          forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label = (UILabel *)view;
    if (!label) {
        label = [UILabel new];
        label.textColor = [UIColor whiteColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.minimumScaleFactor = 0.8;
        label.lineBreakMode = NSLineBreakByClipping;
        label.font = [UIFont systemFontOfSize:12/pickersScaleFactor];
    }
    
    label.text = [self pickerView:pickerView titleForRow:row forComponent:component];
    
    return label;
}

-(void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (pickerView == self.pvOffense){
        [self selectStrategy:offenseStrategies atIndex:row];
    }
    else{
        [self selectStrategy:defenseStrategies atIndex:row];
    }
    if (isLive)
        [self blinkButtons];
}


@end


