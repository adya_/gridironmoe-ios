//
//  GMProfileViewController.m
//  GridironMoe
//
//  Created by Adya on 8/20/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMProfileViewController.h"
#import "GMScoresManager.h"
#import "GMUser.h"
#import "TSNotifier.h"
#import "GMScore.h"
#import "TSUtils.h"
#import "GMTeam.h"
#import "GMEditUsernameView.h"
#import "KLCPopup.h"
#import "GMUserManager.h"
#import "GMMoeGenerator.h"
#import "GMMoeFace.h"

@interface GMProfileViewController () <GMScoresDelegate, GMUserProfileDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *ivAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lNickname;
@property (weak, nonatomic) IBOutlet UILabel *lLocation;

@property (weak, nonatomic) IBOutlet UIView *vBody;
@property (weak, nonatomic) IBOutlet UIView *vEdit;

@property (weak, nonatomic) IBOutlet UIImageView *ivBg;

@property (weak, nonatomic) IBOutlet UILabel *lRank;
@property (weak, nonatomic) IBOutlet UILabel *lTeam;

@property (weak, nonatomic) IBOutlet UILabel *lDefensePoints;
@property (weak, nonatomic) IBOutlet UILabel *lOffensePoints;
@end

@implementation GMProfileViewController{
    BOOL isEditing;
    KLCPopup* popup;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.ivBg.clipsToBounds = YES;
    self.enableFloatingMoes = NO;
    
    [GMScoresManager sharedManager].delegate = self;
    [[GMScoresManager sharedManager] performUpdateUserScores];
        [self initPopup];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [GMUserManager sharedManager].profileDelegate = self;
    [self updateUserInfo];
}

-(void) revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position{
    [super revealController:revealController willMoveToPosition:position];
    if (position == FrontViewPositionLeft)
        [self updateUserInfo];
}

-(void) initPopup{
    GMEditUsernameView* editView = [[[NSBundle mainBundle] loadNibNamed:@"GMEditUsernameView" owner:self options:nil] lastObject];
    popup = [KLCPopup popupWithContentView:editView showType:KLCPopupShowTypeGrowIn dismissType:KLCPopupDismissTypeGrowOut  maskType:KLCPopupMaskTypeDimmed dismissOnBackgroundTouch:YES dismissOnContentTouch:NO];
    __weak GMEditUsernameView* weakEditView = editView;
    __weak GMProfileViewController* weakSelf = self;
    __weak KLCPopup* weakPopup = popup;
    [editView setUsername:[GMUserManager sharedManager].user.username];
    [editView setEditedCallback:^(NSString *username) {
        [weakSelf dismissKeyboard];
        if (username && ![username isEqualToString:[GMUserManager sharedManager].user.username]){
            [TSNotifier showProgressWithMessage:@"Saving.." onView:weakEditView];
            [[GMUserManager sharedManager] performChangeUsername:trim(username)];
        }
        else
            [weakPopup dismiss:YES];
    }];
}

-(void) onUserScoreUpdated:(BOOL)success withUser:(GMUser *)user orError:(TSError *)error{
    if (success)
        [self updateUserInfo];
}

-(void) onUserProfileUpdated:(BOOL)success withUser:(GMUser *)user orError:(TSError *)error{
    [TSNotifier hideProgressOnView:popup.contentView];
    if (success){
        [popup dismiss:YES];
        [self updateUserInfo];
    }
    else
        [TSNotifier alertError:error];
}

-(void) updateUserInfo{
    GMUser* user = [GMScoresManager sharedManager].user;
    self.lNickname.text = user.username;
    self.lRank.text = user.rankSeasonTitle;
    self.lOffensePoints.text = [TSUtils prettyNumber:@(user.offence.score)];
    self.lDefensePoints.text = [TSUtils prettyNumber:@(user.defence.score)];
    self.lTeam.text = (user.team ? user.team.name : @"NONE");
    self.lLocation.text = user.team.locationTitle;
    self.ivAvatar.image = [UIImage imageNamed:[GMUserManager sharedManager].moeFace.avatar];
}

- (IBAction)changeAvatar:(id)sender {
    [TSNotifier notify:@"This feature is not supported yet" onView:self.view];
    [self hideEditMenu:YES];
}

- (IBAction)changeNameClick:(id)sender {
    [popup show];
    [self hideEditMenu:YES];
}



- (IBAction)toggleEdit:(id)sender {
    if (isEditing) [self hideEditMenu:YES];
    else [self showEditMenu:YES];
}

-(void) viewDidLayoutSubviews{
    [self hideEditMenu:NO];
}

-(void) showEditMenu:(BOOL) animated{
    if (animated)
        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            [self showEditMenu:NO];
        } completion:nil];
    else{
        self.vEdit.frame  = CGRectMake(0, 0, self.vBody.frame.size.width, self.vBody.frame.size.height);
        isEditing = YES;
    }
}

-(void) hideEditMenu:(BOOL) animated{
    if (animated){
        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            [self hideEditMenu:NO];
        } completion:nil];
    }
    else{
        self.vEdit.frame  = CGRectMake(0, self.vBody.frame.size.height, self.vBody.frame.size.width, self.vBody.frame.size.height);
        isEditing = NO;
    }
}

@end
