//
//  GMMapViewController.m
//  GridironMoe
//
//  Created by Adya on 8/17/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMMapViewController.h"
#import "GMUserManager.h"
#import "GMMapTeamFansCell.h"
#import "TSNotifier.h"
#import "GMUser.h"
#import "GMTeam.h"
#import "GMStorage.h"


@interface GMMapViewController () <UITableViewDelegate, UITableViewDataSource, GMUserProfileDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableViewLeft;
@property (weak, nonatomic) IBOutlet UITableView *tableViewRight;

@property (weak, nonatomic) IBOutlet UIView *vMap;
@property (weak, nonatomic) IBOutlet UILabel *lCounter;

@end

@implementation GMMapViewController{
    UIButton* selectedStadium;
    NSDictionary* fans;
    NSArray* sortedFansKeys;
    NSInteger countInTable;
    GMTeam* selectedTeam;
    BOOL savingTeam;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self updateCounter];
    for (id btn in self.vMap.subviews) {
        if ([btn isKindOfClass:[UIButton class]])
            [btn addTarget:self action:@selector(stadiumClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    GMUserManager* manager = [GMUserManager sharedManager];
    manager.profileDelegate = self;
    if (!manager.fansInStadiums || manager.fansInStadiums.count == 0){
        [TSNotifier showProgressWithMessage:@"Hold on.." onView:self.view];
        [[GMUserManager sharedManager] performUpdateFansInStadiums];
    }
    else{
        [self loadFansTables];
    }
    
    
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    GMUserManager* manager = [GMUserManager sharedManager];
    if (manager.user.team)
        [self selectTeamWithId:manager.user.team.teamId];
}

-(void) loadFansTables{
    NSDictionary* nonEmptyFans = [GMUserManager sharedManager].fansInStadiums;
    
    NSMutableDictionary* allFans = [NSMutableDictionary new];
    NSDictionary* teams = [[GMStorage sharedStorage] getValueForKey:STORAGE_DICTIONARY_TEAMS];
    for (NSString* teamId in [teams allKeys]) {
        NSNumber* numberOfFans = [nonEmptyFans objectForKey:teamId];
        if (numberOfFans)
            [allFans setObject:numberOfFans forKey:teamId];
        else
            [allFans setObject:@(0) forKey:teamId];
    }
    fans = [NSDictionary dictionaryWithDictionary:allFans];
    sortedFansKeys = [teams keysSortedByValueUsingComparator:^NSComparisonResult(GMTeam* obj1, GMTeam* obj2) {
        return [obj1.name compare:obj2.name options:NSLiteralSearch];
    }];
    countInTable = fans.count / 2;
}

-(void) selectTeamWithId:(NSString*) teamId{
    GMTeam* team = [GMTeam teamWithTeamId:teamId];
    if (!team){
        NSLog(@"WRONG TEAM ID: %@", teamId);
        return;
    }
    if ([selectedTeam isEqual:team])
        return;
    
    selectedTeam = team;
    
    UIButton* std = (UIButton*)[self.vMap viewWithTag:team.locationId];
    if (selectedStadium)
        [self setStadium:selectedStadium selected:NO];
    
    selectedStadium = std;
    [self setStadium:selectedStadium selected:YES];
    UITableView* tableView;
    NSInteger index = [sortedFansKeys indexOfObject:teamId];
    if (index != NSNotFound){
        if (index < countInTable){
            [self.tableViewRight reloadData];
            tableView = self.tableViewLeft;
        }
        else{
            index -= countInTable;
            [self.tableViewLeft reloadData];
            tableView = self.tableViewRight;
        }
        
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    }
}


-(void) setStadium:(UIButton*)button selected:(BOOL)isSelected{
    NSString* img = (isSelected? @"map_stadium_selected" : @"map_stadium_dark");
    [button setImage:[UIImage imageNamed:img] forState:UIControlStateNormal];
}

- (IBAction)stadiumClicked:(id)sender {
    int tag = (int)[(UIView*)sender tag];
    GMTeam* team = [GMTeam teamWithLocationId:tag];
    [self selectTeamWithId:team.teamId];
}

-(void) updateCounter{
    __block int total = 0;
    NSDictionary* nonEmptyFans = [GMUserManager sharedManager].fansInStadiums;
    [[nonEmptyFans allValues] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        total += [obj intValue];
    }];
    self.lCounter.text = [NSString stringWithFormat:@"%d", total];//[GMUserManager sharedManager].loggedInUsers];
}

-(void) refresh{
    [self.tableViewLeft reloadData];
    [self.tableViewRight reloadData];
}

-(void) onFansInStadiumsUpdated:(BOOL)success withFans:(NSDictionary *)_fans orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    if (success){
        [self updateCounter];
        if (savingTeam){
            savingTeam = NO;
            [TSNotifier notify:@"Home team was successfully saved" timeInterval:TS_NOTIFICATION_TIME_SHORT onView:self.view];
        }
        [self loadFansTables];
        [self refresh];
    }
}

-(void) onHomeTeamUpdated:(BOOL)success orError:(TSError *)error{
    if (success)
        [[GMUserManager sharedManager] performUpdateFansInStadiums];
    else{
        [TSNotifier hideProgressOnView:self.view];
        [TSNotifier notifyError:error onView:self.view];
    }
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.tableViewLeft)
        return countInTable;
    else
        return fans.count - countInTable; // in case if count was odd.
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [GMMapTeamFansCell height];
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"GMMapTeamFansCell";
    
    GMMapTeamFansCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = (GMMapTeamFansCell*)[[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil].lastObject;
    }
    
    NSInteger index = indexPath.row;
    if (tableView == self.tableViewRight){
        index += countInTable;
    }
    NSString* teamId = [sortedFansKeys objectAtIndex:index];
    NSInteger fansCount = [[fans objectForKey:teamId] integerValue];
    [cell setFansCount:(NSInteger)fansCount forTeam:teamId andSaveBlock:^{
        savingTeam = YES;
        [TSNotifier showProgressWithMessage:@"Hold on.." onView:self.view];
        [[GMUserManager sharedManager] performUpdateHomeTeam:selectedTeam];
    }];
    GMTeam* userTeam = [GMUserManager sharedManager].user.team;
    cell.savedTeam = (userTeam && [teamId isEqualToString:userTeam.teamId]);
    cell.canSave = !cell.savedTeam;

    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger index = indexPath.row;
    if (tableView == self.tableViewRight){
        index += countInTable;
    }
    NSString* teamId = [sortedFansKeys objectAtIndex:index];
    
    if (selectedTeam && [teamId isEqualToString:selectedTeam.teamId])
        return;
    [self selectTeamWithId:teamId];
    
}


@end

