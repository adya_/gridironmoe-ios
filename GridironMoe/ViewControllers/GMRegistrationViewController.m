//
//  GMRegistrationViewController.m
//  GridironMoe
//
//  Created by Adya on 8/10/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMRegistrationViewController.h"
#import "GMUserManager.h"
#import "GMUser.h"
#import "TSNotifier.h"
#import "TSUtils.h"

@interface GMRegistrationViewController () <GMLoginDelegate>
@property (weak, nonatomic) IBOutlet UITextField *tfUsername;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword;

@end

@implementation GMRegistrationViewController{
    GMUserManager* manager;
}

-(void) viewDidLoad{
    [super viewDidLoad];
    manager = [GMUserManager sharedManager];
    manager.loginDelegate = self;
    self.enableFloatingMoes = NO;
}

- (IBAction) backClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction) registerClick:(id)sender {
    NSString* username = trim(self.tfUsername.text);
    NSString* password =  trim(self.tfPassword.text);
    NSString* email = trim(self.tfEmail.text);
    
    if (![manager isValidUsername:username]){
        [TSNotifier notify:NSLocalizedString(@"GM_NOTIF_INVALID_USERNAME", nil) onView:self.view];
    }
    else if (![manager isValidPassword:password]){
        [TSNotifier notify:NSLocalizedString(@"GM_NOTIF_INVALID_PASSWORD", nil) onView:self.view];
    }
    else if (![TSUtils isValidEmail:email]){
        [TSNotifier notify:NSLocalizedString(@"GM_NOTIF_INVALID_EMAIL", nil) onView:self.view];
    }
    else{
        [self dismissKeyboard];
        [TSNotifier showProgressWithMessage:NSLocalizedString(@"GM_PROGRESS_REGISTRATION", nil) onView:self.view];
        [manager performRegistrationWithUsername:username password:password andEmail:email shouldLoginOnSuccess:YES];
    }
}

- (IBAction)loginClick:(id)sender {
    [self dismissKeyboard];
    [TSNotifier showProgressWithMessage:NSLocalizedString(@"GM_PROGRESS_LOGIN", nil) onView:self.view];
    [[GMUserManager sharedManager] performLoginWithUsername:@"adyatest1" andPassword:@"test"];
}

-(void) onRegistrationResult:(BOOL)success orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    if (success)
        [TSNotifier showProgressWithMessage:NSLocalizedString(@"GM_PROGRESS_LOGIN", nil) onView:self.view];
    else
        [TSNotifier alertError:error];
}

-(void) onLoginResult:(BOOL)success withUser:(GMUser *)user orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    if (success){
        [self performSegueWithIdentifier:@"segRegMain" sender:self];
    }
    else
        [TSNotifier alertError:error];
        
}

@end
