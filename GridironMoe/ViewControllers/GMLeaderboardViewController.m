//
//  GMLeaderboardViewController.m
//  GridironMoe
//
//  Created by Adya on 8/20/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMLeaderboardViewController.h"
#import "HMSegmentedControl.h"
#import "TSUtils.h"
#import "GMScoresManager.h"
#import "TSNotifier.h"
#import "GMUser.h"
#import "GMScore.h"
#import "GMScoreboardCell.h"
#import "UIImageView+AFNetworking+Animated.h"
#import "GMMoeFace.h"

#define TOP_LEADERS_NUMBER 3

@interface GMLeaderboardViewController ()<GMScoresDelegate, UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *vTabs;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIImageView *ivDrillAvatar1;
@property (weak, nonatomic) IBOutlet UIImageView *ivDrillAvatar2;
@property (weak, nonatomic) IBOutlet UIImageView *ivDrillAvatar3;

@property (weak, nonatomic) IBOutlet UILabel *lDrillName1;
@property (weak, nonatomic) IBOutlet UILabel *lDrillName2;
@property (weak, nonatomic) IBOutlet UILabel *lDrillName3;

@property (weak, nonatomic) IBOutlet UILabel *lDrillPoints1;
@property (weak, nonatomic) IBOutlet UILabel *lDrillPoints2;
@property (weak, nonatomic) IBOutlet UILabel *lDrillPoints3;

@property (weak, nonatomic) HMSegmentedControl* tabs;


@end

@implementation GMLeaderboardViewController{
    int pendingRequests;
    int attempts;
    BOOL errorOcurred;
    NSArray* leaderboard;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self styleAvatarView:self.ivDrillAvatar1];
    [self styleAvatarView:self.ivDrillAvatar2];
    [self styleAvatarView:self.ivDrillAvatar3];
    attempts = 5;
    [GMScoresManager sharedManager].delegate = self;
    [self attemptLoadLeaderboard];
}


-(void) revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position{
    [super revealController:revealController willMoveToPosition:position];
    if (position == FrontViewPositionLeft)
        [self showLeaderboard];
}

-(void) styleAvatarView:(UIImageView*) view{
    view.layer.cornerRadius = view.frame.size.width/2;
    view.layer.masksToBounds = YES;
    view.contentMode = UIViewContentModeScaleAspectFit;
}

-(void) attemptLoadLeaderboard{
    pendingRequests = 1;
    [TSNotifier showProgressWithMessage:@"Loading scores.." onView:self.view];
    [[GMScoresManager sharedManager] performUpdateLeaderboardScores];
}

-(void) viewDidLayoutSubviews{
    if (!self.tabs)
        self.tabs = [self createTabs];
}

-(HMSegmentedControl*) createTabs{
    HMSegmentedControl *segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"Current Season"]];//@[@"INDIVIDUAL\nRANKING", @"TEAMS\nRANKING"]];
    
    segmentedControl.shouldAnimateUserSelection = YES;
    
    segmentedControl.backgroundColor = colorHexString(@"#1B1C1F");
    segmentedControl.titleTextAttributes = @{NSForegroundColorAttributeName : colorHexString(@"#FFFFFF"),
        NSFontAttributeName:[UIFont systemFontOfSize:20]};
    segmentedControl.selectionIndicatorColor = colorHexString(@"#00BA42");
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.selectionIndicatorHeight = 4;
    segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    
    segmentedControl.verticalDividerEnabled = YES;
    segmentedControl.verticalDividerColor = colorHexString(@"#26282D");
    segmentedControl.verticalDividerWidth = 1;
    
    [segmentedControl setIndexChangeBlock:^(NSInteger index) {
        switch (index) {
            default:
            case 0:
                leaderboard = [GMScoresManager sharedManager].leaderboard;
                [self showLeaderboard];
                break;
            case 1:
                leaderboard = [GMScoresManager sharedManager].leaderboard;
                [self showLeaderboard];
                break;
        }
    }];
    segmentedControl.frame = CGRectMake(0, 0, self.vTabs.frame.size.width, self.vTabs.frame.size.height);
    NSLog(@"Leaderboard segmented control: %@",NSStringFromCGRect(segmentedControl.frame));
    [self.vTabs addSubview:segmentedControl];
    return segmentedControl;
}

-(void) showLeaderboard{
    for (NSInteger i = 0; i < TOP_LEADERS_NUMBER; ++i) {
        GMUser* user = (i < leaderboard.count ? [leaderboard objectAtIndex:i] : [[GMUser alloc] initWithUsername:@"Empty" andPassword:@""]);
        [self setTopScore:user atIndex:i];
    }
    
    if (leaderboard.count > TOP_LEADERS_NUMBER){
        self.tableView.hidden = NO;
        [self.tableView reloadData];
    }
    else self.tableView.hidden = YES;
}

-(void) setTopScore:(GMUser*)user atIndex:(NSInteger) index{
    switch (index) {
        case 0:
            if (user.avatarURL)
                [self.ivDrillAvatar1 setImageWithURL:[NSURL URLWithString:user.avatarURL]];
            else
                self.ivDrillAvatar1.image = [UIImage imageNamed:[[GMScoresManager sharedManager] avatarForUser:user]];
            self.lDrillName1.text = user.username;
            self.lDrillPoints1.text = [NSString stringWithFormat:@"%d", user.total.score];
            break;
        case 1:
            if (user.avatarURL)
                [self.ivDrillAvatar2 setImageWithURL:[NSURL URLWithString:user.avatarURL]];
            else
                self.ivDrillAvatar2.image = [UIImage imageNamed:[[GMScoresManager sharedManager] avatarForUser:user]];
            self.lDrillName2.text = user.username;
            self.lDrillPoints2.text = [NSString stringWithFormat:@"%d", user.total.score];
            break;
        case 2:
            if (user.avatarURL)
                [self.ivDrillAvatar3 setImageWithURL:[NSURL URLWithString:user.avatarURL]];
            else
                self.ivDrillAvatar3.image = [UIImage imageNamed:[[GMScoresManager sharedManager] avatarForUser:user]];
            self.lDrillName3.text = user.username;
            self.lDrillPoints3.text = [NSString stringWithFormat:@"%d", user.total.score];
            break;
        default:
            break;
            
    }
}

-(void) onLeaderboardScoresUpdated:(BOOL)success withLeaderboard:(NSArray *)_leaderboard orError:(TSError *)error{
    --pendingRequests;
    errorOcurred = errorOcurred || !success;
    if (pendingRequests == 0)
        [TSNotifier hideProgressOnView:self.view];
    
    if (errorOcurred){
        if (attempts > 0){
            --attempts;
            [self attemptLoadLeaderboard];
        }
        else{
            [TSNotifier notify:@"Unable to load leaderboard. Please, try again later" onView:self.view];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else{
        leaderboard = _leaderboard;
        [self showLeaderboard];
    }
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return (leaderboard.count <=  TOP_LEADERS_NUMBER ? 0 : leaderboard.count - TOP_LEADERS_NUMBER);
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [GMScoreboardCell height];
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"GMScoreboardCell";
    
    GMScoreboardCell* cell = (GMScoreboardCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    GMUser* user = [leaderboard objectAtIndex:indexPath.row + 3];
    [cell setUser:user.username withAvatar:[[GMScoresManager sharedManager] avatarForUser:user] andPoints:user.total.score toPosition:(indexPath.row + TOP_LEADERS_NUMBER + 1)];
    //[cell setUser:user.username withAvatarURL:user.avatarURL andPoints:user.total.score toPosition:(indexPath.row + TOP_LEADERS_NUMBER + 1)];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


@end
