//
//  GMQuizScoreboardViewController.m
//  GridironMoe
//
//  Created by Adya on 8/17/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMQuizScoreboardViewController.h"
#import "HMSegmentedControl.h"
#import "TSUtils.h"
#import "GMQuizManager.h"
#import "GMQuizResult.h"
#import "TSNotifier.h"
#import "GMScoreboardCell.h"
#import "GMMoeFace.h"

#define TOP_LEADERS_NUMBER 3

#define TEMP_AVATAR_PLACEHOLDER [UIImage imageNamed:[GMMoeFace moeFaceAtIndex:4].avatar]

@interface GMQuizScoreboardViewController () <GMQuizDelegate, UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *vTabs;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIImageView *ivDrillAvatar1;
@property (weak, nonatomic) IBOutlet UIImageView *ivDrillAvatar2;
@property (weak, nonatomic) IBOutlet UIImageView *ivDrillAvatar3;

@property (weak, nonatomic) IBOutlet UILabel *lDrillName1;
@property (weak, nonatomic) IBOutlet UILabel *lDrillName2;
@property (weak, nonatomic) IBOutlet UILabel *lDrillName3;

@property (weak, nonatomic) IBOutlet UILabel *lDrillPoints1;
@property (weak, nonatomic) IBOutlet UILabel *lDrillPoints2;
@property (weak, nonatomic) IBOutlet UILabel *lDrillPoints3;

@property (weak, nonatomic) IBOutlet UILabel *lDrillDate1;
@property (weak, nonatomic) IBOutlet UILabel *lDrillDate2;
@property (weak, nonatomic) IBOutlet UILabel *lDrillDate3;


@property (weak, nonatomic) HMSegmentedControl* tabs;

@end

@implementation GMQuizScoreboardViewController{
    NSArray* drills;
    int pendingRequests;
    int attepmts;
    BOOL errorOcurred;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self styleAvatarView:self.ivDrillAvatar1];
    [self styleAvatarView:self.ivDrillAvatar2];
    [self styleAvatarView:self.ivDrillAvatar3];
    [GMQuizManager sharedManager].updateDelegate = self;
    attepmts = 5;
    [self attemptLoadDrills];
}

-(void) revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position{
    [super revealController:revealController willMoveToPosition:position];
    if (position == FrontViewPositionLeft){
        [[GMQuizManager sharedManager] updateDrillsAvatars];
        [self showDrills];
    }
        
}

-(NSString*) shareSocialMessageForService:(NSString *)service{
    return NSLocalizedString(@"GM_SHARE_2_MIN_DRILL", nil);
}

-(void) styleAvatarView:(UIImageView*) view{
    view.layer.cornerRadius = view.frame.size.width/2;
    view.layer.masksToBounds = YES;
    view.contentMode = UIViewContentModeScaleAspectFit;
}

-(void) attemptLoadDrills{
    pendingRequests = 2;
    errorOcurred = NO;
    [TSNotifier showProgressWithMessage:@"Loading scores.." onView:self.view];
    [[GMQuizManager sharedManager] performUpdateQuizUserDrills];
    [[GMQuizManager sharedManager] performUpdateQuizLeadersDrills];
}

-(void) onQuizUserDrillsUpdatedResult:(BOOL)success withUserDrills:(NSArray *)_drills orError:(TSError *)error{
    --pendingRequests;
    errorOcurred = errorOcurred || !success;
    if (errorOcurred || pendingRequests == 0)
        [TSNotifier hideProgressOnView:self.view];
    
    if (errorOcurred){
        if (attepmts > 0){
            --attepmts;
            [self attemptLoadDrills];
        }
        else{
            [TSNotifier notify:@"Unable to load leaderboard. Please, try again later" onView:self.view];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else if (pendingRequests == 0){
        drills = _drills;
        [self.tabs setSelectedSegmentIndex:0];
        [self showDrills];
    }
    
}

-(void) onQuizLeadersDrillsUpdatedResult:(BOOL)success withLeadersDrills:(NSArray *)_drills orError:(TSError *)error{
    --pendingRequests;
    errorOcurred = errorOcurred || !success;
    if (errorOcurred || pendingRequests == 0)
        [TSNotifier hideProgressOnView:self.view];
        
    if (errorOcurred){
        if (attepmts > 0){
            --attepmts;
            [self attemptLoadDrills];
        }
        else{
            [TSNotifier notify:@"Unable to load leaderboard. Please, try again later" onView:self.view];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else if (pendingRequests == 0){
        drills = [GMQuizManager sharedManager].userDrills;
        [self.tabs setSelectedSegmentIndex:0];
        [self showDrills];
    }
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) viewDidLayoutSubviews{
    if (!self.tabs)
        self.tabs = [self createTabs];
}

-(HMSegmentedControl*) createTabs{
    HMSegmentedControl *segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"MY DRILLS", @"LEADERS"]];
    
    segmentedControl.shouldAnimateUserSelection = YES;
    
    segmentedControl.backgroundColor = colorHexString(@"#1B1C1F");
    segmentedControl.titleTextAttributes = @{NSForegroundColorAttributeName : colorHexString(@"#FFFFFF")};
    segmentedControl.selectionIndicatorColor = colorHexString(@"#00BA42");
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.selectionIndicatorHeight = 4;
    segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    
    segmentedControl.verticalDividerEnabled = YES;
    segmentedControl.verticalDividerColor = colorHexString(@"#26282D");
    segmentedControl.verticalDividerWidth = 1;
    
    [segmentedControl setIndexChangeBlock:^(NSInteger index) {
        switch (index) {
            default:
            case 0:
                drills = [GMQuizManager sharedManager].userDrills;
                [self showDrills];
            break;
            case 1:
                drills = [GMQuizManager sharedManager].leadersDrills;
                [self showDrills];
            break;
        }
    }];
    segmentedControl.frame = CGRectMake(0, 0, self.vTabs.frame.size.width, self.vTabs.frame.size.height);
    NSLog(@"Quiz Scoreboard segmented control: %@",NSStringFromCGRect(segmentedControl.frame));
    [self.vTabs addSubview:segmentedControl];
    return segmentedControl;
}

-(void) showDrills{
    for (NSInteger i = 0; i < TOP_LEADERS_NUMBER; ++i) {
        GMQuizResult* res;
        if (i < drills.count)
            res = [drills objectAtIndex:i];
        else {
            res = [GMQuizResult new];
            res.username = @"Empty";
        }
        [self setTopDrill:res atIndex:i];
    }
    if (drills.count > TOP_LEADERS_NUMBER){
        self.tableView.hidden = NO;
        [self.tableView reloadData];
    }
    else self.tableView.hidden = YES;
}

-(void) setTopDrill:(GMQuizResult*)drill atIndex:(NSInteger) index{
    switch (index) {
        case 0:
            self.ivDrillAvatar1.image = [UIImage imageNamed:drill.userAvatar];
            self.lDrillName1.text = drill.username;
            self.lDrillPoints1.text = [NSString stringWithFormat:@"%d", drill.score];
            self.lDrillDate1.text = drill.playedDate;
            break;
        case 1:
            self.ivDrillAvatar2.image = [UIImage imageNamed:drill.userAvatar];
            self.lDrillName2.text = drill.username;
            self.lDrillPoints2.text = [NSString stringWithFormat:@"%d", drill.score];
            self.lDrillDate2.text = drill.playedDate;
            break;
        case 2:
            self.ivDrillAvatar3.image = [UIImage imageNamed:drill.userAvatar];
            self.lDrillName3.text = drill.username;
            self.lDrillPoints3.text = [NSString stringWithFormat:@"%d", drill.score];
            self.lDrillDate3.text = drill.playedDate;
            break;
        default:
            break;
            
    }
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return (drills.count <=  TOP_LEADERS_NUMBER ? 0 : drills.count - TOP_LEADERS_NUMBER);
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [GMScoreboardCell height];
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"GMScoreboardCell";
    
    GMScoreboardCell* cell = (GMScoreboardCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    GMQuizResult* result = [drills objectAtIndex:indexPath.row + 3];
    [cell setUser:result.username withAvatar:result.userAvatar andPoints:result.score toPosition:indexPath.row + TOP_LEADERS_NUMBER + 1 datePlayed:result.playedDate];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
