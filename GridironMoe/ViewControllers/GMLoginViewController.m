//
//  GMLoginViewController.m
//  GridironMoe
//
//  Created by Adya on 8/17/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMLoginViewController.h"
#import "GMUserManager.h"
#import "TSNotifier.h"
#import "GMRequestManager.h"
#import "GMUser.h"
#import "GMStorage.h"
#import "KLCPopup.h"
#import "GMLoginView.h"
#import "TSUtils.h"
#import "GMMoeGenerator.h"
#import "GMForgotPasswordView.h"
#import "TSError.h"

@interface GMLoginViewController ()<GMLoginDelegate>
@property (weak, nonatomic) IBOutlet UITextField *tfUsername;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword;
@property (weak, nonatomic) IBOutlet UIImageView *ivFamily;

@end

@implementation GMLoginViewController{
    KLCPopup* popup;
    GMLoginView* loginView;
    GMForgotPasswordView* resetView;
    BOOL popupLogin;
    BOOL resetUsername;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [GMUserManager sharedManager].loginDelegate = self;
    self.enableFloatingMoes = NO;
    [[GMStorage sharedStorage] putValue:self forKey:STORAGE_REF_LOGIN_VIEW_CONTROLLER];
    if ([GMUserManager sharedManager].hasStoredUser){
        popupLogin = NO;
        [TSNotifier showProgressWithMessage:NSLocalizedString(@"GM_PROGRESS_LOGIN", nil) onView:self.view];
        [[GMUserManager sharedManager] performLoginWithSavedUser];
    }
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tfUsername.text = [GMUserManager sharedManager].user.username;
    //if (loginView)
    //    [loginView setDefaultUserame:[GMUserManager sharedManager].user.username];
    if ([GMUserManager sharedManager].hasStoredUser){
        self.tfPassword.text = [GMUserManager sharedManager].user.password;
    }
    else{
        self.tfPassword.text = @"";
    }
}

// login through popup view
-(void) initLoginPopup{
    loginView = [[[NSBundle mainBundle] loadNibNamed:@"GMLoginView" owner:self options:nil] lastObject];
    __weak GMLoginViewController* weakSelf = self;
    __weak UIView* weakLoginView = loginView;
    popup = [KLCPopup popupWithContentView:loginView showType:KLCPopupShowTypeGrowIn dismissType:KLCPopupDismissTypeGrowOut  maskType:KLCPopupMaskTypeDimmed dismissOnBackgroundTouch:YES dismissOnContentTouch:NO];
    [loginView setLoginCallback:^(NSString *username, NSString *password) {
        popupLogin = YES;
        [weakSelf performLoginWithUsername:username andPassword:password senderView:weakLoginView];
    }];
}

-(KLCPopup*) weakPopup{
    return popup;
}

// login through popup view
-(void) initResetPasswordPopup{
    resetView = [[[NSBundle mainBundle] loadNibNamed:@"GMForgotPasswordView" owner:self options:nil] lastObject];
    __weak GMLoginViewController* weakSelf = self;
    __weak UIView* weakResetView = resetView;
    popup = [KLCPopup popupWithContentView:resetView showType:KLCPopupShowTypeGrowIn dismissType:KLCPopupDismissTypeGrowOut  maskType:KLCPopupMaskTypeDimmed dismissOnBackgroundTouch:YES dismissOnContentTouch:NO];
    [resetView setEditedCallback:^(NSString *username, NSString* email) {
        if (email){ // if username == nil popup was canceled
            username = trim(username);
            email = trim(email);
            if (username && ![[GMUserManager sharedManager] isValidUsername:username]){
                [TSNotifier notify:@"Username invalid." onView:weakResetView];
            }
            else if (![TSUtils isValidEmail:email]){
                [TSNotifier notify:@"Email invalid." onView:weakResetView];
            }
            else{
                if (username){
                    resetUsername = NO;
                    [TSNotifier alert:@"You are about to reset your password. New password will be sent to the given email if it is connected to your account. Are you sure?" acceptButton:@"Yes, reset it" acceptBlock:^{
                        [TSNotifier showProgressWithMessage:@"Hold on.." onView:weakResetView];
                        [[GMUserManager sharedManager] performForgotPasswordForUsername:username andEmail:email];
                    } cancelButton:@"Cancel" cancelBlock:^{
                        [[weakSelf weakPopup] dismiss:YES];
                    }];
                }
                else {
                    resetUsername = YES;
                    [TSNotifier showProgressWithMessage:@"Sending username.." onView:weakResetView];
                    [[GMUserManager sharedManager] performForgotPasswordForUsername:nil andEmail:email];
                }
            }
        }
        else {
            [[weakSelf weakPopup] dismiss:YES];
        }
    }];
}

-(void) performLoginWithUsername:(NSString*) username andPassword:(NSString *)password senderView:(UIView*) senderView{
    username = trim(username);
    password = trim(password);
    if (![[GMUserManager sharedManager] isValidUsername:username]){
        [TSNotifier notify:@"Username invalid." onView:senderView];
    }
    else if (![[GMUserManager sharedManager] isValidPassword:password]){
        [TSNotifier notify:@"Password invalid." onView:senderView];
    }
    else{
        [TSNotifier showProgressWithMessage:NSLocalizedString(@"GM_PROGRESS_LOGIN", nil) onView:senderView];
        [[GMUserManager sharedManager] performLoginWithUsername:username andPassword:password];
    }
}

- (IBAction)loginClick:(id)sender {
    // [self initLoginPopup];
 //   [popup show]; // this will trigger login through popup
    
    NSString* username = trim(self.tfUsername.text);
    NSString* password = trim(self.tfPassword.text);
    popupLogin = NO;
    [self performLoginWithUsername:username andPassword:password senderView:self.view];
}
- (IBAction)resetPassword:(id)sender {
    [self initResetPasswordPopup];
    [popup showWithLayout:KLCPopupLayoutMake(KLCPopupHorizontalLayoutCenter, KLCPopupVerticalLayoutAboveCenter)];
    [resetView startEdit];
}


- (IBAction)facebookClick:(id)sender {
    [[GMUserManager sharedManager] performLoginWithFacebookFromViewController:self];
}

-(void) onLoginResult:(BOOL)success withUser:(GMUser *)user orError:(TSError *)error{
    [TSNotifier hideProgressOnView:(popupLogin ? loginView : self.view)];
    if (success){
        //[popup dismiss:YES];
        [self performSegueWithIdentifier:@"segLoginMain" sender:self];
        self.tfPassword.text = @"";
    }
    else{
        [TSNotifier alert:NSLocalizedString(@"GM_ALERT_LOGIN_FAILED", nil)];
    }
}

-(void) onForgotPasswordResult:(BOOL)success orError:(TSError *)error{
    [TSNotifier hideProgressOnView:resetView];
    if (success){
        [popup dismiss:YES];
        if (resetUsername){
            [TSNotifier alert:@"We've sent you your username to the email you provided. Thank you."];
        }
        else{
            [TSNotifier alert:@"We've sent you a temporary password to the given email. You can set your password and email in Settings."];
        }
    }
    else{
        [TSNotifier alert:@"Sorry, password reset failed. Please, try again later."];
    }
}

@end
