//
//  GMQuizQuestionViewController.m
//  GridironMoe
//
//  Created by Adya on 8/13/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMQuizQuestionViewController.h"
#import "LMAlertView.h"
#import "GMQuizAnswerView.h"
#import "GMQuizManager.h"
#import "GMQuestion.h"
#import "GMQuizResult.h"
#import "TSNotifier.h"
#import "SWRevealViewController.h"
#import "GMStorage.h"
#import "KLCPopup.h"

@interface GMQuizQuestionViewController () <GMQuizGameDelegate, TSShareDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lTimer;
@property (weak, nonatomic) IBOutlet UILabel *lScore;
@property (weak, nonatomic) IBOutlet UILabel *lQuestion;
@property (weak, nonatomic) IBOutlet UIScrollView *svQuestions;


// This is done because of simple buttons didn't want to stretch their height to fit answer size.

@property (weak, nonatomic) IBOutlet UITextView *tvAnswer1;
@property (weak, nonatomic) IBOutlet UITextView *tvAnswer2;
@property (weak, nonatomic) IBOutlet UITextView *tvAnswer3;

@property (weak, nonatomic) IBOutlet UIImageView *ivAnswer1;
@property (weak, nonatomic) IBOutlet UIImageView *ivAnswer2;
@property (weak, nonatomic) IBOutlet UIImageView *ivAnswer3;

@property (weak, nonatomic) IBOutlet UIButton *bAnswer;

@end

@implementation GMQuizQuestionViewController{
    GMQuizManager* gameManager;
    GMQuestion* currentQuestion;
    BOOL questionLayouted;
    int selectedIndex;
    NSMutableDictionary* shuffledAnswers; //Tag-Index Pairs
    NSArray* answers;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.enableFloatingMoes = NO;
    gameManager = [GMQuizManager sharedManager];
    gameManager.gameDelegate = self;
    shuffledAnswers = [NSMutableDictionary new];
    [self loadNextQuestion];
    [self initAnswersTap];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [gameManager startQuiz];
}

-(void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    gameManager.gameDelegate = nil;
    if (gameManager.isRunning)
        [gameManager stopQuiz];
}

-(NSString*) shareSocialMessageForService:(NSString *)service{
    return NSLocalizedString(@"GM_SHARE_2_MIN_DRILL", nil);
}

-(void) revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position{
    [super revealController:revealController didMoveToPosition:position];
    if (position == FrontViewPositionLeft)
        [gameManager startQuiz];
    else
        [gameManager pauseQuiz];
}

-(void) initAnswersTap{
    [self.tvAnswer1 addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(answerClick:)]];
    [self.tvAnswer2 addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(answerClick:)]];
    [self.tvAnswer3 addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(answerClick:)]];
}

-(void) setTimeLabel:(int) time{
    int mins = time / 60;
    int sec = time % 60;
    self.lTimer.text = [NSString stringWithFormat:@"%02d : %02d", mins, sec];
}

-(void) setScoreLabel:(int)score{
    self.lScore.text = [NSString stringWithFormat:@"Score: %d", score];
}

-(void) setQuestion:(GMQuestion*) question{
    int qNo = gameManager.quizResult.total + 1;
    NSUInteger digits = [NSString stringWithFormat:@"%d",qNo].length; // calculate length of number to set bold text
    NSString* qStr = [NSString stringWithFormat:@"Q%d. %@. %@", qNo, question.superbowl, question.question];
    NSMutableAttributedString* str = [[NSMutableAttributedString alloc] initWithString:qStr];
    [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:17] range:NSMakeRange(0, digits + 2)]; // +2 for "Q" and "."
    [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:17] range:NSMakeRange(digits + 2, str.length - digits - 2)];
    self.lQuestion.attributedText = str;
    answers = @[currentQuestion.answer, currentQuestion.wrongAnswer1, currentQuestion.wrongAnswer2];
    [shuffledAnswers removeAllObjects];
    int index = 0;
    while (shuffledAnswers.count != 3) {
        int rndTag = (arc4random() % 3) + 1;
        if (![shuffledAnswers objectForKey:@(rndTag)]){
            [shuffledAnswers setObject:@(index) forKey:@(rndTag)];
            ++index;
        }
    }
    [self setAnswer:self.tvAnswer1];
    [self setAnswer:self.tvAnswer2];
    [self setAnswer:self.tvAnswer3];
    questionLayouted = NO;
  //  [self.view layoutIfNeeded];
}

-(void) setSelected:(BOOL)isSelected answer:(UIImageView*) selectedView{
    [selectedView setImage:[UIImage imageNamed:(isSelected?@"quiz_answer_tab_hover":@"quiz_answer_tab")]];
}

-(void) setAnswer:(UITextView*) answer{
    int index = [[shuffledAnswers objectForKey:@(answer.tag)] intValue];
    NSString* letter = [@[@"A", @"B", @"C"] objectAtIndex:(answer.tag-1)];
    NSString* title = [NSString stringWithFormat:@"%@: %@", letter, [answers objectAtIndex:index]];
    answer.selectable = YES; // weird bug when UITextView resets it's font&color if "selectable" property set to NO. Weird enough, isn't it?
    answer.text = title;
    CGFloat topCorrect = (answer.bounds.size.height - answer.contentSize.height)/2.0;
   // topCorrect = ( topCorrect < 0.0 ? 0.0 : topCorrect);
    answer.contentOffset = (CGPoint){.x = 0, .y = -topCorrect};
    answer.selectable = NO;
}

-(void) loadNextQuestion{
    currentQuestion = [gameManager nextQuestion];
    selectedIndex = -1;
    [self setSelected:NO answer:self.ivAnswer1] ;
    [self setSelected:NO answer:self.ivAnswer2] ;
    [self setSelected:NO answer:self.ivAnswer3] ;
    [self setQuestion:currentQuestion];
}

-(void) onQuizStarted{
    [self setTimeLabel:gameManager.timeLeft];
    [self loadNextQuestion];
}

-(void) onQuizEnded:(GMQuizResult *)result{
    [TSNotifier notify:@"Time's Up!" onView:self.view];
    [[GMStorage sharedStorage] putValue:gameManager.quizResult forKey:STORAGE_VALUE_QUIZ_RESULT];
    [self performSegueWithIdentifier:@"segQuizQuestionQuizResult" sender:self];
}

-(void) onQuizTimerTick:(int)timeLeft{
    [self setTimeLabel:timeLeft];
}

- (void) answerClick:(id)sender {
    UITapGestureRecognizer* tap = sender;
    selectedIndex = [[shuffledAnswers objectForKey:@(tap.view.tag)] intValue];
    [self setSelected:(tap.view == self.tvAnswer1) answer:self.ivAnswer1] ;
    [self setSelected:(tap.view == self.tvAnswer2) answer:self.ivAnswer2] ;
    [self setSelected:(tap.view == self.tvAnswer3) answer:self.ivAnswer3] ;
    [self blinkCallPlayButton];
}

- (void)blinkCallPlayButton {
    CGFloat oldAlpha = self.bAnswer.alpha;
    CGFloat minAlpha = 0.55;
    enum UIViewAnimationOptions options = UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionAutoreverse | UIViewAnimationOptionCurveEaseInOut;
    
    [UIView animateWithDuration:.5 delay:0.3 options:options animations:^{
        [UIView setAnimationRepeatCount:20];
        self.bAnswer.alpha = minAlpha;
    }
                     completion:^(BOOL finished) {
                         self.bAnswer.alpha = oldAlpha;
                     } ];
}

- (IBAction)acceptClick:(id)sender {
    GMQuizAnswerView* view = [[[NSBundle mainBundle] loadNibNamed:@"GMQuizAnswerView" owner:self options:nil] lastObject];
    KLCPopup* popup = [KLCPopup popupWithContentView:view showType:KLCPopupShowTypeGrowIn dismissType:KLCPopupDismissTypeGrowOut  maskType:KLCPopupMaskTypeDimmed dismissOnBackgroundTouch:NO dismissOnContentTouch:NO];
    if (selectedIndex == -1){
        [view setAlertWithTitle:@"You Have to Pick One" andMessage:@"Please select your answer" withCompletion:^{
            [popup dismiss:YES];
        }];
        [popup show];
        return;
    }
    [self.bAnswer.layer removeAllAnimations];
    NSString* answer = [answers objectAtIndex:selectedIndex];
    BOOL success = [gameManager answerQuestion:currentQuestion withChoice:answer];
    if (success){
        [self setScoreLabel:gameManager.quizResult.score];
    }
    
    
    __block int index = 0;
    [shuffledAnswers enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if ([obj intValue] == 0){
            index = [key intValue] - 1;
            *stop = YES;
        }
    }];
    
    NSString* letter = [@[@"A", @"B", @"C"] objectAtIndex:(index)];
    
    [view setAnswer:success withLetter:letter andPoints:gameManager.pointsPerAnswer orRedirectBlock:nil withCompletion:^{
        [popup dismiss:YES];
        [self loadNextQuestion];
        [gameManager startQuiz];
    }];
    [popup show];
    [gameManager pauseQuiz];
}

-(void) onShareViewWillAppearWithService:(NSString *)service{
    [gameManager pauseQuiz];
}

-(void) onShareViewDidDisappearWithService:(NSString *)service shared:(BOOL) shared{
    [gameManager startQuiz];
}

@end
