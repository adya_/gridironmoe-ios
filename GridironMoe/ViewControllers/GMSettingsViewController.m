//
//  GMSettingsViewController.m
//  GridironMoe
//
//  Created by Adya on 8/20/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMSettingsViewController.h"
#import "TSUtils.h"
#import "GMSelectMoeView.h"
#import "KLCPopup.h"
#import "GMUserManager.h"
#import "GMEditPasswordView.h"
#import "GMEditEmailView.h"
#import "GMUser.h"
#import "TSNotifier.h"
#import "GMMoeGenerator.h"
#import "TSNotificationManager.h"

static NSString* const kEmail = @"email";
static NSString* const kPassword = @"password";
static NSString* const kMoeFace = @"moe_face";
static NSString* const kRandomMoe = @"random_moe";
static NSString* const kConnectFB = @"connect_fb";
static NSString* const kConnectTW = @"connect_tw";
static NSString* const kGameNotifications = @"game_notif";
static NSString* const kPushNotifications = @"push_notif";

@interface GMSettingsViewController () <UITableViewDelegate, UITableViewDataSource, GMUserProfileDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

typedef NS_ENUM(NSInteger, GMRequestType) {
    GM_CHANGE_PASSWORD,
    GM_CHANGE_EMAIL
};

@implementation GMSettingsViewController{
    NSArray* items;
    KLCPopup* popup;
    
    GMRequestType requestType;
    
    GMSelectMoeView* selectMoeView;
    GMEditPasswordView* passwordView;
    GMEditEmailView* emailView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    items = @[kEmail, kPassword, //kMoeFace,
              kRandomMoe,
              //kConnectFB, kConnectTW,
              kGameNotifications, kPushNotifications
              ];
    [GMUserManager sharedManager].profileDelegate = self;
    [self initPopups];
}

-(void) changeEmail{
    [self initPopupWithView:emailView];
    [emailView setEmail:[GMUserManager sharedManager].user.email];
    [emailView startEdit];
    [popup showWithLayout:KLCPopupLayoutMake(KLCPopupHorizontalLayoutCenter, KLCPopupVerticalLayoutAboveCenter)];
}

-(void) changePassword{
    [self initPopupWithView:passwordView];
    [passwordView startEdit];
    [popup showWithLayout:KLCPopupLayoutMake(KLCPopupHorizontalLayoutCenter, KLCPopupVerticalLayoutAboveCenter)];
}

-(void) changeMoeFace{
    [self initPopupWithView:selectMoeView];
    [popup show];
}

-(void) enabledRandomMoe{
    BOOL enabled = ![GMMoeGenerator isEnabled];
    [GMMoeGenerator setEnabled:enabled];
    UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[items indexOfObject:kRandomMoe] inSection:0]];
    UIImageView* toggle = (UIImageView*)[cell viewWithTag:1];
    [self setToggle:toggle selected:enabled];
    if (enabled)
        [GMMoeGenerator startSpawnOnView:self.view];
}

-(void) initPopups{
    [self initMoePopup];
    [self initEmailPopup];
    [self initPasswordPopup];
}

-(void) initPopupWithView:(UIView*)view{
    popup = [KLCPopup popupWithContentView:view showType:KLCPopupShowTypeGrowIn dismissType:KLCPopupDismissTypeGrowOut  maskType:KLCPopupMaskTypeDimmed dismissOnBackgroundTouch:YES dismissOnContentTouch:NO];
}

-(KLCPopup*) weakPopup{
    return popup;
}

-(void) initMoePopup{
    selectMoeView = [[[NSBundle mainBundle] loadNibNamed:@"GMSelectMoeView" owner:self options:nil] lastObject];
    __weak GMSettingsViewController* weakSelf = self;
    __weak NSArray* weakItems = items;
        [selectMoeView setSelectMoeCallback:^(GMMoeFace *moeFace) {
        [[weakSelf weakPopup] dismiss:YES];
        [[GMUserManager sharedManager] selectMoe:moeFace];
        [weakSelf updateMoeFace];
        UITableViewCell* cell = [weakSelf.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[weakItems indexOfObject:kMoeFace] inSection:0]];
            UIImageView* moe = (UIImageView*)[cell viewWithTag:1];
            moe.image = [GMUserManager sharedManager].moeFaceImage;
    }];
}

-(void) initEmailPopup{
    emailView = [[[NSBundle mainBundle] loadNibNamed:@"GMEditEmailView" owner:self options:nil] lastObject];
    __weak GMSettingsViewController* weakSelf = self;
    __weak id weakView = emailView;
    [emailView setEditedCallback:^(NSString *email) {
        [weakSelf dismissKeyboard];
        if (email){
            if ([email isEqualToString:[GMUserManager sharedManager].user.email]){
                [TSNotifier notify:@"Please change your email" onView:weakView];
            }
            else{
                [TSNotifier showProgressWithMessage:@"Saving.." onView:weakView];
                requestType = GM_CHANGE_EMAIL;
                [[GMUserManager sharedManager] performChangeEmail:email];
            }
        }
        else
            [[weakSelf weakPopup] dismiss:YES];
    }];

}

-(void) initPasswordPopup{
    passwordView = [[[NSBundle mainBundle] loadNibNamed:@"GMEditPasswordView" owner:self options:nil] lastObject];
    __weak GMSettingsViewController* weakSelf = self;
    __weak GMEditPasswordView* weakView = passwordView;
    [passwordView setEditedCallback:^(NSString *oldPassword, NSString *newPassword, NSString *confirmPassword) {
        [weakSelf dismissKeyboard];
        if (!oldPassword && !newPassword && !confirmPassword){
           [[weakSelf weakPopup] dismiss:YES];
            return;
        }
        GMUser* user = [GMUserManager sharedManager].user;
        if (!oldPassword || !newPassword || !confirmPassword){
            [TSNotifier notify:@"Required fields missing" onView:weakView];
            return;
        }
        else if (![oldPassword isEqualToString:user.password]){
            [TSNotifier notify:@"Old password is wrong" onView:weakView];
            return;
        }
        else if (![newPassword isEqualToString:confirmPassword]) {
            [TSNotifier notify:@"New password doesn't match Confirm password" onView:weakView];
            return;
        }
        else if ([newPassword isEqualToString:user.password]){
            [TSNotifier notify:@"Password must be different" onView:weakView];
            return;
        }
        else{
            [TSNotifier showProgressWithMessage:@"Saving.." onView:weakView];
            requestType = GM_CHANGE_PASSWORD;
            
            [[GMUserManager sharedManager] performChangePassword:newPassword];
            [weakView clear];
        }
        
    }];

}

-(void) onUserProfileUpdated:(BOOL)success withUser:(GMUser *)user orError:(TSError *)error{
    [TSNotifier hideProgressOnView:popup.contentView];
    if (success){
        switch (requestType) {
            case GM_CHANGE_EMAIL:
                [TSNotifier notify:@"Email changed successfully!" onView:self.view];
                break;
               case GM_CHANGE_PASSWORD:
                [TSNotifier notify:@"Password changed successfully!" onView:self.view];
                [[GMUserManager sharedManager] logout];
                break;
            default:
                break;
        }
        [popup dismiss:YES];
    }
    else
        [TSNotifier alertError:error];
}

-(void) connectFB{
    
}

-(void) connectTW{
    
}

-(void) gameNotifications{
    UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[items indexOfObject:kGameNotifications] inSection:0]];
    BOOL enabled = ![TSNotificationManager isHandleNotificationsEnabled];
    [TSNotificationManager setHandleNotificationsEnabled:enabled];
    UIImageView* toggle = (UIImageView*)[cell viewWithTag:1];
    [self setToggle:toggle selected:enabled];
    
}

-(void) pushNotifications{
    UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[items indexOfObject:kPushNotifications] inSection:0]];
    BOOL enabled = ![TSNotificationManager isNotificationsEnabled];
    [TSNotificationManager setNotificationsEnabled:enabled];
    UIImageView* toggle = (UIImageView*)[cell viewWithTag:1];
    [self setToggle:toggle selected:enabled];
}

-(void) setToggle:(UIImageView*)iv selected:(BOOL) selected{
    iv.image = [UIImage imageNamed:(selected?@"toggle_on" : @"toggle_off")];
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return items.count;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:[items objectAtIndex:indexPath.row]];
    UIView *bgColorView = [UIView new];
    bgColorView.backgroundColor = colorRGB(16, 16, 16);
    [cell setSelectedBackgroundView:bgColorView];
    
    if ([items objectAtIndex:indexPath.row] == kGameNotifications){
        UIImageView* toggle = (UIImageView*)[cell viewWithTag:1];
        [self setToggle:toggle selected:[TSNotificationManager isHandleNotificationsEnabled]];
    }
    else if ([items objectAtIndex:indexPath.row] == kPushNotifications){
        UIImageView* toggle = (UIImageView*)[cell viewWithTag:1];
        [self setToggle:toggle selected:[TSNotificationManager isNotificationsEnabled]];
    }
    else if ([items objectAtIndex:indexPath.row] == kMoeFace){
        UIImageView* moe = (UIImageView*)[cell viewWithTag:1];
        moe.image = [GMUserManager sharedManager].moeFaceImage;
    }
    else if ([items objectAtIndex:indexPath.row] == kRandomMoe){
        UIImageView* toggle = (UIImageView*)[cell viewWithTag:1];
        [self setToggle:toggle selected:[GMMoeGenerator isEnabled]];
    }

    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    if ([items objectAtIndex:indexPath.row] == kEmail)
        [self changeEmail];
    else if ([items objectAtIndex:indexPath.row] == kPassword)
        [self changePassword];
    else if ([items objectAtIndex:indexPath.row] == kMoeFace)
        [self changeMoeFace];
    else if ([items objectAtIndex:indexPath.row] == kRandomMoe)
        [self enabledRandomMoe];
    else if ([items objectAtIndex:indexPath.row] == kConnectFB)
        [self connectFB];
    else if ([items objectAtIndex:indexPath.row] == kConnectTW)
        [self connectTW];
    else if ([items objectAtIndex:indexPath.row] == kGameNotifications)
        [self gameNotifications];
    else if ([items objectAtIndex:indexPath.row] == kPushNotifications)
        [self pushNotifications];
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
