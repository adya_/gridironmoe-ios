//
//  GMQuizResultsViewController.m
//  GridironMoe
//
//  Created by Adya on 8/17/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMQuizResultsViewController.h"
#import "GMQuizResult.h"
#import "GMStorage.h"
#import "TSNotifier.h"
#import "GMQuizManager.h"

@interface GMQuizResultsViewController () <GMQuizDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lAnswers;
@property (weak, nonatomic) IBOutlet UILabel *lPoints;

@end

@implementation GMQuizResultsViewController{
    GMQuizResult* result;
    int attepmts;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    result = [[GMStorage sharedStorage] getValueForKey:STORAGE_VALUE_QUIZ_RESULT];
    self.lAnswers.text = [NSString stringWithFormat:@"Correct Answers:\n%d of %d", result.right, result.total];
    self.lPoints.text = [NSString stringWithFormat:@"You've Earned\n%d Points", result.score];
    [GMQuizManager sharedManager].updateDelegate = self;
    attepmts = 10;
    [TSNotifier showProgressWithMessage:@"Saving your result.." onView:self.view];
    [[GMQuizManager sharedManager] performSendUserQuizResults:result];
}

-(NSString*) shareSocialMessageForService:(NSString *)service{
    return NSLocalizedString(@"GM_SHARE_2_MIN_DRILL", nil);
}

-(void) onUserQuizResultSentResult:(BOOL)success orError:(TSError *)error{
    if (!success){
        if (attepmts == 0){
            [TSNotifier hideProgressOnView:self.view];
            [TSNotifier notify:@"Sorry, we've lost your result." onView:self.view];
        }
        else{
            --attepmts;
            [[GMQuizManager sharedManager] performSendUserQuizResults:result];
        }
    }
    else{
        [TSNotifier hideProgressOnView:self.view];
        [TSNotifier notify:@"Your score has been saved!" onView:self.view];
    }
}


- (IBAction)playAgain:(id)sender {
    [self.navigationController popToViewController:[[GMStorage sharedStorage] getValueForKey:STORAGE_REF_QUIZ_READY_VIEW_CONTROLLER] animated:YES];
}

@end
