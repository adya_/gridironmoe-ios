//
//  GMSplashViewController.m
//  GridironMoe
//
//  Created by Adya on 10/18/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMSplashViewController.h"
#import "GMStorage.h"
#import "GMUserManager.h"
#import "TSNotifier.h"
#import "GMGameManager.h"

@interface GMSplashViewController () <GMGameDelegate>

@end

@implementation GMSplashViewController{
    int pendingRequests;
    BOOL errorOccured;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [GMGameManager sharedManager].delegate = self;
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self load];
}

-(void) load{
    pendingRequests = 2;
    errorOccured = NO;
    [TSNotifier showProgressWithMessage:@"Loading.." onView:self.view];
    [[GMStorage sharedStorage] preloadStorageWithCompletion:^(BOOL success, TSError *error) {
        pendingRequests--;
        if (success){
            [[GMUserManager sharedManager] loadMoe];
            [self handleSuccess];

        }
        else{
            [self handleError:error];
        }
    }];
    [[GMGameManager sharedManager] performUpdateGames];
}

-(void) onGamesUpdated:(BOOL)success withScheduledGames:(NSArray *)scheduled andActiveGames:(NSArray *)active orError:(TSError *)error{
    pendingRequests--;
    if (success){
        [self handleSuccess];
    }
    else{
        [self handleError:error];
    }
}

-(void) handleError:(TSError*) error{
    if (pendingRequests == 0){
        [TSNotifier hideProgressOnView:self.view];
        if (errorOccured){
            [TSNotifier alert:@"Unable to connect server. Please, try again." acceptButton:@"Try again" acceptBlock:^{
                [self load];
            }];
        }
    }

}

-(void) handleSuccess{
    if (pendingRequests == 0 && !errorOccured)
        [self performSegueWithIdentifier:@"segLaunch" sender:self];
}

@end
