//
//  GMHowToPlayViewController.m
//  GridironMoe
//
//  Created by Adya on 8/20/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMPlaybookViewController.h"
#import "HMSegmentedControl.h"
#import "TSUtils.h"
#import "GMStorage.h"
#import "GMStrategy.h"

@interface GMPlaybookViewController () <UIPickerViewDelegate, UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *vTabs;
@property (weak, nonatomic) IBOutlet UIPickerView *pvStrategies;
@property (weak, nonatomic) IBOutlet UILabel *lDetails;
@property (weak, nonatomic) HMSegmentedControl* tabs;
@end

@implementation GMPlaybookViewController{
    NSArray* strategies;
    BOOL defStrategies;
    NSInteger selectedDef;
    NSInteger selectedOff;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self switchStrategies:NO];
    [self selectStrategyAtIndex:0];
}


-(void) switchStrategies:(BOOL) toDef{
    if (toDef){
        defStrategies = YES;
        strategies = [[GMStorage sharedStorage] getValueForKey:STORAGE_ARRAY_OFFENSE_STRATEGIES];
    }
    else{
        defStrategies = NO;
        strategies = [[GMStorage sharedStorage] getValueForKey:STORAGE_ARRAY_DEFENSE_STRATEGIES];
    }
    
    [self showStrategies];
}

-(void) selectStrategyAtIndex:(NSInteger) index{
    if (defStrategies)
        selectedDef = index;
    else
        selectedOff = index;
    self.lDetails.text = [(GMStrategy*)[strategies objectAtIndex:index] details];
    [self.lDetails sizeToFit];
}

-(void) viewDidLayoutSubviews{
    if (!self.tabs)
        self.tabs = [self createTabs];
}

-(HMSegmentedControl*) createTabs{
    HMSegmentedControl *segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"OFFENSE", @"DEFENSE"]];
    
    segmentedControl.shouldAnimateUserSelection = YES;
    
    segmentedControl.backgroundColor = colorHexString(@"#1B1C1F");
    segmentedControl.titleTextAttributes = @{NSForegroundColorAttributeName : colorHexString(@"#FFFFFF"),
                                             NSFontAttributeName:[UIFont systemFontOfSize:20]};
    segmentedControl.selectionIndicatorColor = colorHexString(@"#00BA42");
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.selectionIndicatorHeight = 4;
    segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    
    segmentedControl.verticalDividerEnabled = YES;
    segmentedControl.verticalDividerColor = colorHexString(@"#26282D");
    segmentedControl.verticalDividerWidth = 1;
    
    [segmentedControl setIndexChangeBlock:^(NSInteger index) {
        switch (index) {
            default:
            case 0:
                [self switchStrategies:NO];
                break;
            case 1:
                [self switchStrategies:YES];
                break;
        }
    }];
    segmentedControl.frame = CGRectMake(0, 0, self.vTabs.frame.size.width, self.vTabs.frame.size.height);
    NSLog(@"Playbook segmented control: %@",NSStringFromCGRect(segmentedControl.frame));
    [self.vTabs addSubview:segmentedControl];
    return segmentedControl;
}

-(void) showStrategies{
    [self.pvStrategies reloadAllComponents];
    NSInteger cur = (defStrategies ? selectedDef : selectedOff);
    [self.pvStrategies selectRow:cur inComponent:0 animated:NO];
    [self selectStrategyAtIndex:cur];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [(GMStrategy*)[strategies objectAtIndex:row] name];
}

-(UIView*) pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel*label = (UILabel*) view;
    if (!label)
        label = [UILabel new];
    label.font = [UIFont boldSystemFontOfSize:24];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = [self pickerView:pickerView titleForRow:row forComponent:component];
    return label;
}

-(CGFloat) pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 60;
}

-(NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return strategies.count;
}

-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    [self selectStrategyAtIndex:row];
}

@end
