//
//  GMVoteResultViewController.m
//  GridironMoe
//
//  Created by Adya on 8/28/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMVoteResultViewController.h"
#import "GMGameSchedule.h"
#import "GMGameManager.h"
#import "GMTeam.h"
#import "TSNotifier.h"
#import "TSUtils.h"
#import "GMVoteResult.h"
#import "GMStorage.h"
#import "GMStrategy.h"
#import "GMGameLive.h"

#define GAME_VIEW_SELECTED_COLOR colorHexString(@"#00BA42")
#define GAME_VIEW_DESELECTED_COLOR [UIColor clearColor]

#define TAG_OFFENCE_VOTE_VIEW 100
#define TAG_DEFENSE_VOTE_VIEW 200

#define TAG_SELECTED_VOTE 1
#define TAG_AGREED_PLAYERS 2
#define TAG_POINTS_EARNED 3
#define TAG_TOP_PLAYED 4

#define TAG_SCHEDULE_TEAM_LABEL 1
#define TAG_SCHEDULE_DATE_LABEL 2
#define TAG_SCHEDULE_TIME_LABEL 3


// game view holder
@interface GMStatsView : UIView
@property (weak, nonatomic) IBOutlet UILabel* lSelectedVote;
@property (weak, nonatomic) IBOutlet UILabel* lPlayers;
@property (weak, nonatomic) IBOutlet UILabel* lPoints;
@property (weak, nonatomic) IBOutlet UILabel* lTop;
@end

@implementation GMStatsView
@synthesize lSelectedVote, lPoints, lPlayers, lTop;

-(id) initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    [self linkViews];
    return  self;
}

-(id) initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    [self linkViews];
    return self;
}

-(void) linkViews{
    lSelectedVote = (UILabel*)[self viewWithTag:self.tag + TAG_SELECTED_VOTE];
    lPlayers = (UILabel*)[self viewWithTag:self.tag + TAG_AGREED_PLAYERS];
    lPoints = (UILabel*)[self viewWithTag:self.tag + TAG_POINTS_EARNED];
    lTop = (UILabel*)[self viewWithTag:self.tag + TAG_TOP_PLAYED];
}
@end


@interface GMVoteResultViewController () <GMGameDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lTotal;
@property (weak, nonatomic) IBOutlet UICollectionView *cvSchedule;
@property (weak, nonatomic) IBOutlet UILabel *lNoSchedule;
@property (weak, nonatomic) IBOutlet GMStatsView *vOffenceStats;
@property (weak, nonatomic) IBOutlet GMStatsView *vDefenseStats;
@property (weak, nonatomic) IBOutlet UIView *vTotalStats;
@property (weak, nonatomic) IBOutlet UIScrollView *svContent;

@end

@interface GMVoteResultViewController (Schedule)
-(void) reloadSchedule;
@end

@implementation GMVoteResultViewController{
    NSArray* games;
    GMGame* votedGame;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self showVoteResults];
    [self reloadSchedule];
}

-(void) viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.svContent.contentSize = CGSizeMake(self.svContent.frame.size.width, self.vTotalStats.frame.origin.y + self.vTotalStats.frame.size.height);
}

-(void) showVoteResults{
    GMVoteResult* res = [[GMStorage sharedStorage] getValueForKey:STORAGE_VALUE_VOTE_RESULT];
    GMStrategy* offStr = res.offenseVote;
    GMStrategy* defStr = res.defenseVote;
    GMStatsView* offView = self.vOffenceStats;
    GMStatsView* defView = self.vDefenseStats;
    GMGame* game = [[GMStorage sharedStorage] getValueForKey:STORAGE_VALUE_VOTE_GAME];
    BOOL isLive = game != nil;
    offView.lTop.adjustsFontSizeToFitWidth = YES;
    defView.lTop.adjustsFontSizeToFitWidth = YES;
    if (offStr){
        offView.lSelectedVote.text = offStr.name;
        offView.lPoints.text = [NSString stringWithFormat:@"%d", res.offensePoints];
        offView.lPlayers.text = [NSString stringWithFormat:@"%d%%", res.offenseAgreed];
        offView.lTop.text = (res.offenseTopVote ? [NSString stringWithFormat:@"%@ (%d%%)", res.offenseTopVote.name, res.offenseTopVoteRate] : @"");
    }
    else{
        offView.lSelectedVote.text = @"Not selected";
        offView.lPoints.text = @"0";
        offView.lPlayers.text = @"0%";
        offView.lTop.text = @"";
    }

    if (defStr){
        defView.lSelectedVote.text = defStr.name;
        defView.lPoints.text = [NSString stringWithFormat:@"%d", res.defensePoints];
        defView.lPlayers.text = [NSString stringWithFormat:@"%d%%", res.defenseAgreed];
        defView.lTop.text = (res.defenseTopVote ? [NSString stringWithFormat:@"%@ (%d%%)", res.defenseTopVote.name, res.defenseTopVoteRate] : @"");
        [defView.lTop sizeToFit];
    }
    else{
        defView.lSelectedVote.text = @"Not selected";
        defView.lPoints.text = @"0";
        defView.lPlayers.text = @"0%";
        defView.lTop.text = @"";
    }
    [self.view layoutSubviews];
    if (defStr || offStr)
        self.lTotal.text = [NSString stringWithFormat:@"%d", res.defensePoints + res.offensePoints];
    else
        self.lTotal.text = @"0";
    if (!isLive){
        [TSNotifier alert:NSLocalizedString(@"GM_ALERT_MSG_NO_LIVE_GAMES", nil) acceptButton:NSLocalizedString(@"GM_ALERT_BUTTON_SURE", nil) acceptBlock:^{
            [self showViewControllerWithIdentifier:GM_QUIZ_VIEW_CONTROLLER_ID];
        } cancelButton:NSLocalizedString(@"GM_ALERT_BUTTON_NO_THANKS", nil) cancelBlock:nil];
    }
}

- (IBAction)voteAgain:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end


@implementation GMVoteResultViewController (Schedule)


-(void) reloadSchedule{
    NSArray* scheduledGames = [GMGameManager sharedManager].scheduledGames;
    NSArray* activeGames = [GMGameManager sharedManager].activeGames;
    votedGame = [[GMStorage sharedStorage] getValueForKey:STORAGE_VALUE_VOTE_GAME];
    if (activeGames.count > 0)
        games = [activeGames arrayByAddingObjectsFromArray:scheduledGames];
    else
        games = scheduledGames;
    self.lNoSchedule.hidden = (games.count > 0);
    if (games.count > 0)
        [self.cvSchedule reloadData];
}

-(CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger index = indexPath.row;
    if (index % 2 == 0){
        return CGSizeMake(self.cvSchedule.frame.size.height*1.47f, self.cvSchedule.frame.size.height);
    }
    else
        return CGSizeMake(1, self.cvSchedule.frame.size.height);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return ((games.count == 0) ? 0 : games.count * 2); // counts also dividerCells
}

-(UICollectionViewCell*) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger index = indexPath.row;
    NSInteger gameIndex = index / 2;
    UICollectionViewCell* cell;
    if (index % 2 == 0){ // game cell
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"gameCell" forIndexPath:indexPath];
        GMGameSchedule* game = [games objectAtIndex:gameIndex];
        UILabel* lTeams = (UILabel*)[cell viewWithTag:TAG_SCHEDULE_TEAM_LABEL];
        UILabel* lDate = (UILabel*)[cell viewWithTag:TAG_SCHEDULE_DATE_LABEL];
        UILabel* lTime = (UILabel*)[cell viewWithTag:TAG_SCHEDULE_TIME_LABEL];
        lTime.text = game.timeString;
        lDate.text = game.dateString;
        lTeams.text = [NSString stringWithFormat:@"%@ @ %@", game.awayTeam.teamId, game.homeTeam.teamId];
        cell.contentView.backgroundColor = ([game isEqual:votedGame] ? GAME_VIEW_SELECTED_COLOR : GAME_VIEW_DESELECTED_COLOR);
    } // divider cell
    else{
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"dividerCell" forIndexPath:indexPath];
    }
    return cell;
}

-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger index = (indexPath.row % 2 == 0 ? indexPath.row : indexPath.row - 1);
    NSInteger gameIndex = index / 2;
    GMGame* game = [games objectAtIndex:gameIndex];
    if ([[GMGameManager sharedManager].activeGames indexOfObject:game] != NSNotFound && game.status == GM_GAME_LIVE){
        if (![[GMGameManager sharedManager].liveGame isEqual:game]){
            UICollectionViewCell* cell;
            if (votedGame){ // deselect prev one
                NSInteger index = [games indexOfObject:votedGame];
                cell = [collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:index*2 inSection:0]];
                cell.backgroundColor = GAME_VIEW_DESELECTED_COLOR;
            }
            cell = [collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
            cell.backgroundColor = GAME_VIEW_SELECTED_COLOR;
        }
    }
    if (![votedGame isEqual:game]) // if selected another game put it in storage
        [[GMStorage sharedStorage] putValue:game forKey:STORAGE_VALUE_VOTE_SELECTED_GAME];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void) collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    
}


@end



