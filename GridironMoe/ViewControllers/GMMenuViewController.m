//
//  GMMenuTableViewController.m
//  GridironMoe
//
//  Created by Adya on 8/20/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMMenuViewController.h"
#import "TSUtils.h"
#import "GMUserManager.h"
#import "SWRevealViewController.h"
#import "GMStorage.h"
#import "TSNotifier.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "GMUser.h"
#import "GMGameManager.h"
#import "KLCPopup.h"
#import "GMSelectMoeView.h"

@interface GMMenuViewController () <MFMailComposeViewControllerDelegate, GMGameDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *bLive;

@end
static NSString* const kDrill = @"twomindrill";
static NSString* const kPlaybook = @"playbook";
static NSString* const kProfile = @"profile";
static NSString* const kTeams = @"teams";
static NSString* const kLeaderboards = @"leaderboards";
static NSString* const kHowtoplay = @"howtoplay";
static NSString* const kFAQ = @"faq";
static NSString* const kTerms = @"terms";
static NSString* const kSupport = @"support";
static NSString* const kMyMoe = @"mymoe";
static NSString* const kSettings = @"settings";
static NSString* const kLogout = @"logout";


@implementation GMMenuViewController{
    NSArray* menus;
    KLCPopup* popup;
    GMSelectMoeView* selectMoeView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.enableFloatingMoes = NO;
    menus = @[kDrill, kPlaybook, kProfile,
              //kTeams,
              kLeaderboards, kHowtoplay, kMyMoe, kSettings, kFAQ, kTerms, kSupport, kLogout];
    
    [GMGameManager sharedManager].delegate = self;
    [[GMGameManager sharedManager] performUpdateActiveGames];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self updateLiveGameButton];
}

-(void) onActiveGamesUpdated:(BOOL)success withGames:(NSArray *)games orError:(TSError *)error{
    [self updateLiveGameButton];
}


-(void) updateLiveGameButton{
    [self setLive:[GMGameManager sharedManager].activeGames.count != 0];
}

-(void) setLive:(BOOL)isLive{
    NSTextAttachment *attachment = [NSTextAttachment new];
    attachment.image = [UIImage imageNamed:(isLive ? @"sidebar_play_icon_green" : @"sidebar_play_icon_red")];
    attachment.bounds = CGRectMake(0, -8, 30, 30); // 1/4 of size
    UIColor* color = (isLive ? colorHexString(@"#00BA42") : colorHexString(@"D50017"));
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    
    NSMutableAttributedString *title = [NSMutableAttributedString new];
    [title appendAttributedString:attachmentString];
    [title appendAttributedString:[[NSAttributedString alloc] initWithString:@"  LIVE"]];
    [title appendAttributedString:[[NSAttributedString alloc] initWithString:@" Game"]];
    NSRange range = NSMakeRange(0, title.length);
    [title addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:18] range:range];
    [title addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, title.length)];
    [title addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(1, 6)];
    [title addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:18] range:NSMakeRange(1, 6)];
    
    [self.bLive setAttributedTitle:title forState:UIControlStateNormal];
}

-(void) attachRevealViewController{/*Stub to prevent attaching reveal view controller to menu*/}

-(void) performLogout{
    [[GMUserManager sharedManager] logout];
    [[GMUserManager sharedManager] performLogout];
    [self.revealViewController.navigationController popToRootViewControllerAnimated:YES];
}

-(void) sendMail{
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setSubject:@"Gridiron Moe Support"];
        [controller setToRecipients:@[@"moe@gridironmoe.com"]];
#warning Add user's email once it will be aailable.
//        [controller setCcRecipients:@[[GMUserManager sharedManager].user.email]];
        [self presentViewController:controller animated:YES completion:nil];
        [self hideSideMenu];
    } else {
        [TSNotifier notify:@"Device is not configured to send e-mails. Please, contact us through regular e-mail service: moe@gridironmoe.com." onView:self.view];
    }
}

-(KLCPopup*) weakPopup{
    return popup;
}

-(void) initMoePopup{
    selectMoeView = [[[NSBundle mainBundle] loadNibNamed:@"GMSelectMoeView" owner:self options:nil] lastObject];
    __weak id weakSelf = self;
  //  __weak NSArray* weakItems = items;
    [selectMoeView setSelectMoeCallback:^(GMMoeFace *moeFace) {
        [[weakSelf weakPopup] dismiss:YES];
        [[GMUserManager sharedManager] selectMoe:moeFace];
       // [weakSelf updateMoeFace];
//        UITableViewCell* cell = [weakSelf.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[weakItems indexOfObject:kMoeFace] inSection:0]];
//        UIImageView* moe = (UIImageView*)[cell viewWithTag:1];
//        moe.image = [GMUserManager sharedManager].moeFaceImage;
    }];
}


-(void) initPopupWithView:(UIView*)view{
    popup = [KLCPopup popupWithContentView:view showType:KLCPopupShowTypeGrowIn dismissType:KLCPopupDismissTypeGrowOut  maskType:KLCPopupMaskTypeDimmed dismissOnBackgroundTouch:YES dismissOnContentTouch:NO];
}

-(void) chooseMoe{
    [self initMoePopup];
    [self initPopupWithView:selectMoeView];
    [popup show];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        [TSNotifier notify:@"Thanks! We will process your reuest as soon as possible." onView:self.view];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return menus.count;
}

-(UITableViewCell*) tableView:(UITableView*) tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *CellIdentifier = [menus objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    UIView *bgColorView = [UIView new];
    [cell setBackgroundColor:[UIColor clearColor]]; // iOS 7+ bug with white bg...
    bgColorView.backgroundColor = colorRGB(16, 16, 16);
    [cell setSelectedBackgroundView:bgColorView];
    cell.separatorInset = UIEdgeInsetsZero;
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == [menus indexOfObject:kLogout]){
        [self performLogout];
    }
    else if (indexPath.row == [menus indexOfObject:kSupport]){
        [self sendMail];
    }
    else if (indexPath.row == [menus indexOfObject:kMyMoe]){
        [self chooseMoe];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
