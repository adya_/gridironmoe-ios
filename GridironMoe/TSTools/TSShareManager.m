//
//  TSShareManager.m
//  GridironMoe
//
//  Created by Adya on 11/17/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "TSShareManager.h"
#import <Social/Social.h>
#import "TSUtils.h"
#import "TSNotifier.h"

NSString* const TSSocialShareFacebook = @"Facebook";
NSString* const TSSocialShareTwitter = @"Twitter";

@implementation TSShareManager{
    id<TSShareDelegate> shareSocialDelegate;
}

+ (instancetype) sharedManager{
    static TSShareManager* manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{manager = [[self alloc] init];});
    return manager;
}

+(void) setShareSocialDelegate:(id<TSShareDelegate>)delegate{
    [[TSShareManager sharedManager] setShareSocialDelegate:delegate];
}

+(void) shareWithService:(NSString *)service onViewController:(UIViewController *)viewController{
    [[TSShareManager sharedManager] shareWithService:service onViewController:viewController];
}

-(void) setShareSocialDelegate:(id<TSShareDelegate>)delegate{
    shareSocialDelegate = delegate;
}


- (void) shareWithService:(NSString*) service onViewController:(UIViewController*) viewController{
    NSString* slType = nil;
    if ([TSSocialShareFacebook isEqualToString:service])
        slType = SLServiceTypeFacebook;
    else if ([TSSocialShareTwitter isEqualToString:service])
        slType = SLServiceTypeTwitter;
    if (slType){
        if ([SLComposeViewController isAvailableForServiceType:slType])
        {
            SLComposeViewController *shareSheet = [SLComposeViewController composeViewControllerForServiceType:slType];
            NSString* msg = nil;
            if ([shareSocialDelegate respondsToSelector:@selector(shareSocialMessageForService:)])
                msg = [shareSocialDelegate shareSocialMessageForService:service];
            UIImage* img = [TSUtils takeScreenshotOnView:viewController.view];
            UIImageWriteToSavedPhotosAlbum(img, nil, nil, nil);
            [shareSheet setInitialText:msg];
            [shareSheet addImage:img];
            [shareSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
                if ([shareSocialDelegate respondsToSelector:@selector(onShareViewDidDisappearWithService:shared:)])
                    [shareSocialDelegate onShareViewDidDisappearWithService:service shared:result == SLComposeViewControllerResultDone];
            }];
            
            if ([shareSocialDelegate respondsToSelector:@selector(onShareViewWillAppearWithService:)])
                [shareSocialDelegate onShareViewWillAppearWithService:service];
            [viewController presentViewController:shareSheet animated:YES completion:nil];
        }
        else
            [self requestSocialAccountSetup:service];
    }
}


-(void) requestSocialAccountSetup:(NSString*) social{
    float ios = SYSTEM_VERSION;
    BOOL autoOpenSettings = (ios >= 8.0);
    NSString* msg = [NSString stringWithFormat:@"%@ account was not setup", social];
    msg = [NSString stringWithFormat:@"%@. %@", msg, [NSString stringWithFormat:@"Please, go to Settings->%@ and login into your social account.", social]];
    NSString* accept = (autoOpenSettings ? @"Open Settings": @"Got it!");
    [TSNotifier alertWithTitle:[NSString stringWithFormat:@"Share %@", social]
                message:msg
                acceptButton:accept
                acceptBlock:^{
                    if (autoOpenSettings)
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                }
                cancelButton:@"Later"
                cancelBlock:nil];
}

@end
