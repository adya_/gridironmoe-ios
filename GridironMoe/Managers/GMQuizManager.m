//
//  GMQuestionManager.m
//  GridironMoe
//
//  Created by Adya on 8/20/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMQuizManager.h"
#import "GMRequestManager.h"
#import "GMUser.h"
#import "GMUserManager.h"
#import "GMQuizResult.h"
#import "GMQuestion.h"
#import "GMAppDelegate.h"
#import "GMMoeFace.h"

#define APP_DELEGATE ((GMAppDelegate*)[UIApplication sharedApplication].delegate)

#define DB_VERSION @"questionsVersion"

#define POINTS_PER_ANSWER 10
#define QUIZ_TIME 120

@implementation GMQuizManager{
    int version;
    GMQuizResult* result;
    NSTimer* timer;
    NSMutableArray* usedQuestions;
}

@synthesize leadersDrills, userDrills, updateDelegate, gameDelegate, quizTime, timeLeft, pointsPerAnswer;

-(GMQuizResult*) quizResult{
    return result;
}

-(BOOL) isRunning{
    return timeLeft != 0;
}

+ (instancetype) sharedManager{
    static GMQuizManager* manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{manager = [[self alloc] init];});
    return manager;
}

-(id) init{
    self = [super init];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:DB_VERSION])
        version = [[defaults objectForKey:DB_VERSION] intValue];
    else
        version = -1;
    [self restoreDefaults];
    return self;
}

-(void) restoreDefaults{
    quizTime = QUIZ_TIME;
    pointsPerAnswer = POINTS_PER_ANSWER;
}

-(void) updateDrillsAvatars{
    [self updateAvatarsInDrills:userDrills];
    [self updateAvatarsInDrills:leadersDrills];
}

-(void) updateAvatarsInDrills:(NSArray*) drills{
    for (GMQuizResult* res in drills) {
        if ([res.username isEqualToString:[GMUserManager sharedManager].user.username])
            res.userAvatar = [GMUserManager sharedManager].moeFace.avatar;
        else
            res.userAvatar = [GMMoeFace moeFaceAtIndex:[res.username hash]  % [GMMoeFace availableMoeFaces]].avatar;
    }
}

//  Deletes all the stored questions
+ (void)clearAllQuestions {
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"Question" inManagedObjectContext:APP_DELEGATE.managedObjectContext]];
    [request setIncludesPropertyValues:NO];
    NSError *error = nil;
    NSArray *questions = [APP_DELEGATE.managedObjectContext executeFetchRequest:request error:&error];
    for(NSManagedObject *question in questions) {
        [APP_DELEGATE.managedObjectContext deleteObject:question];
    }
    NSError *saveError = nil;
    [APP_DELEGATE.managedObjectContext save:&saveError];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:nil forKey:DB_VERSION];
    [defaults synchronize];
}

-(void) performUpdateQuestions{
    [[GMRequestManager sharedManager] requestGetQuestionsVersionWithResponseCallback:^(BOOL success, NSObject *object, TSError *error) {
        if (success){
            int vers = [(NSNumber*)object intValue];
            if (vers > version){
                [[GMRequestManager sharedManager] requestGetQuestionsWithResponseCallback:^(BOOL success, TSError *error) {
                    if (success){
                        version = vers;
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        [defaults setValue:@(version) forKey:DB_VERSION];
                        [defaults synchronize];
                    }
                    if ([updateDelegate respondsToSelector:@selector(onQuestionsUpdatedResult:orError:)])
                        [updateDelegate onQuestionsUpdatedResult:success orError:error];
                }];
                return; // request sent awaiting response
            }
            else{
                
            }
        } // otherwise notify delegate that questions is up to date
        if ([updateDelegate respondsToSelector:@selector(onQuestionsUpdatedResult:orError:)])
                [updateDelegate onQuestionsUpdatedResult:NO orError:nil];
    }];
}

-(void) performUpdateQuizUserDrills{
    GMUser* user = [GMUserManager sharedManager].user;
    [[GMRequestManager sharedManager] requestUserDrillsWithUsername:user.username andPassword:user.password withResponseCallback:^(BOOL success, NSArray *array, TSError *error) {
        if (success){
            userDrills = [array sortedArrayUsingComparator:^NSComparisonResult(GMQuizResult* obj1, GMQuizResult* obj2) {
                int dif = obj1.score - obj2.score;
                if (dif > 0) return NSOrderedAscending;
                else if (dif < 0) return NSOrderedDescending;
                else return NSOrderedSame;
            }];
            [userDrills enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                GMQuizResult* drill = obj;
                drill.username = user.username;
                drill.userAvatar = [GMUserManager sharedManager].moeFace.avatar;
            }];
        }
        if ([updateDelegate respondsToSelector:@selector(onQuizUserDrillsUpdatedResult:withUserDrills:orError:)])
            [updateDelegate onQuizUserDrillsUpdatedResult:success withUserDrills:userDrills orError:error];
    }];

}

-(void) performUpdateQuizLeadersDrills{
    GMUser* user = [GMUserManager sharedManager].user;
    [[GMRequestManager sharedManager] requestLeadersDrillsWithUsername:user.username andPassword:user.password withResponseCallback:^(BOOL success, NSArray *array, TSError *error) {
       if (success)
            leadersDrills = [array sortedArrayUsingComparator:^NSComparisonResult(GMQuizResult* obj1, GMQuizResult* obj2) {
               int dif = obj1.score - obj2.score;
               if (dif > 0) return NSOrderedAscending;
               else if (dif < 0) return NSOrderedDescending;
               else return NSOrderedSame;
           }];
            [leadersDrills enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            GMQuizResult* drill = obj;
#warning TEMP AVATAR FOR SCOARBOARDS
            drill.userAvatar = [GMMoeFace moeFaceAtIndex:[drill.username hash]  % [GMMoeFace availableMoeFaces]].avatar;
        }];
        if ([updateDelegate respondsToSelector:@selector(onQuizLeadersDrillsUpdatedResult:withLeadersDrills:orError:)])
            [updateDelegate onQuizLeadersDrillsUpdatedResult:success withLeadersDrills:leadersDrills orError:error];
    }];
}

-(void) performSendUserQuizResults:(GMQuizResult *)quizResult{
    GMUser* user = [GMUserManager sharedManager].user;
    [[GMRequestManager sharedManager] requestSendUserQuizResultsWithScore:quizResult.score rightAnswers:quizResult.right of:quizResult.total withUsername:user.username andPassword:user.password withResponseCallback:^(BOOL success, TSError *error) {
        if ([updateDelegate respondsToSelector:@selector(onUserQuizResultSentResult:orError:)])
            [updateDelegate onUserQuizResultSentResult:success orError:error];

    }];
}

-(void) timerTick:(NSTimer*) sender{
    --timeLeft;
    if (timeLeft > 0){
        if ([gameDelegate respondsToSelector:@selector(onQuizTimerTick:)])
            [gameDelegate onQuizTimerTick:timeLeft];
    }
    else{
        [self stopQuiz];
    }
}

-(void) startQuiz{
    if (timeLeft == 0){
        timeLeft = quizTime;
        result = [GMQuizResult new];
        usedQuestions = [NSMutableArray new];
        if ([gameDelegate respondsToSelector:@selector(onQuizStarted)])
            [gameDelegate onQuizStarted];
    }
    else{
        if ([gameDelegate respondsToSelector:@selector(onQuizStateChnanged:tileLeft:)])
            [gameDelegate onQuizStateChnanged:YES tileLeft:timeLeft];
    }
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
}

-(void) pauseQuiz{
    [timer invalidate];
    if ([gameDelegate respondsToSelector:@selector(onQuizStateChnanged:tileLeft:)])
        [gameDelegate onQuizStateChnanged:NO tileLeft:timeLeft];
}

-(void) stopQuiz{
    [timer invalidate];
    if (timeLeft > 0){
        timeLeft = 0;
        if ([gameDelegate respondsToSelector:@selector(onQuizStateChnanged:tileLeft:)])
            [gameDelegate onQuizStateChnanged:NO tileLeft:0];
    }
    else
        if ([gameDelegate respondsToSelector:@selector(onQuizEnded:)])
            [gameDelegate onQuizEnded:result];
}

-(BOOL) answerQuestion:(GMQuestion *)question withChoice:(NSString*)choice{
    BOOL success = NO;
    if ([question.answer isEqualToString:choice]){
        ++result.right;
        result.score += pointsPerAnswer;
        success = YES;
    }
    ++result.total;
    return success;
}

-(GMQuestion*) rndQuestion{
    int min = [[GMQuestion minId] intValue];
    int total = [GMQuestion count];
    int rnd = (arc4random() % total) + min + 1;
    return [GMQuestion questionWithQuestionId:@(rnd)];
}

-(GMQuestion*) nextQuestion{
    GMQuestion* q = [self rndQuestion];
    //q = [GMQuestion questionWithQuestionId:@(6588)];
    if (usedQuestions.count == [GMQuestion count])
        [usedQuestions removeAllObjects];
    
    while (!q || [usedQuestions containsObject:q.questionId])
        q = [self rndQuestion];
    
    [usedQuestions addObject:q.questionId];
    return q;
}

@end