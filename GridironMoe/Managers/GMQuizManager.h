//
//  GMQuestionManager.h
//  GridironMoe
//
//  Created by Adya on 8/20/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GMQuizResult;
@class GMQuestion;
@class TSError;

@protocol GMQuizDelegate <NSObject>

@optional -(void) onQuestionsUpdatedResult:(BOOL)success  orError:(TSError*) error;
@optional -(void) onQuizLeadersDrillsUpdatedResult:(BOOL) success withLeadersDrills:(NSArray*) drills orError:(TSError*)error;
@optional -(void) onQuizUserDrillsUpdatedResult:(BOOL) success withUserDrills:(NSArray*) drills orError:(TSError*)error;
@optional -(void) onUserQuizResultSentResult:(BOOL) success orError:(TSError*)error;

@end

@protocol GMQuizGameDelegate <NSObject>

@optional -(void) onQuizStateChnanged:(BOOL) isRunning tileLeft:(int) time;
-(void) onQuizStarted;
-(void) onQuizEnded:(GMQuizResult*) result;
-(void) onQuizTimerTick:(int) timeLeft;

@end

@interface GMQuizManager : NSObject

+ (instancetype) sharedManager;

@property NSArray* leadersDrills;
@property NSArray* userDrills;

@property (readonly) GMQuizResult* quizResult;
@property (readonly) BOOL isRunning;
@property (readonly) int timeLeft;

@property int quizTime; // seconds
@property int pointsPerAnswer;

-(void) restoreDefaults;

-(void) updateDrillsAvatars;

@property id<GMQuizDelegate> updateDelegate;
@property id<GMQuizGameDelegate> gameDelegate;

-(void) performUpdateQuestions; // check version before update
-(void) performUpdateQuizLeadersDrills;
-(void) performUpdateQuizUserDrills;

-(void) performSendUserQuizResults:(GMQuizResult*)quizResult;


-(void) startQuiz;
-(void) pauseQuiz;
-(void) stopQuiz;

-(BOOL) answerQuestion:(GMQuestion*) question withChoice:(NSString*) choice;

-(GMQuestion*) nextQuestion;

@end
