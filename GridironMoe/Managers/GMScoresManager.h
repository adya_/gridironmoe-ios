//
//  GMScoresManager.h
//  GridironMoe
//
//  Created by Adya on 8/19/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GMUser;
@class TSError;

@protocol GMScoresDelegate <NSObject>

@optional -(void) onUserScoreUpdated:(BOOL) success withUser:(GMUser*) user orError:(TSError*) error;

@optional -(void) onLeaderboardScoresUpdated:(BOOL) success withLeaderboard:(NSArray*) leaderboard orError:(TSError*) error;

@end

@interface GMScoresManager : NSObject

+ (instancetype) sharedManager;

@property id<GMScoresDelegate> delegate;
@property (readonly) GMUser* user;
@property (readonly) NSArray* leaderboard;

-(void) performUpdateUserScores;
-(void) performUpdateLeaderboardScores;
-(NSString*) avatarForUser:(GMUser*)user;

@end
