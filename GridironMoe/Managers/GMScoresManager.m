//
//  GMScoresManager.m
//  GridironMoe
//
//  Created by Adya on 8/19/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMScoresManager.h"
#import "GMRequestManager.h"
#import "GMUserManager.h"
#import "GMUser.h"
#import "GMScore.h"
#import "GMMoeFace.h"

@implementation GMScoresManager


@synthesize delegate;
@synthesize leaderboard;

+ (instancetype) sharedManager{
    static GMScoresManager* manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{manager = [[self alloc] init];});
    return manager;
}

-(GMUser*) user{
    return [GMUserManager sharedManager].user;
}

-(void) performUpdateUserScores{
    GMUser* user = [self user];
    [[GMRequestManager sharedManager] requestScoresWithUsername:user.username andPassword:user.password withResponseCallback:^(BOOL success, NSObject *object, TSError *error) {
        if (success){
            GMUser* updatedUser = (GMUser*) object;
            user.total = updatedUser.total;
            user.defence = updatedUser.defence;
            user.offence = updatedUser.offence;
        }
        if ([delegate respondsToSelector:@selector(onUserScoreUpdated:withUser:orError:)])
            [delegate onUserScoreUpdated:success withUser:user orError:error];
    }];
}

-(void) performUpdateLeaderboardScores{
    GMUser* user = [self user];
    [[GMRequestManager sharedManager] requestLeaderboardScoresWithUsername:user.username andPassword:user.password withResponseCallback:^(BOOL success, NSArray *array, TSError *error) {
        if (success)
            leaderboard = [array sortedArrayUsingComparator:^NSComparisonResult(GMUser* obj1, GMUser* obj2) {
                int dif = obj1.total.score - obj2.total.score;
                if (dif > 0) return NSOrderedAscending;
                else if (dif < 0) return NSOrderedDescending;
                else return NSOrderedSame;
            }];
        if ([delegate respondsToSelector:@selector(onLeaderboardScoresUpdated:withLeaderboard:orError:)])
            [delegate onLeaderboardScoresUpdated:success withLeaderboard:leaderboard orError:error];
    }];
}

-(NSString*) avatarForUser:(GMUser*)user{
    if ([user.username isEqualToString:[GMUserManager sharedManager].user.username])
        return [GMUserManager sharedManager].moeFace.avatar;
    else
        return [GMMoeFace moeFaceAtIndex:[user.username hash]  % [GMMoeFace availableMoeFaces]].avatar;
}
@end
