//
//  GMLoginManager.h
//  GridironMoe
//
//  Created by Adya on 8/18/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

//
//  CUUserManager.h
//  CelcoinUsuario
//
//  Created by Adya on 7/9/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GMUser;
@class TSError;

@protocol GMLoginDelegate <NSObject>
    @optional -(void) onLoginResult:(BOOL) success withUser:(GMUser*) user orError:(TSError*) error;
@optional -(void) onSocialLoginResult:(BOOL) success withUser:(GMUser*)user isNewUser:(BOOL) isNew orError:(TSError*) error;
    @optional -(void) onLogoutResult:(BOOL) success orError:(TSError*) error;
    @optional -(void) onRegistrationResult:(BOOL) success orError:(TSError*) error;
    @optional -(void) onForgotPasswordResult:(BOOL) success orError:(TSError*)error;
@end

@protocol GMUserProfileDelegate <NSObject>

@optional -(void) onUserProfileUpdated:(BOOL) success withUser:(GMUser*)user orError:(TSError*) error;


    @optional -(void) onFansInStadiumsUpdated:(BOOL) success withFans:(NSDictionary*) fans orError:(TSError*)error;
    @optional -(void) onHomeTeamUpdated:(BOOL)success orError:(TSError*)error;


@end


// LoginManager manages login process

// 1. Set loginDelegate to handle auth process callbacks
// 2. Check if manager has some stored token.
// 2.1 If it has then call performLoginWithSavedUser to attempt to restore previous session if it wasn't ended with logout.
// 2.2 If it has not or prev step failed then call performLoginWihtUsername:andPassowrd: to perform login.

@class GMTeam;
@class GMMoeFace;

@interface GMUserManager : NSObject

@property id<GMLoginDelegate> loginDelegate;
@property id<GMUserProfileDelegate> profileDelegate;

@property (readonly) GMUser* user;
@property UIImage* moeFaceImage;
@property GMMoeFace* moeFace;

@property (readonly) int loggedInUsers;
@property (readonly) NSDictionary* fansInStadiums;

+ (instancetype) sharedManager;

-(BOOL) hasStoredUser;
-(BOOL) isLoggedIn;
-(BOOL) isValidPassword:(NSString*)password;
-(BOOL) isValidUsername:(NSString*)username;

-(void) logout;

-(void) loadMoe;
-(void) selectMoe:(GMMoeFace*)moeName;
-(void) selectMoeAtIndex:(NSInteger)moeNameIndex;
@end


@interface GMUserManager (LoginProcess)

-(void) performLoginWithUsername:(NSString*) username andPassword:(NSString*) password;
-(void) performLoginWithSavedUser;
-(void) performLogout;

@end

@interface GMUserManager (RegistrationProcess)

-(void) performRegistrationWithUsername:(NSString *)username password:(NSString *)password andEmail:(NSString*)email shouldLoginOnSuccess:(BOOL)shouldLogin;
@end

@interface GMUserManager (UserProfile)

-(void) performChangeUsername:(NSString*) newUsername;
-(void) performChangePassword:(NSString*) newPassword;
-(void) performChangeEmail:(NSString*) newEmail;

-(void) performForgotPasswordForUsername:(NSString*) username andEmail:(NSString*) email;

-(void) performUpdateFansInStadiums;
-(void) performUpdateHomeTeam:(GMTeam *)team;

@end

@interface GMUserManager (Facebook)

-(void) performLoginWithFacebookFromViewController:(UIViewController*)viewController;
@end
