//
//  GMGameManager.h
//  GridironMoe
//
//  Created by Adya on 8/19/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GMTeam;
@class GMGame;
@class TSError;
@class GMGameLive;
@class GMStrategy;
@class GMVoteResult;

#define GM_ERROR_GAME_NOT_LIVE 100
#define GM_ERROR_GAME_OVER 101
#define GM_ERROR_GAME_NOT_STARTED 102

@protocol GMGameDelegate <NSObject>

@optional -(void) onActiveGamesUpdated:(BOOL) success withGames:(NSArray*)games orError:(TSError*)error;
@optional -(void) onScheduledGamesUpdated:(BOOL) success withGames:(NSArray*)games orError:(TSError*)error;

@optional -(void) onGamesUpdated:(BOOL) success withScheduledGames:(NSArray*)scheduled andActiveGames:(NSArray*)active orError:(TSError*) error;

@optional -(void) onLiveFeedUpdated:(BOOL) success withLiveGame:(GMGameLive*) game orError:(TSError*) error;

@optional -(void) onVoteSent:(BOOL) success withResults:(GMVoteResult*)result orError:(TSError*)error;
@end

@interface GMGameManager : NSObject

+ (instancetype) sharedManager;


@property (readonly) NSArray* activeGames;
@property (readonly) NSArray* scheduledGames;
@property (readonly) GMGameLive* liveGame;
@property id<GMGameDelegate> delegate;

@property CGFloat liveUpdateFrequency;
@property CGFloat gamesUpdateFrequency;

@property BOOL autoUpdateGames;

@property BOOL testing;

-(void) performUpdateGames;
-(void) performUpdateActiveGames;
-(void) performUpdateSchedule;
-(void) performUpdateLiveFeedForGame:(GMGame*) game;

-(void) performSendVoteWithOffence:(GMStrategy*) offence andDefense:(GMStrategy*) defense forTeam:(GMTeam*) team;

-(void) setGame:(GMGame*) game live:(BOOL) isLive;
@end
