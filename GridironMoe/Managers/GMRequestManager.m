//
//  GMRequestManager.m
//  GridironMoe
//
//  Created by Adya on 8/18/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMRequestManager.h"
#import "AFHTTPRequestOperation.h"
#import "AFHTTPRequestOperationManager.h"
#import "GMUser.h"
#import "GMScore.h"
#import "TSUtils.h"
#import "GMTeam.h"
#import "GMQuestion.h"
#import "GMQuizResult.h"
#import "GMGameSchedule.h"
#import "GMGameActive.h"
#import "GMGameLive.h"
#import "GMVoteResult.h"
#import "TSError.h"


// POST
#define REQUEST_LOGIN @"login"

#define REQUEST_LOGIN_SOCIAL @"loginSocial"

// POST
#define REQUEST_LOGOUT @"logout"
// POST
#define REQUEST_REGISTER @"register"

#define REQUEST_SET_USERNAME @"changeUsername"

#define REQUEST_SET_PASSWORD @"changePassword"

#define REQUEST_FORGOT_PASSWORD @"sendPasswordViaEmail"

#define REQUEST_SET_EMAIL @"changeEmail"

#define REQUEST_SET_AVATAR @"changeAvatar"

// GET
#define REQUEST_GET_ACTIVE_GAMES @"getActiveGames"
// GET
#define REQUEST_GET_LIVE_FEED @"getLiveFeed"
// POST
#define REQUEST_LEADER_BOARD_SCORES @"leaderBoardScoresV2"
// POST
#define REQUEST_SCORES @"scores"
// POST
#define REQUEST_SEND_VOTE @"sendVoteV2"
// GET
#define REQUEST_GET_SCHEDULE @"getSchedule"
// POST
#define REQUEST_SET_HOME_TEAM @"setHomeTeam"
// GET
#define REQUEST_GET_COUNT_IN_STADIUM @"getCountInStadium"
// GET
#define REQUEST_GET_TEAM_MAP @"getTeamMap"

#define REQUEST_GET_QUIZ_QUESTIONS @"getAllRecords"

#define REQUEST_GET_QUIZ_QUESTIONS_VERSION @"getVersionNumber"

#define REQUEST_GET_QUIZ_USER_DRILLS @"getUserQuizeScore"

#define REQUEST_GET_QUIZ_LEADERS_DRILLS @"getFirstTopTenScorer"

#define REQUEST_SEND_USER_QUIZ_SCORE @"updateUserQuizeScore"


#define SERVER_URL @"http://104.130.165.68:9000"
#define ERROR_CODE_PARAM @"code"
#define ERROR_MSG_PARAM @"message"
#define RESPONSE_PARAM @"data"

#define SOCIAL_TYPE_FACEBOOK @"facebook"


@implementation GMRequestManager (Logging)

-(void) POST:(NSString *)requestUrl withBody:(NSDictionary *)body andHeaders:(NSDictionary *)headers successBlock:(void (^)(AFHTTPRequestOperation *, id))success failureBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failure{
    NSLog(@"Request: %@/%@", [self getServerUrl], requestUrl);
    NSLog(@"Body:\n%@", body);
    [super POST:requestUrl withBody:body andHeaders:headers
   successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
       NSLog(@"Response Success:\n%@", responseObject);
       if (success)
           success(operation, responseObject);
   }
   failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
       NSLog(@"Response Failed:\n%@", error.userInfo);
       if (failure)
           failure(operation, error);
   }];
}

-(void) GET:(NSString *)requestUrl withParameters:(NSDictionary *)params andHeaders:(NSDictionary *)headers successBlock:(void (^)(AFHTTPRequestOperation *, id))success failureBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failure{
    NSLog(@"Request: %@/%@", [self getServerUrl], requestUrl);
    NSLog(@"Params:\n%@", params);
    [super GET:requestUrl withParameters:params andHeaders:headers successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
       NSLog(@"Response Success:\n%@", responseObject);
       if (success)
           success(operation, responseObject);
   }
   failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
       NSLog(@"Response Failed:\n%@", error.userInfo);
       if (failure)
           failure(operation, error);
   }];
}

@end


@implementation GMRequestManager

-(NSString*) getServerUrl{
    return SERVER_URL;
}

-(NSString*) getErrorMessageParamName{
    return ERROR_MSG_PARAM;
}

-(NSString*) getErrorCodeParamName{
    return ERROR_CODE_PARAM;
}

-(NSString*) getResponseParamName{
    return RESPONSE_PARAM;
}

-(BOOL) isSuccessfulResponse:(NSDictionary *)response{
    int code = [[response objectForKey:[self getErrorCodeParamName]] intValue];
    return response && (![response objectForKey:[self getErrorCodeParamName]] || (code >= 200 && code < 300));
}

-(void) prepareManager{
    [self setAcceptTypes: @[@"application/json"]];
    [self setContentTypes: @[@"application/json"]];
}
////////////////

-(void) requestLoginWithUsername:(NSString*) username andPassword:(NSString*) password withResponseCallback:(ArrayResponseCallback) callback
{
    NSDictionary* body = @{@"username":username,
                           @"password":password};
    [self POST:REQUEST_LOGIN withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([[responseObject objectForKey:[self getErrorCodeParamName]] intValue] == 200){
            NSDictionary* data = [self getDataFromResponseObject:responseObject];
            NSDictionary* userObj = [data objectForKey:@"user"];
            GMUser* user = [GMUser new];
            user.total = [[GMScore alloc] initWithScore:[nonNull([userObj objectForKey:@"score"]) intValue] andRank:GM_SCORE_UNRANKED];
            user.email = nonNull([userObj objectForKey:@"userEmailAddress"]);
            NSString* team = nonNull([userObj objectForKey:@"team"]);
            if (team)
                user.team = [GMTeam teamWithTeamId:team];
            callback(YES, @[user, nonNull([data objectForKey:@"noOfLoggedInUsers"])], nil);
        } else
            callback(NO, nil, [self parseError:responseObject]);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}

-(void) requestLoginWithToken:(NSString*) token forSocialNetwork:(NSString*)social withResponseCallback:(ArrayResponseCallback) callback{
    NSDictionary* body = @{@"type":social,
                           @"token":token};
    [self POST:REQUEST_LOGIN_SOCIAL withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([[responseObject objectForKey:[self getErrorCodeParamName]] intValue] == 200){
            NSDictionary* data = [self getDataFromResponseObject:responseObject];
            NSDictionary* userObj = [data objectForKey:@"user"];
            GMUser* user = [[GMUser alloc] initWithUsername:nonNull([userObj objectForKey:@"username"]) andPassword:nonNull([userObj objectForKey:@"password"])];
            user.total = [[GMScore alloc] initWithScore:[nonNull([userObj objectForKey:@"score"]) intValue] andRank:GM_SCORE_UNRANKED];
            user.email = nonNull([userObj objectForKey:@"userEmailAddress"]);
            NSString* team = nonNull([userObj objectForKey:@"team"]);
            if (team)
                user.team = [GMTeam teamWithTeamId:team];
            callback(YES, @[user, nonNull([data objectForKey:@"noOfLoggedInUsers"])], nil);
        } else
            callback(NO, nil, [self parseError:responseObject]);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];

}


-(void) requestRegisterWithUsername:(NSString *)username password:(NSString *)password andEmail:(NSString *)email withResponseCallback:(OperationResponseCallback)callback{
    NSDictionary* body = @{@"username":username,
                           @"password":password,
                           @"email" : email};
    [self POST:REQUEST_REGISTER withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([[responseObject objectForKey:[self getErrorCodeParamName]] intValue] == 200)
            callback(YES, nil);
        else
            callback(NO, [self parseError:responseObject]);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, [self parseNSError:error]);
    }];
}

-(void) requestLogoutWithUsername:(NSString *)username andPassword:(NSString *)password withResponseCallback:(OperationResponseCallback)callback{
    NSDictionary* body = @{@"username":username,
                           @"password":password};
    [self POST:REQUEST_LOGOUT withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject])
            callback(YES, nil);
        else
            callback(NO, [self parseError:responseObject]);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, [self parseNSError:error]);
    }];
}

-(void) requestSetUsername:(NSString*)newUsername forUserWithUsername:(NSString*)username andPassword:(NSString*)password withResponseCallback:(OperationResponseCallback)callback{
    NSDictionary* body = @{@"username":username,
                           @"password":password,
                           @"newUsername":newUsername};
    [self POST:REQUEST_SET_USERNAME withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, nil);
        } else
            callback(NO, [self parseError:responseObject]);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, [self parseNSError:error]);
    }];
}

-(void) requestSetPassword:(NSString*)newPassword forUserWithUsername:(NSString*)username andPassword:(NSString*)password withResponseCallback:(OperationResponseCallback)callback{
    NSDictionary* body = @{@"username":username,
                           @"password":password,
                           @"newPassword":newPassword};
    [self POST:REQUEST_SET_PASSWORD withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, nil);
        } else
            callback(NO, [self parseError:responseObject]);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, [self parseNSError:error]);
    }];
}

-(void) requestForgotPasswordForUserWithUsername:(NSString *)username andEmail:(NSString *)email withResponseCallback:(OperationResponseCallback)callback{
    NSMutableDictionary* body = [NSMutableDictionary dictionaryWithDictionary: @{@"email":email}];
    if (username)
        [body setObject:username forKey:@"username"];
    
    [self POST:REQUEST_FORGOT_PASSWORD withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, nil);
        } else
            callback(NO, [self parseError:responseObject]);
    } failureBlock:^(AFHTTPRequestOperation * operation, NSError * error) {
        callback(NO, [self parseNSError:error]);
    }];
}

-(void) requestSetEmail:(NSString*)newEmail forUserWithUsername:(NSString*)username andPassword:(NSString*)password withResponseCallback:(OperationResponseCallback)callback{
    NSDictionary* body = @{@"username":username,
                           @"password":password,
                           @"newEmail":newEmail};
    [self POST:REQUEST_SET_EMAIL withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([[responseObject objectForKey:[self getErrorCodeParamName]] intValue] == 218){
            callback(YES, nil);
        } else
            callback(NO, [self parseError:responseObject]);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, [self parseNSError:error]);
    }];
}

-(void) requestSetAvatar:(UIImage*)newAvatar forUserWithUsername:(NSString*)username andPassword:(NSString*)password withResponseCallback:(ObjectResponseCallback)callback{
    #warning CHANGE AVATAR REQUEST
}

-(void) requestScoresWithUsername:(NSString *)username andPassword:(NSString *)password withResponseCallback:(ObjectResponseCallback)callback{
    NSDictionary* body = @{@"username":username,
                           @"password":password};
    [self POST:REQUEST_SCORES withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            GMUser* user = [GMUser new];
            NSDictionary* data = [responseObject objectForKey:[self getResponseParamName]];
            NSString* total = [data objectForKey:@"total"];
            NSString* def = [data objectForKey:@"defence"];
            NSString* off = [data objectForKey:@"offence"];
            user.total = [GMScore scoreFromString:total];
            user.defence = [GMScore scoreFromString:def];
            user.offence = [GMScore scoreFromString:off];
            callback(YES, user, nil);
        }
        else
            callback(NO, nil, [self parseError:responseObject]);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}

-(void) requestLeaderboardScoresWithUsername:(NSString *)username andPassword:(NSString *)password withResponseCallback:(ArrayResponseCallback)callback{
    [self GET:REQUEST_LEADER_BOARD_SCORES withParameters:nil andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            NSArray* array = [responseObject objectForKey:@"user_score"];
            if (array && array.count > 0){
                NSMutableArray* scoreboard = [NSMutableArray new];
                for (NSDictionary* userObj in array) {
                    GMUser* user = [[GMUser alloc] initWithUsername:nonNull([userObj objectForKey:@"user_name"]) andPassword:nil];
                    user.total = [[GMScore alloc] initWithScore:[[userObj objectForKey:@"total_point"] intValue] andRank:GM_SCORE_UNRANKED];
                    [scoreboard addObject:user];
                }
                callback(YES, [NSArray arrayWithArray:scoreboard], nil);
            }
            else
                callback(YES, [NSArray new], nil);
        }
        else
            callback(NO, nil, [self parseError:responseObject]);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}

-(void) requestSetHomeTeam:(NSString *)team withUsername:(NSString *)username andPassword:(NSString *)password withResponseCallback:(OperationResponseCallback)callback{
    NSString* request = [NSString stringWithFormat:@"%@/%@", REQUEST_SET_HOME_TEAM, team];
    NSDictionary* body = @{@"username":username,
                           @"password":password};
    [self POST:request withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject])
            callback(YES, nil);
        else
            callback(NO, [self parseError:responseObject]);

    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, [self parseNSError:error]);
    }];
}

-(void) requestSendVoteForDownWithId:(NSString*)downId forGameWithId:(NSString*) gameId withQuarter:(int) quarter clock:(NSString*)clock seasonYear:(int) year offenseVote:(NSString*) offense andDefense:(NSString*)defense forTeamWithId:(NSString*)teamId withUsername:(NSString*) username andPassword:(NSString*) password withResponseCallback:(ObjectResponseCallback) callback{
    NSDictionary* body = @{@"username":username,
                           @"password":password,
                           @"quarter":@(quarter),
                           @"clock":clock,
                           @"seasonYear":@(year),
                           @"game_id": gameId,
                           @"downId" : downId};
    NSMutableDictionary* bodyWithVotes = [NSMutableDictionary dictionaryWithDictionary:body];
    if (!offense && !defense)
        callback(NO, nil, [TSError errorWithCode:TS_ERROR_INTERNAL Title:nil andDescription:@"No votes specified"]);
    if (offense)
        [bodyWithVotes setValue:offense forKey:@"offense"];
    if (defense)
        [bodyWithVotes setValue:defense forKey:@"defense"];
    
    [self POST:REQUEST_SEND_VOTE withBody:bodyWithVotes andHeaders:nil successBlock:^(AFHTTPRequestOperation * operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject])
            callback(YES, [[GMVoteResult alloc] initWithJSON:[self getDataFromResponseObject:responseObject]], nil);
        else
            callback(NO, nil, [self parseError:responseObject]);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}

-(void) requestGetActiveGamesWithResponseCallback:(ArrayResponseCallback)callback{
    
    [self GET:REQUEST_GET_ACTIVE_GAMES withParameters:nil andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]) {
            NSArray* gamesDic = [responseObject objectForKey:@"games"];
            NSMutableArray* games = [NSMutableArray new];
            for (NSDictionary* dic in gamesDic) {
                [games addObject:[[GMGameActive alloc] initWithJSON:dic]];
            }
            callback(YES, [NSArray arrayWithArray:games], nil);
        }
        else{
            callback(NO, nil, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}


-(void) requestGetLiveFeedForGameWithId:(NSString*)gameId withResponseCallback:(ObjectResponseCallback)callback{
    NSString* request = [NSString stringWithFormat:@"%@/%@", REQUEST_GET_LIVE_FEED, gameId];
    [self GET:request withParameters:nil andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]) {
            callback(YES, [[GMGameLive alloc] initWithJSON:responseObject] , nil);
        }
        else{
            callback(NO, nil, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}

-(void) requestGetCountInStadiumWithResponseCallback:(ObjectResponseCallback)callback{
    [self GET:REQUEST_GET_COUNT_IN_STADIUM withParameters:nil andHeaders:nil successBlock:^(AFHTTPRequestOperation * operation, id responseObject) {
        
        NSMutableDictionary* fans = [NSMutableDictionary new];
        [(NSDictionary*)responseObject enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            NSString* teamId = nonNull(key);
            if ([teamId rangeOfString:@"null"].location != NSNotFound)
                teamId = nil;
            if (teamId){
                [fans setObject:obj forKey:teamId];
            }
        }];
        if ([self isSuccessfulResponse:responseObject])
            callback(YES, [NSDictionary dictionaryWithDictionary:fans], nil);
        else
            callback(NO, nil, [self parseError:responseObject]);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}

-(void) requestGetTeamMapWithResponseCallback:(ObjectResponseCallback)callback{
    [self GET:REQUEST_GET_TEAM_MAP withParameters:nil andHeaders:nil successBlock:^(AFHTTPRequestOperation * operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            NSArray* array = [self getDataFromResponseObject:responseObject];
            NSMutableDictionary* teams = [NSMutableDictionary new];
            for (NSDictionary* teamObj in array) {
                GMTeam* team = [[GMTeam alloc] initWithJSON:teamObj];
                [teams setObject:team forKey:team.teamId];
            }
            callback(YES, teams, nil);
        }
        else
            callback(NO, nil, [self parseError:responseObject]);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}

-(void) requestGetScheduleWithResponseCallback:(ArrayResponseCallback)callback{
    [self GET:REQUEST_GET_SCHEDULE withParameters:nil andHeaders:nil successBlock:^(AFHTTPRequestOperation * operation, id responseObject) {
        
        if ([self isSuccessfulResponse:responseObject]){
            NSMutableArray* schedule = [NSMutableArray new];
            NSArray* scheduleDic = [self getDataFromResponseObject:responseObject];
            for (NSDictionary* dic in scheduleDic) {
                [schedule addObject:[[GMGameSchedule alloc] initWithJSON:dic]];
            }
            callback(YES, [NSArray arrayWithArray:schedule], nil);
        }
        else
            callback(NO, nil, [self parseError:responseObject]);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}


@end

@implementation GMRequestManager (Quiz)

-(void) requestGetQuestionsWithResponseCallback:(OperationResponseCallback) callback
{
    [self GET:REQUEST_GET_QUIZ_QUESTIONS withParameters:nil andHeaders:nil successBlock:^(AFHTTPRequestOperation * operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSArray class]]){
            NSArray* array = responseObject;
            int i = 0;
            NSNumber* min;
            NSNumber* max;
            for (NSDictionary* dic in array) {
                GMQuestion* q = [GMQuestion createQuestionFromDictionary:dic];
                if (i == 0)
                    min = q.questionId;
                else if (i == array.count - 1)
                    max = q.questionId;
                ++i;
            }
            [GMQuestion setIdRange:min max:max];
            callback(YES, nil);
        }
        else
            callback(NO, [self parseError:responseObject]);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, [self parseNSError:error]);
    }];
}

-(void) requestGetQuestionsVersionWithResponseCallback:(ObjectResponseCallback) callback
{
    [self GET:REQUEST_GET_QUIZ_QUESTIONS_VERSION withParameters:nil andHeaders:nil successBlock:^(AFHTTPRequestOperation * operation, id responseObject) {
        if (![responseObject objectForKey:@"code"])
            callback(YES, [responseObject objectForKey:@"id"], nil);
        else
            callback(NO, nil, [self parseError:responseObject]);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}

-(void) requestUserDrillsWithUsername:(NSString*) username andPassword:(NSString*) password withResponseCallback:(ArrayResponseCallback) callback
{
    NSDictionary* body = @{@"username":username,
                           @"password":password};
    [self POST:REQUEST_GET_QUIZ_USER_DRILLS withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation * operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSArray class]]){
            NSMutableArray* drills = [NSMutableArray new];
            NSArray* array = responseObject;
            if (array && array.count > 0){
                for (NSDictionary* dic in [array reverseObjectEnumerator]) {
                    [drills addObject:[[GMQuizResult alloc] initWithJSON:dic]];
                }
                callback(YES, [NSArray arrayWithArray:drills], nil);
            }
            else
                callback(YES, [NSArray new], nil);
        }
        else
            callback(NO, nil, [self parseError:responseObject]);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}

-(void) requestLeadersDrillsWithUsername:(NSString*) username andPassword:(NSString*) password withResponseCallback:(ArrayResponseCallback) callback
{
    NSDictionary* body = @{@"username":username,
                           @"password":password};
    [self POST:REQUEST_GET_QUIZ_LEADERS_DRILLS withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation * operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSArray class]]){
            NSMutableArray* drills = [NSMutableArray new];
            NSArray* array = responseObject;
            if (array && array.count > 0){
                for (NSDictionary* dic in [array reverseObjectEnumerator]) {
                    [drills addObject:[[GMQuizResult alloc] initWithJSON:dic]];
                }
                callback(YES, [NSArray arrayWithArray:drills], nil);
            }
            else
                callback(YES, [NSArray new], nil);
        }
        else
            callback(NO, nil, [self parseError:responseObject]);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}

-(void) requestSendUserQuizResultsWithScore:(int) score rightAnswers:(int) right of:(int) total withUsername:(NSString*) username andPassword:(NSString*) password withResponseCallback:(OperationResponseCallback) callback
{
    NSDictionary* body = @{@"username":username,
                           @"password":password,
                           @"score" : @(score),
                           @"right" : @(right),
                           @"attempted" : @(total)};
    [self POST:REQUEST_SEND_USER_QUIZ_SCORE withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation * operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject])
            callback(YES, nil);
        else
            callback(NO, [self parseError:responseObject]);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, [self parseNSError:error]);
    }];
}


@end
