//
//  GMRequestManager.h
//  GridironMoe
//
//  Created by Adya on 8/18/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "TSRequestManager.h"

@interface GMRequestManager : TSRequestManager

/// returns array like the following [user, numberOfLoggedInUsers]
-(void) requestLoginWithUsername:(NSString*) username andPassword:(NSString*) password withResponseCallback:(ArrayResponseCallback) callback;

-(void) requestLoginWithToken:(NSString*) token forSocialNetwork:(NSString*)social withResponseCallback:(ArrayResponseCallback) callback;

-(void) requestRegisterWithUsername:(NSString*) username password:(NSString*) password andEmail:(NSString*) email withResponseCallback:(OperationResponseCallback) callback;

-(void) requestLogoutWithUsername:(NSString*) username andPassword:(NSString*) password withResponseCallback:(OperationResponseCallback) callback;

-(void) requestSetUsername:(NSString*)newUsername forUserWithUsername:(NSString*)username andPassword:(NSString*)password withResponseCallback:(OperationResponseCallback)callback;

-(void) requestSetPassword:(NSString*)newPassword forUserWithUsername:(NSString*)username andPassword:(NSString*)password withResponseCallback:(OperationResponseCallback)callback;

-(void) requestForgotPasswordForUserWithUsername:(NSString*) username andEmail:(NSString*) email withResponseCallback:(OperationResponseCallback) callback;

-(void) requestSetEmail:(NSString*)newEmail forUserWithUsername:(NSString*)username andPassword:(NSString*)password withResponseCallback:(OperationResponseCallback)callback;

-(void) requestSetAvatar:(UIImage*)newAvatar forUserWithUsername:(NSString*)username andPassword:(NSString*)password withResponseCallback:(ObjectResponseCallback)callback;

-(void) requestScoresWithUsername:(NSString*) username andPassword:(NSString*) password withResponseCallback:(ObjectResponseCallback) callback;

/// returns array of GMUser objects with filled username, total, and team properties
-(void) requestLeaderboardScoresWithUsername:(NSString*) username andPassword:(NSString*) password withResponseCallback:(ArrayResponseCallback) callback;

-(void) requestSetHomeTeam:(NSString*) team withUsername:(NSString*) username andPassword:(NSString*) password withResponseCallback:(OperationResponseCallback) callback;

-(void) requestSendVoteForDownWithId:(NSString*)downId forGameWithId:(NSString*) gameId withQuarter:(int) quarter clock:(NSString*)clock seasonYear:(int) year offenseVote:(NSString*) offence andDefense:(NSString*)defense forTeamWithId:(NSString*)teamId withUsername:(NSString*) username andPassword:(NSString*) password withResponseCallback:(ObjectResponseCallback) callback;

-(void) requestGetActiveGamesWithResponseCallback:(ArrayResponseCallback) callback;

-(void) requestGetLiveFeedForGameWithId:(NSString*)gameId withResponseCallback:(ObjectResponseCallback)callback;

-(void) requestGetCountInStadiumWithResponseCallback:(ObjectResponseCallback) callback;

-(void) requestGetScheduleWithResponseCallback:(ArrayResponseCallback) callback;

-(void) requestGetTeamMapWithResponseCallback:(ObjectResponseCallback) callback;

@end

@interface GMRequestManager (Quiz)

-(void) requestGetQuestionsWithResponseCallback:(OperationResponseCallback) callback;

-(void) requestGetQuestionsVersionWithResponseCallback:(ObjectResponseCallback) callback;

-(void) requestUserDrillsWithUsername:(NSString*) username andPassword:(NSString*) password withResponseCallback:(ArrayResponseCallback) callback;

-(void) requestLeadersDrillsWithUsername:(NSString*) username andPassword:(NSString*) password withResponseCallback:(ArrayResponseCallback) callback;

-(void) requestSendUserQuizResultsWithScore:(int) score rightAnswers:(int) right of:(int) total withUsername:(NSString*) username andPassword:(NSString*) password withResponseCallback:(OperationResponseCallback) callback;

@end
