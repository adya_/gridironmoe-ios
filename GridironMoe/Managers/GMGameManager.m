//
//  GMGameManager.m
//  GridironMoe
//
//  Created by Adya on 8/19/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMGameManager.h"
#import "GMRequestManager.h"
#import "GMTeam.h"
#import "GMUser.h"
#import "GMUserManager.h"
#import "GMGame.h"
#import "GMGameLive.h"
#import "TSError.h"
#import "TSUtils.h"
#import "GMStrategy.h"
#import "GMVoteResult.h"
#import "GMGameActive.h"
#import "TSNotificationManager.h"
#import "TSNotifier.h"

// number of updates per tick (schedule and active games)
#define UPDATES_NUMBER 2

#define NOTIFICATIONS_INTERVAL_BEFORE_GAME -5
static NSString* const kNotificationGameId = @"kNotificationGameId";

@implementation GMGameManager{
    NSTimer* liveTimer;
    NSTimer* gamesTimer;
    int updatesPending;
    BOOL updatesError;
    BOOL updateAll;
    BOOL firstUpdate;
    GMGame* gameToLive;
}

@synthesize activeGames;
@synthesize scheduledGames;
@synthesize liveGame;
@synthesize delegate;

@synthesize testing;

@synthesize liveUpdateFrequency, gamesUpdateFrequency;

+ (instancetype) sharedManager{
    static GMGameManager* manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{manager = [[self alloc] init];});
    return manager;
}

-(id) init{
    self = [super init];
    self.liveUpdateFrequency = 4;
    self.gamesUpdateFrequency = 10;
    return self;
}

-(void) setAutoUpdateGames:(BOOL)autoUpdateGames{
    if (autoUpdateGames){
        [self performUpdateGames];
        gamesTimer = [NSTimer scheduledTimerWithTimeInterval:self.gamesUpdateFrequency target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:gamesTimer forMode:NSRunLoopCommonModes];
    }
    else{
        [gamesTimer invalidate];
        gamesTimer = nil;
    }
}

-(BOOL) autoUpdateGames{
    return gamesTimer != nil;
}

-(void) performUpdateGames{
    updatesPending = 2;
    updatesError = NO;
    updateAll = YES;
    [self performUpdateActiveGames];
    [self performUpdateSchedule];
}

-(void) performUpdateActiveGames{
    [[GMRequestManager sharedManager] requestGetActiveGamesWithResponseCallback:^(BOOL success, NSArray *array, TSError *error) {
        activeGames = array; // update regardless success flag as we have to store only relevant games
        if (self.testing){
            if (activeGames.count == 0)
                activeGames =  @[[self testGameActive]];
            else
                activeGames = [activeGames arrayByAddingObject:[self testGameActive]];
        }
        
        if ([delegate respondsToSelector:@selector(onActiveGamesUpdated:withGames:orError:)])
            [delegate onActiveGamesUpdated:success withGames:activeGames orError:error];
        if (updateAll)
            [self handleUpdatesWithSuccess:success andError:error];
        

    }];
}

-(void) performUpdateSchedule{
    [[GMRequestManager sharedManager] requestGetScheduleWithResponseCallback:^(BOOL success, NSArray *array, TSError *error) {
        if (success)
            scheduledGames = array; // schedule can be outdated a little
        if ([delegate respondsToSelector:@selector(onScheduledGamesUpdated:withGames:orError:)])
            [delegate onScheduledGamesUpdated:success withGames:scheduledGames orError:error];
        if (updateAll)
            [self handleUpdatesWithSuccess:success andError:error];
    }];
    
}

-(void) handleUpdatesWithSuccess:(BOOL)success andError:(TSError*)error{
    --updatesPending;
    if ((!updatesError && (!success || updatesPending == 0))){
        updateAll = NO;
        if ([TSNotificationManager scheduledNotificationsCount] == 0)
            [self setupNotifications];
        else
            [self invalidateNotifications];
        if([delegate respondsToSelector:@selector(onGamesUpdated:withScheduledGames:andActiveGames:orError:)])
            [delegate onGamesUpdated:success withScheduledGames:scheduledGames andActiveGames:activeGames orError:error];
    }
    if (!success)
        updatesError = YES;
}

-(void) performUpdateLiveFeedForGame:(GMGame *)game{
    if (self.testing && [game isEqual:[self testGameActive]]){
        liveGame = [self testGame];
        if ([delegate respondsToSelector:@selector(onLiveFeedUpdated:withLiveGame:orError:)])
            [delegate onLiveFeedUpdated:YES withLiveGame:liveGame orError:nil];
    }
    else{
        [[GMRequestManager sharedManager] requestGetLiveFeedForGameWithId:game.gameId withResponseCallback:^(BOOL success, NSObject *object, TSError *error) {
            liveGame = (GMGameLive*)object;
            TSError* err = error;
            if (success){
                if (!liveGame){
                    success = NO;
                    err = [TSError errorWithCode:GM_ERROR_GAME_NOT_LIVE Title:@"Game isn't live" andDescription:@"Sorry, current game is not live."];
                }
                else if (liveGame.status != GM_GAME_LIVE){
                    success = NO;
                    [self setGame:nil live:NO]; // if game isn't live anymore then stop timer.
                    if (liveGame.status == GM_GAME_SCHEDULED)
                        err = [TSError errorWithCode:GM_ERROR_GAME_NOT_STARTED Title:@"Game isn't live" andDescription:@"Sorry, current game has not started yet."];
                    else if (liveGame.status == GM_GAME_CLOSED)
                        err = [TSError errorWithCode:GM_ERROR_GAME_OVER Title:@"Game Over" andDescription:@"Sorry, current game is over"];
                    else
                        err = [TSError errorWithCode:GM_ERROR_GAME_NOT_LIVE Title:@"Game isn't live" andDescription:@"Sorry, current game is not live."];
                }
            }
            if ([delegate respondsToSelector:@selector(onLiveFeedUpdated:withLiveGame:orError:)])
                [delegate onLiveFeedUpdated:success withLiveGame:liveGame orError:err];
        }];
    }
}

-(void) performSendVoteWithOffence:(GMStrategy *)offence andDefense:(GMStrategy *)defense forTeam:(GMTeam*) team{
    GMUser* user = [GMUserManager sharedManager].user;
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    [[GMRequestManager sharedManager] requestSendVoteForDownWithId:liveGame.downId forGameWithId:liveGame.gameId withQuarter:liveGame.quarter clock:liveGame.clock seasonYear:components.year offenseVote:offence.name andDefense:defense.name forTeamWithId:team.teamId  withUsername:user.username andPassword:user.password withResponseCallback:^(BOOL success, NSObject* res, TSError *error) {
        if ([self.delegate respondsToSelector:@selector(onVoteSent:withResults:orError:)])
            [self.delegate onVoteSent:success withResults:(GMVoteResult*)res orError:error];
    }];
}

-(void) setupNotifications{
    NSLog(@"Scheduling all %d games:", scheduledGames.count);
    for (GMGameSchedule* game in scheduledGames) {
        [self scheduleNotificationForGame:game];
    }
}

-(void) scheduleNotificationForGame:(GMGameSchedule*) game{
    NSString* msg = [NSString stringWithFormat:@"Game %@ @ %@ will start in a few minutes. Be ready for the fun!", game.awayTeam.teamId, game.homeTeam.teamId];
    NSDateComponents *dateComponents = [NSDateComponents new];
    [dateComponents setMinute:NOTIFICATIONS_INTERVAL_BEFORE_GAME];
    NSDate *date = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:game.date options:0];
    [TSNotificationManager scheduleNotificationWithMessage:msg andUserInfo:@{kNotificationGameId:game.gameId} withHandler:^(UILocalNotification *notification) {
        [TSNotifier notify:notification.alertBody];
        NSLog(@"NOTIFICATION: %@", notification.alertBody);
    } on:date withTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"EST"]];

}

-(void) invalidateNotifications{
    NSArray* games = [activeGames arrayByAddingObjectsFromArray:scheduledGames];
    NSMutableArray* newGames = [NSMutableArray arrayWithArray:scheduledGames]; // not scheduled games
    [TSNotificationManager invalidateStoredNotificationsWithBlock:^BOOL(UILocalNotification *notification) {
        NSString* gameId = [notification.userInfo objectForKey:kNotificationGameId];
        if (!nonEmpty(gameId)) return NO;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"gameId == %@", gameId];
        NSArray *filteredArray = [games filteredArrayUsingPredicate:predicate];
        GMGame* game = (filteredArray.count > 0 ? filteredArray.firstObject : nil);
        if (game && game.status == GM_GAME_SCHEDULED)
            [newGames removeObject:game];
        return (game && game.status == GM_GAME_SCHEDULED);
    }];
    NSLog(@"Found %d new not scheduled games", newGames.count);
    for (GMGameSchedule* game  in newGames) {
        [self scheduleNotificationForGame:game];
    }
}

-(GMGameActive*) testGameActive{
    return [[GMGameActive alloc] initWithJSON:@{
        @"id": @"26c58df7-fcb9-4bc9-9981-5b793cdad53a",
        @"home_team": @"ARI",
        @"away_team": @"BAL",
        @"time": @(1440964800000)
        }];
}

-(GMGameLive*) testGame{
    int rndYD = arc4random() % 51;
    NSString* homeTeam = @"ARI";
    NSString* awayTeam = @"BAL";
    NSString* rndOff = (arc4random() % 2 == 1 ? homeTeam : awayTeam);
    NSString* rndSide = (arc4random() % 2 == 1 ? homeTeam : awayTeam);
    int pts1 = arc4random() % 20 + 1;
    int pts2 = arc4random() % 20 + 1;
    int quarter = arc4random() % 4 + 1;
    int down = arc4random() % 4 + 1;
    int yfd = arc4random() % 15 + 1;
    int min = arc4random() % 16;
    int sec = arc4random() % 60;
    NSString* summary = @"";
    NSString* event = @"";
    int summaryId = arc4random() % 50;
    switch (summaryId) {
        default:
        case 0:
            event = @"";
            summary = @"";
            break;
        case 1:
            event = @"tvtimeout";
            summary = @"TV Timeout";
            break;
        case 2:
            event = @"teamtimeout";
            summary = @"Team Timeout";
            break;
        case 3:
            event = @"injurytimeout";
            summary = @"Injury Timeout";
            break;
        case 4:
            event = @"twominuteswarning";
            summary = @"Two Minutes Warning";
            break;
        case 5:
            event = @"quarterend";
            summary = @"End of Quarter";
            break;
        case 6:
            event = @"gameover";
            summary = @"End of Game";
            break;
        case 7:
            event = @"touchdown";
            break;
        case 8:
            event = @"extrapoint";
            break;
        case 9:
            event = @"conversion";
            break;
        case 10:
            event = @"fieldgoal";
            break;
        case 11:
            event = @"safety";
            break;
    }
    NSDictionary* dic = @{
                          @"live": @{
                                  @"id": @"26c58df7-fcb9-4bc9-9981-5b793cdad53a",
                                  @"home_team": @{
                                          @"id": homeTeam,
                                          @"name":[GMTeam teamWithTeamId:homeTeam].name,
                                          @"points": @(pts1)
                                          },
                                  @"away_team": @{
                                          @"id": awayTeam,
                                          @"name": [GMTeam teamWithTeamId:awayTeam].name,
                                          @"points": @(pts2)
                                          },
                                  @"quarter_number": @(quarter),
                                  @"clock": [NSString stringWithFormat:@"%.2d:%.2d", min, sec],
                                  @"down": @(down),
                                  @"yfd": @(yfd),
                                  @"side": rndSide,
                                  @"yard_line":@(rndYD),
                                  @"team": rndOff,
                                  @"updated": @"2015-08-30T21:01:00+00:00",
                                  @"seasonYear": @(2015),
                                  @"downId": @"cb774d4f-de22-4adc-a072-75e79e132227",
                                  @"summary": summary,
                                  @"event": event
                                  },
                          @"status": @"inprogress" // @"closed" / @"inprogress"
                          };
    NSLog(@"TEST: %@", dic);
    return [[GMGameLive alloc] initWithJSON:dic];
}

-(void) setGame:(GMGame*) game live:(BOOL) isLive{
    if (game){
        gameToLive = game;
        if ([game isKindOfClass:[GMGameLive class]])
            liveGame = (GMGameLive*)game;
    }
    if (game && isLive){
        if (liveTimer)
            [liveTimer invalidate];
        liveTimer = [NSTimer scheduledTimerWithTimeInterval:self.liveUpdateFrequency target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:liveTimer forMode:NSRunLoopCommonModes]; // this is a gem. Fixes issue when any scrolling view (including pickerview,textview,etc.) stops timer while scrolling.
    }
    if (!game && !isLive){
        [liveTimer invalidate];
        liveTimer = nil;
        liveGame = nil;
        gameToLive = nil;
    }
}

-(void) timerTick:(id) sender{
    if (sender == liveTimer){
        [self performUpdateLiveFeedForGame:gameToLive];
    }
    else if (sender == gamesTimer){
        [self performUpdateGames];
    }
}

@end
