//
//  GMLoginManager.m
//  GridironMoe
//
//  Created by Adya on 8/18/15.
//  Copyright (c) 2015 Ocusco Corporation. All rights reserved.
//

#import "GMUserManager.h"
#import "GMRequestManager.h"
#import "GMUser.h"
#import "TSError.h"
#import "GMTeam.h"
#import "GMStorage.h"
#import "GMMoeFace.h"
#import "FBSDKLoginManager.h"
#import "FBSDKLoginManagerLoginResult.h"
#import "FBSDKAccessToken.h"
#import "FBSDKProfile.h"
#import "FBSDKGraphRequest.h"
#import "TSUtils.h"


#define USER_TOKEN @"userToken"
#define USER_PASSWORD @"userPassword"
#define USER_USERNAME @"userEmail"
#define USER_MOE_NAME @"userMoeName"

#define SOCIAL_TYPE_FACEBOOK @"facebook"
#define DEFAULT_PASSWORD @"tempPass"

@implementation GMUserManager

@synthesize loginDelegate;
@synthesize profileDelegate;
@synthesize user;

@synthesize moeFaceImage;
@synthesize moeFace;

@synthesize fansInStadiums;
@synthesize loggedInUsers;

+ (instancetype) sharedManager{
    static GMUserManager* manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{manager = [[self alloc] init];});
    return manager;
}

-(id) init{
    self = [super init];
    [self loadUserData];
    return self;
}

-(void) clearUserData{
    NSUserDefaults* prefs = [NSUserDefaults standardUserDefaults];
    [prefs removeObjectForKey:USER_PASSWORD];
    [prefs synchronize];
}

-(void) saveUserData{
    if (!user) return;
    NSUserDefaults* prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:user.username forKey:USER_USERNAME];
    [prefs setObject:user.password forKey:USER_PASSWORD];
    [prefs synchronize];
}

-(void) loadUserData{
    NSUserDefaults* prefs = [NSUserDefaults standardUserDefaults];
    NSString* username = [prefs objectForKey:USER_USERNAME];
    NSString* password = [prefs objectForKey:USER_PASSWORD];
    user = [[GMUser alloc] initWithUsername:username andPassword:password];
}


-(BOOL) hasStoredUser{
    return ((user != nil) && (user.username != nil) && (user.password != nil));
}

-(BOOL) isLoggedIn{
    return user && [self isValidUsername:user.username] && [self isValidPassword:user.password];
}

-(BOOL) isValidUsername:(NSString *)username{
    return username.length > 0;
}

-(BOOL) isValidPassword:(NSString *)password{
    return password.length > 0;
}

-(void) logout{
    [self clearUserData];
    [self loadUserData];
}

// loads stored moe from user defaults or set default moe
-(void) loadMoe{
    NSUserDefaults* prefs = [NSUserDefaults standardUserDefaults];
    NSString* moeName = [prefs valueForKey:USER_MOE_NAME];
    if (!moeName || ![GMMoeFace isValidMoeName:moeName])
        moeFace = [GMMoeFace moeFaceAtIndex:0];
    else
        moeFace = [GMMoeFace moeFaceNamed:moeName];
    [self selectMoe:moeFace];
}

-(void) selectMoe:(GMMoeFace*)moe{
    if (moe){
        moeFace = moe;
        NSUserDefaults* prefs = [NSUserDefaults standardUserDefaults];
        [prefs setValue:moe.name forKey:USER_MOE_NAME];
        [prefs synchronize];
        moeFaceImage = [UIImage imageNamed:moe.topBar];
    }
}

-(void) selectMoeAtIndex:(NSInteger)moeNameIndex{
    [self selectMoe:[GMMoeFace moeFaceAtIndex:moeNameIndex]];
}


@end

@implementation GMUserManager (LoginProcess)
-(void) performLoginWithUsername:(NSString *)username andPassword:(NSString *)password{
    [[GMRequestManager sharedManager] requestLoginWithUsername:username andPassword: password withResponseCallback:^(BOOL success, NSArray* array, TSError *error) {
        if (success){
            user = [[GMUser alloc] initWithUsername:username andPassword:password];
            GMUser* authUser = (GMUser*)[array objectAtIndex:0];
            loggedInUsers = [[array objectAtIndex:1] intValue];
            user.team = authUser.team;
            user.total = authUser.total;
            user.email = authUser.email;
            [self saveUserData];
        }
        else{
            user = nil;
        }
        if ([loginDelegate respondsToSelector:@selector(onLoginResult:withUser:orError:)])
            [loginDelegate onLoginResult:success withUser:user orError:error];
    }];
}

-(void) performLoginWithSavedUser{
    if ([self hasStoredUser])
        [self performLoginWithUsername:user.username andPassword:user.password];
}

-(void) performLogout{
    if ([self isLoggedIn])
        [[GMRequestManager sharedManager] requestLogoutWithUsername:user.username andPassword:user.password withResponseCallback:^(BOOL success, TSError *error) {
            if (success) {
                [self logout];
            }
            if ([loginDelegate respondsToSelector:@selector(onLogoutResult:orError:)])
                [loginDelegate onLogoutResult:success orError:nil];
        }];
    else{
        [self logout];
        if ([loginDelegate respondsToSelector:@selector(onLogoutResult:orError:)])
            [loginDelegate onLogoutResult:YES orError:nil];
    }
}

@end

@implementation GMUserManager (RegistrationProcess)

-(void) performRegistrationWithUsername:(NSString *)username password:(NSString *)password andEmail:(NSString*)email shouldLoginOnSuccess:(BOOL)shouldLogin{
    [[GMRequestManager sharedManager] requestRegisterWithUsername:username password:password andEmail:email withResponseCallback:^(BOOL success, TSError *error) {
        if ([loginDelegate respondsToSelector:@selector(onRegistrationResult:orError:)])
            [loginDelegate onRegistrationResult:success orError:error];
        if (success && shouldLogin)
            [self performLoginWithUsername:username andPassword:password];
    }];
}

@end

@implementation GMUserManager (UserProfile)

-(void) performChangeUsername:(NSString *)newUsername{
    [[GMRequestManager sharedManager] requestSetUsername:newUsername forUserWithUsername:user.username andPassword:user.password withResponseCallback:^(BOOL success, TSError *error) {
        if (success){
            GMUser* newUser = [[GMUser alloc] initWithUsername:newUsername andPassword:user.password];
            newUser.team = user.team;
            newUser.total = user.total;
            newUser.defence = user.defence;
            newUser.offence = user.offence;
            newUser.avatarURL = user.avatarURL;
            newUser.email = user.email;
            user = newUser;
            [self saveUserData];
        }
        if ([profileDelegate respondsToSelector:@selector(onUserProfileUpdated:withUser:orError:)])
            [profileDelegate onUserProfileUpdated:success withUser:user orError:error];
    }];

    
}

-(void) performChangePassword:(NSString*) newPassword{
    [[GMRequestManager sharedManager] requestSetPassword:newPassword forUserWithUsername:user.username andPassword:user.password withResponseCallback:^(BOOL success, TSError *error) {
        if (success){
            GMUser* newUser = [[GMUser alloc] initWithUsername:user.username andPassword:newPassword];
            newUser.team = user.team;
            newUser.total = user.total;
            newUser.defence = user.defence;
            newUser.offence = user.offence;
            newUser.avatarURL = user.avatarURL;
            newUser.email = user.email;
            user = newUser;
            [self saveUserData];
        }
        if ([profileDelegate respondsToSelector:@selector(onUserProfileUpdated:withUser:orError:)])
            [profileDelegate onUserProfileUpdated:success withUser:user orError:error];
    }];
}

-(void) performForgotPasswordForUsername:(NSString *)username andEmail:(NSString *)email{
    username = trim(username);
    if (username.length == 0) username = nil;
    [[GMRequestManager sharedManager] requestForgotPasswordForUserWithUsername:trim(username) andEmail:email withResponseCallback:^(BOOL success, TSError *error) {
            if ([loginDelegate respondsToSelector:@selector(onForgotPasswordResult:orError:)])
                [loginDelegate onForgotPasswordResult:success orError:error];
    }];
}

-(void) performChangeEmail:(NSString*) newEmail{
    [[GMRequestManager sharedManager] requestSetEmail:newEmail forUserWithUsername:user.username andPassword:user.password withResponseCallback:^(BOOL success, TSError *error) {
        if (success){
            user.email = newEmail;
        }
        if ([profileDelegate respondsToSelector:@selector(onUserProfileUpdated:withUser:orError:)])
            [profileDelegate onUserProfileUpdated:success withUser:user orError:error];
    }];
}


-(void) performUpdateFansInStadiums{
    [[GMRequestManager sharedManager] requestGetCountInStadiumWithResponseCallback:^(BOOL success, NSObject *dic, TSError *error) {
        if (success)
            fansInStadiums = (NSDictionary*)dic;
        if ([profileDelegate respondsToSelector:@selector(onFansInStadiumsUpdated:withFans:orError:)])
            [profileDelegate onFansInStadiumsUpdated:success withFans:fansInStadiums orError:error];
    }];
}

-(void) performUpdateHomeTeam:(GMTeam *)team{
    [[GMRequestManager sharedManager] requestSetHomeTeam:team.teamId withUsername:user.username andPassword:user.password withResponseCallback:^(BOOL success, TSError *error) {
        if (success)
            user.team = [GMTeam teamWithTeamId:team.teamId];
        if ([profileDelegate respondsToSelector:@selector(onHomeTeamUpdated:orError:)])
            [profileDelegate onHomeTeamUpdated:success orError:error];
    }];
}

@end

@implementation GMUserManager (Facebook)

-(void) performLoginWithFacebookFromViewController:(UIViewController *)viewController{
    FBSDKLoginManager* manager = [FBSDKLoginManager new];
    [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
    [manager logInWithReadPermissions:@[@"public_profile", @"email"] fromViewController:viewController handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error || result.isCancelled) {
            if ([loginDelegate respondsToSelector:@selector(onSocialLoginResult:withUser:isNewUser:orError:)])
                [loginDelegate onSocialLoginResult:NO withUser:nil isNewUser:NO orError:[TSError errorWithCode:TS_ERROR_INTERNAL Title:nil andDescription:@"Facebook login failed."]];
        } else {
            [self performLoginWithFacebookToken:result.token];
        }
    }];
}

-(void) performLoginWithFacebookToken:(FBSDKAccessToken*)token{

    [[GMRequestManager sharedManager] requestLoginWithToken:token.userID forSocialNetwork:SOCIAL_TYPE_FACEBOOK withResponseCallback:^(BOOL success, NSArray *array, TSError *error) {
        if (success){
            user = (GMUser*)[array objectAtIndex:0];
            loggedInUsers = [[array objectAtIndex:1] intValue];
            [self saveUserData];
            if ([loginDelegate respondsToSelector:@selector(onSocialLoginResult:withUser:isNewUser:orError:)])
                [loginDelegate onSocialLoginResult:YES withUser:user isNewUser:NO orError:nil];
        }
        else if (error.code == TS_ERROR_INTERNAL){
            [self performReigstrationWithFacebookToken:token];
        }
        else{
            [self performReigstrationWithFacebookToken:token];
            if ([loginDelegate respondsToSelector:@selector(onSocialLoginResult:withUser:isNewUser:orError:)])
                [loginDelegate onSocialLoginResult:NO withUser:nil isNewUser:NO orError:error];
        }
    }];

}

-(void) performReigstrationWithFacebookToken:(FBSDKAccessToken*)token{
    FBSDKProfile* profile = [FBSDKProfile currentProfile];
    NSString* username = profile.firstName;
    NSString* password = DEFAULT_PASSWORD;
    if ([FBSDKAccessToken currentAccessToken]) {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields":@"email"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
              //   NSLog(@"fetched user:%@", result);
                 NSString* email = [result objectForKey:@"email"];
                 user = [[GMUser alloc] initWithUsername:username andPassword:password];
                 if ([loginDelegate respondsToSelector:@selector(onSocialLoginResult:withUser:isNewUser:orError:)])
                     [loginDelegate onSocialLoginResult:YES withUser:user isNewUser:YES orError:nil];
             }
         }];
    }
    
}
@end