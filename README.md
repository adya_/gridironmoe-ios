# Gridiron Moe v2

Welcome to the Gridiron Moe project wiki!

This wiki is written for those who will support this project.

Below is a short guidance which will help you understand how it works and be able to support it.

***

### Layers

![Layers](https://bytebucket.org/adya_/gridironmoe-ios/raw/5812c08418c373532dab0c2e3715b5199bb93e4b/wiki/img/GMLayers.png?token=18eeb5cd98e7b11f75d1dd9fcdc803662fcbd75a)


### View Controllers Hierarchy 

![View Controllers](https://bytebucket.org/adya_/gridironmoe-ios/raw/5812c08418c373532dab0c2e3715b5199bb93e4b/wiki/img/GMViewControllers.png?token=801729c9919dc3ecf48f53a0e0b80a639263ec43)

**Please, follow this architecture to easily add any modifications. Do not break flows in the app. Thanks.**

***

Regards,
Adya.